source 'https://rubygems.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.0.3'

# Use SCSS for stylesheets
gem 'sass-rails', '~> 4.0.0'
gem 'bootstrap-sass', '~> 3.1.1'
gem 'font-awesome-rails'
gem 'bootstrap_form', git: 'https://github.com/bootstrap-ruby/rails-bootstrap-forms.git'

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'

# See https://github.com/sstephenson/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'

# Add jquery UI for autocomplete
gem 'jquery-ui-rails'

# Active Model Serializer for structuring JSON responses
gem 'active_model_serializers', git: 'https://github.com/rails-api/active_model_serializers.git'
 
group :doc do
  # bundle exec rake doc:rails generates the API under doc/api.
  gem 'sdoc', require: false
end

# Use ActiveModel has_secure_password
gem 'bcrypt-ruby', '~> 3.1.2'

# Use Kaminari for paging
gem 'kaminari'
gem 'kaminari-bootstrap', '~> 3.0.1'

# Use AWS_SDK for communicating with S3
gem 'aws-sdk'

# Use RMagik for image manipulation
gem 'rmagick', '2.13.2', :require => 'RMagick'

# Use Hipchat communicating with their API
gem 'hipchat'

# Use Resque to run Hipchat jobs asynchronously
gem 'resque', require: 'resque/server'

# HTTP Client
gem 'rest-client'

# Use unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano', group: :development

group :development do
  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'ninefold', '~> 1.5.0'
  gem 'jasmine-rails'
  gem 'jasmine-jquery-rails'
end

group :development, :test do
  # Use sqlite3 as the database for Active Record
  gem 'sqlite3'
  gem 'rspec-rails', '~> 2.14.1'
  gem 'factory_girl_rails'
end

group :test do
  gem 'database_cleaner'
  gem 'shoulda-matchers'
end

group :production, :staging do
  gem 'pg'
end
