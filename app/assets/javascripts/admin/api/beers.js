jQuery(function () {
    return $('#api_beer_form_style_name').autocomplete({
        source: $('#api_beer_form_style_name').data('autocomplete-source')
    });
});

jQuery(function () {
    return $('#api_beer_form_style_name').on('autocompleteselect', function (event, ui) {
        $('#api_beer_form_style_name').val(ui.item.label);
        $('#api_beer_form_style_brewerydb_id').val(ui.item.value);
        return false;
    });
});

jQuery(function () {
    return $('#api_beer_form_style_name').on('autocompletefocus', function (event, ui) {
        $('#api_beer_form_style_name').val(ui.item.label);
        return false;
    });
});
