PlReviews.Application = {};

// Global includes
jQuery(function () {
    $.fn.setAutocomplete = function () {
        this.autocomplete({
            source: this.data('autocomplete-source')
        });
        return this;
    };
});

// On focus, set the autocomplete field to the label
jQuery(function () {
    $.fn.setAutocompleteFocusHandler = function () {
        this.on('autocompletefocus', PlReviews.Application.onAutocompleteFocusHandler);
        return this;
    };

    PlReviews.Application.onAutocompleteFocusHandler = function (event, ui) {
        $(this).val(ui.item.label);
        return false;
    };
});

// Initialize tooltips
jQuery(function () {
    PlReviews.Application.setFridgeTooltip = function () {
        $('.fridge-icon').tooltip();
    };

    PlReviews.Application.setFridgeTooltip();
});