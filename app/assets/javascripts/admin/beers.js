PlReviews.Beers = {};

// BeerForm Brewery functions

// Brewery select handler
jQuery(function () {
    PlReviews.Beers.setBreweryAutocompleteSelectHandler = function () {
        $('#beer_form_brewery_name').on('autocompleteselect', PlReviews.Beers.onBreweryAutocompleteSelect);
    };

    PlReviews.Beers.onBreweryAutocompleteSelect = function (event, ui) {
        PlReviews.Beers.updateFormFromBrewerySelection(ui.item.label, ui.item.value);
        PlReviews.Beers.getBeerAutocompleteData(ui.item.value);

        return false;
    };

    PlReviews.Beers.updateFormFromBrewerySelection = function (breweryName, breweryDbId) {
        $('#beer_form_brewery_name').val(breweryName);
        $('#beer_form_brewery_name').toggle();
        $('#selected_brewery').toggle();
        $('#selected_brewery_name').html(breweryName);

        $('#beer_form_brewery_brewerydb_id').val(breweryDbId);
        $('#beer_fields').prop('disabled', false);

        create_brewerydb_link_url = $('#create_brewerydb_beer_link').data('url');
        create_brewerydb_link_url = create_brewerydb_link_url.replace(/-id-/, breweryDbId);
        $('#create_brewerydb_beer_link').attr('href', create_brewerydb_link_url);
        $('#create_brewerydb_beer_link').toggle();
    };

    PlReviews.Beers.getBeerAutocompleteData = function (breweryDbId) {
        var url = $('#beer_form_name').data('autocomplete-source');
        url = url.replace(/-id-/, breweryDbId);
        $.ajax(url)
            .done(function (results) {
                $('#beer_form_name').autocomplete({
                    source: results
                });
            });
    };
});

// automatically populate brewery on form load
jQuery(function () {
    PlReviews.Beers.autoPopulateBrewery = function () {
        var breweryName = $('#beer_form_brewery_name'),
            breweryId = $('#beer_form_brewery_brewerydb_id');
        if (breweryName.val()) {
            breweryName.trigger('autocompleteselect', {item: {label: breweryName.val(), value: breweryId.val()}});
        }

        return false;
    };

    PlReviews.Beers.setBeerFormReadyHandler = function () {
        $('#new_beer_form').ready(PlReviews.Beers.autoPopulateBrewery);
    }
});

// Clear brewery fields
jQuery(function () {
    PlReviews.Beers.clearBeerForm = function () {
        $('#beer_form_brewery_name').val('');
        $('#beer_form_brewery_name').toggle();

        $('#selected_brewery').toggle();
        $('#beer_form_brewery_brewerydb_id').val('');

        $('#beer_fields').prop('disabled', true);
        $('#beer_form_name').val('');
        $('#beer_form_beer_brewerydb_id').val('');

        $('#create_brewerydb_beer_link').attr('href', '');
        $('#create_brewerydb_beer_link').toggle();
    };

    PlReviews.Beers.setClearButtonClick = function () {
        $('#clear_brewery').on('click', PlReviews.Beers.clearBeerForm);
    }
});

// BeerForm Beer functions
jQuery(function () {
    PlReviews.Beers.onBeerAutocompleteSelect = function (event, ui) {
        $('#beer_form_name').val(ui.item.label);
        $('#beer_form_beer_brewerydb_id').val(ui.item.value);
        return false;
    };

    PlReviews.Beers.setBeerAutocompleteSelectHandler = function () {
        $('#beer_form_name').on('autocompleteselect', PlReviews.Beers.onBeerAutocompleteSelect);
    };
});


// Initializers for New Beer Form
jQuery(function () {
    $('#beer_form_brewery_name').setAutocomplete();
    $('#beer_form_brewery_name').setAutocompleteFocusHandler();
    PlReviews.Beers.setBreweryAutocompleteSelectHandler();
    PlReviews.Beers.setBeerFormReadyHandler();
    PlReviews.Beers.setClearButtonClick();

    // Beer Autocomplete functions
    $('#beer_form_name').setAutocompleteFocusHandler();
    PlReviews.Beers.setBeerAutocompleteSelectHandler();
});
