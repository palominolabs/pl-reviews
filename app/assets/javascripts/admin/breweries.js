PlReviews.Breweries = {};

jQuery(function () {
    PlReviews.Breweries.setBreweryNameAutocompleteselectHandler = function () {
        $('#brewery_name').on('autocompleteselect', PlReviews.Breweries.oBreweryNamenAutocompleteselect);
    };

    PlReviews.Breweries.oBreweryNamenAutocompleteselect = function (event, ui) {
        $('#brewery_name').val(ui.item.label);
        $('#brewery_brewerydb_id').val(ui.item.value);
        return false;
    };
});

jQuery(function () {
    PlReviews.Breweries.setBreweryNameAutocompleteselectHandler();
    $('#brewery_name').setAutocomplete();
    $('#brewery_name').setAutocompleteFocusHandler();
});
