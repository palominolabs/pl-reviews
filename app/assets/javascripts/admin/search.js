PlReviews.Search = {};

jQuery(function () {
    PlReviews.Search.loadCustomCatcompleteWidget = function () {
        $.widget("custom.catcomplete", $.ui.autocomplete, {
            _renderMenu: PlReviews.Search.renderMenu
        });
    };

    PlReviews.Search.renderMenu = function (ul, items) {
        var me = this,
            currentCategory = "";
        $.each(items, function (index, item) {
            currentCategory = PlReviews.Search.renderMenuItem(me, ul, item, currentCategory);
        });
    };

    PlReviews.Search.renderMenuItem = function (menu, ul, item, currentCategory) {
        if (item.category != currentCategory) {
            ul.append("<li class='ui-autocomplete-category'>" + item.category + "</li>");
            currentCategory = item.category;
        }
        menu._renderItemData(ul, item);
        return currentCategory;
    };

    $.fn.setCatcomplete = function () {
        this.catcomplete({
            delay: 0,
            source: this.data('autocomplete-source')
        });
    };
});

jQuery(function () {
    PlReviews.Search.setSearchSelectHandler = function () {
        return $('#search').on('catcompleteselect', PlReviews.Search.onSearchSelect);
    };

    PlReviews.Search.onSearchSelect = function (event, ui) {
        var url;

        if (ui.item.category == 'Beers') {
            url = $('#search').data('beers-url') + '/' + ui.item.value;
        } else if (ui.item.category == 'Breweries') {
            url = $('#search').data('breweries-url') + '/' + ui.item.value;
        }

        if (url) {
            PlReviews.Search.redirectToUrl(url);
        }
    };

    PlReviews.Search.redirectToUrl = function (url) {
        window.location = url;
    };

    PlReviews.Search.setSearchFocusHandler = function () {
        return $('#search').on('catcompletefocus', PlReviews.Search.onSearchFocus);
    };

    PlReviews.Search.onSearchFocus = function (event, ui) {
        $('#search').val(ui.item.label);
        return false;
    };
});

jQuery(function () {
    PlReviews.Search.loadCustomCatcompleteWidget();
    PlReviews.Search.setSearchSelectHandler();
    PlReviews.Search.setSearchFocusHandler();
    $('#search').setCatcomplete();
});