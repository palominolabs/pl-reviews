class ActivitiesController < ApplicationController
  skip_before_action :require_authentication, only: [:index, :show]
  before_action :set_activity, only: [:show]
  after_filter :set_x_frame_options_header_for_widget, only: [:index], if: lambda { |controller| controller.request.format.widget? }

  # GET /events
  def index
    @activities = get_activities_list
    respond_with @activities, meta: {total_count: @activities.total_count, page: params[:page] || 1}
  end

  # GET /show/1
  def show
    respond_with @activity
  end

  private
  def set_activity
    @activity = Activity.find(params[:id])
  end

  def get_activities_list
    if params[:coffee] == '1'
      @filter = 'Coffee'
      activities = Activity.coffee_related
    elsif params[:beer] == '1'
      @filter = 'Beer'
      activities = Activity.beer_related
    else
      @filter = 'All'
      activities = Activity.all
    end

    if params[:type]
      activities.where!(type: params[:type])
    end

    activities.order('created_at desc').page(params[:page])
  end
end
