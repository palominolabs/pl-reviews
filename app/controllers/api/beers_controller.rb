module Api
  class BeersController < ApplicationController
    before_action :set_styles_autocomplete, only: [:new, :create]
    before_action :set_brewery_name, only: [:new, :create]
    respond_to :json, only: [:beer_styles_autocomplete]
    respond_to :html, only: [:new]

    def new
      @beer_form = Api::BeerForm.new({brewery_brewerydb_id: params[:brewery_id]})
    end

    def create
      brewery_brewerydb_id = params[:brewery_id]
      @beer_form = Api::BeerForm.new(beer_form_params)
      @beer_form.brewery_brewerydb_id = brewery_brewerydb_id
      if @beer_form.valid?
        begin
          beer_brewerydb_id = BrewerydbApiService.create_beer(@beer_form)

          if beer_brewerydb_id
            flash.notice = 'Beer successfully added to BreweryDB'
            redirect_params = {
                from_api: true,
                beer_form: {
                    name: @beer_form.name,
                    beer_brewerydb_id: beer_brewerydb_id,
                    brewery_name: @brewery_name,
                    brewery_brewerydb_id: brewery_brewerydb_id
                }
            }

            redirect_to new_beer_path(redirect_params) and return
          end

        rescue BrewerydbApiService::RequestFailed, BrewerydbApiService::BeerCreationFailed => e
          flash.now.alert = 'Failed to save beer to BreweryDB'
        end
      end
      render :new
    end

    def beer_styles_autocomplete
      beer_styles = BrewerydbApiService.get_style_autocomplete
      respond_with beer_styles.to_json
    end

    private
    def beer_form_params
      params.require(:api_beer_form).permit(:name, :style_name, :style_brewerydb_id, :abv)
    end

    def set_styles_autocomplete
      @beer_styles_autocomplete = BrewerydbApiService.get_style_autocomplete
    end

    def set_brewery_name
      @brewery_name = BrewerydbApiService.get_brewery(params[:brewery_id])[:name] if params[:brewery_id]
    end
  end
end
