module Api
  class BreweriesController < ApplicationController
    respond_to :json
    respond_to :html, only: [:new]

    def brewery_autocomplete
      partial_name = '*'
      if params[:term]
        partial_name = partial_name + params[:term] + '*'
      end
      brewery_names = BrewerydbApiService.get_brewery_autocomplete name: partial_name
      respond_with brewery_names.to_json
    end

    def brewery_beer_autocomplete
      beer_names = BrewerydbApiService.get_brewery_beer_autocomplete params[:id]
      respond_with beer_names.to_json
    end

    def new
      @brewery_form = Api::BreweryForm.new
    end

    def create
      @brewery_form = Api::BreweryForm.new(brewery_form_params)
      if @brewery_form.valid?
        begin
          brewery_brewerydb_id = BrewerydbApiService.create_brewery(@brewery_form)
          if brewery_brewerydb_id
            flash.notice = 'Brewery successfully added to BreweryDB'
            # Redirect to Api/beers/new
            redirect_to new_api_brewery_beer_path(brewery_brewerydb_id) and return
          end

        rescue BrewerydbApiService::RequestFailed, BrewerydbApiService::BreweryCreationFailed => e
          flash.now.alert = 'Failed to save brewery to BreweryDB'
        end
      end
      render :new
    end

    private
    def brewery_form_params
      params.require(:api_brewery_form).permit(:name)
    end
  end
end
