class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  helper_method :current_user
  respond_to :html, :json
  before_action :require_authentication
  before_action :set_view_path_for_format
  skip_before_action :verify_authenticity_token, if: :json_request?

  private
  def require_authentication
    unless current_user
      respond_to do |format|
        format.html do
          flash[:error] = 'You must be logged in to access this section.'
          redirect_to log_in_url
        end

        format.json do
          render nothing: true, status: :unauthorized
        end
      end
    end
  end

  def set_view_path_for_format
    if request.format == :html
      prepend_view_path 'app/views/admin'
    else
      prepend_view_path 'app/views/widget'
    end
  end

  def set_x_frame_options_header_for_widget
    headers['x-frame-options'] = Rails.configuration.iFrameConfig.x_frame_options
  end

  def current_user
    @current_user ||= User.find_by_id(session[:user_id]) if session[:user_id]
  end

  def json_request?
    request.format.json?
  end
end
