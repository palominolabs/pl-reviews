class BagsController < ApplicationController
  skip_before_action :require_authentication, only: [:index]
  before_action :set_bag, only: [:show, :destroy, :open, :finish]
  before_action :set_coffee_from_coffee_id, only: [:index, :new, :create]

  # GET /bags
  def index
    @bags = Bag.all.order('roasted_on desc').where(coffee: @coffee).page params[:page]
  end

  # GET /bags/new
  def new
    @bag = Bag.new
  end

  # POST /bags
  def create
    @bag = Bag.new(bag_params)
    @bag.coffee = @coffee

    @bag.bag_opened_activity = BagOpenedActivity.new(user: current_user, coffee: @bag.coffee) if @bag.opened_on

    if @bag.save
      redirect_to coffee_bags_path(@coffee), notice: 'Bag was successfully created.'
    else
      flash.now.alert = 'Failed to save bag.'
      render :new
    end
  end

  def destroy
    coffee = @bag.coffee
    @bag.destroy
    redirect_to coffee_bags_path(coffee)
  end

  def open
    if @bag.open(current_user)
      if @bag.save
        flash.notice = 'Bag was successfully opened.'
      else
        flash.alert = 'Failed to open bag.'
      end
    else
      flash.notice = 'Bag was already opened.'
    end

    redirect_to coffee_bags_path(@bag.coffee)
  end

  def finish
    if @bag.finish
      if @bag.save
        flash.notice = 'Bag was successfully finished.'
      else
        flash.alert = 'Failed to finish bag.'
      end
    else
      flash.notice = 'Bag was already finished or not opened yet.'
    end

    redirect_to coffee_bags_path(@bag.coffee)
  end

  private
  def bag_params
    params.require(:bag).permit(:roasted_on, :coffee_id, :opened_on)
  end

  def set_bag
    @bag = Bag.find(params[:id])
  end

  def set_coffee_from_coffee_id
    @coffee = Coffee.find(params[:coffee_id]) if params[:coffee_id]
  end
end