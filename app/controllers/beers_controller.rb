class BeersController < ApplicationController
  skip_before_action :require_authentication, only: [:index, :show, :random]
  before_action :set_beer, only: [:show, :edit, :update, :destroy, :open, :sync_beer]
  before_action :set_brewery_from_brewery_id, only: [:index, :create, :destroy]
  before_action :set_filter_params, only: [:index]
  before_action :set_new_review, only: [:show, :open]
  after_filter :set_x_frame_options_header_for_widget, only: [:index], if: lambda { |controller| controller.request.format.widget? }
  helper_method :sort_column, :sort_direction
  BATCH_SIZE = 10.freeze

  # GET /beers
  def index
    @beers = get_beers_list

    # Save total_count before applying paging
    metadata = {
        total_count: @beers.length
    }

    unless params[:no_paging] == '1'
      @beers = @beers.page(params[:page])
      @beers = @beers.per(@page_size) if @page_size
      metadata[:page] = params[:page] || 1
    end

    respond_with @beers, meta: metadata
  end

  # GET /beers/1
  def show
    @reviewable = @beer
    respond_with @beer
  end

  # GET /beers/new
  def new
    if params[:from_api]
      @from_api = params[:from_api]
      @beer_form = BeerForm.new(beer_form_params)
    else
      @beer_form = BeerForm.new
    end
  end

  # GET /beers/1/edit
  def edit
  end

  # POST /beers
  def create
    @beer_form = BeerForm.new(beer_form_params)

    if @beer_form.valid?
      begin
        brewery = Brewery.find_by_brewerydb_id(@beer_form.brewery_brewerydb_id)

        unless brewery
          brewery = Brewery.new(BrewerydbApiService.get_brewery(@beer_form.brewery_brewerydb_id))

          unless brewery.save
            flash.now.alert = 'Failed to save brewery'
            render action: :new and return
          end
        end

        beer = Beer.create_from_beer_form_and_brewery(@beer_form, brewery)
        beer.assign_attributes(BrewerydbApiService.get_beer(@beer_form.beer_brewerydb_id))

        beer.activities << BeerAddedActivity.new(beer: beer, user: current_user)

        if beer.save
          redirect_to beer, notice: 'Beer was successfully created.' and return
        else
          flash.now.alert = 'Failed to save beer'

          if beer.errors[:brewerydb_id].any?
            @beer_form.errors.add(:name, 'Beer already exists in database!')
          end
        end

      rescue BrewerydbApiService::BeerNotFound => e
        @beer_form.errors.add(:beer_name, 'Beer name must match existing beer for the specified brewery in BreweryDB')
      rescue BrewerydbApiService::BreweryNotFound => e
        @beer_form.errors.add(:brewery_name, 'Brewery name must match existing brewery in BreweryDB')
      rescue BrewerydbApiService::RequestFailed => e
        flash.now.alert = 'Failed to get brewery or beer from BreweryDB. Please check that both names are valid'
      end
    end

    render action: :new
  end

  # PATCH/PUT /beers/1
  def update
    if @beer.update(beer_params)
      redirect_to @beer, notice: 'Beer was successfully updated.'
    else
      render action: :edit
    end
  end

  # DELETE /beers/1
  def destroy
    if @beer.destroy
      flash.notice = 'Beer successfully deleted'
    else
      flash.alert = 'Failed to delete beer'
    end

    if params[:brewery_id]
      redirect_to brewery_beers_path(@brewery)
    else
      redirect_to beers_url
    end
  end

  # GET OPEN /beers/1/open
  def open
    open_user = get_open_user
    if @beer.open(open_user)
      if @beer.save
        Resque.enqueue(HipchatBeerOpenedMessage, @beer.id, open_user.id)
        status = :ok
        flash.notice = "A bottle of #{@beer.name} was successfully opened."
      else
        status = :internal_server_error
        flash.alert = "Failed to open a bottle of #{@beer.name}."
      end
    else
      status = :unprocessable_entity
      flash.alert = "No bottles of #{@beer.name} left to open."
    end

    respond_to do |format|
      format.html do
        redirect_to @beer
      end

      format.json do
        if status == :ok
          respond_with @beer
        else
          render nothing: true, status: status
        end
      end
    end
  end

  # GET SYNC /beers/sync
  def sync
    updated_entries = 0

    get_brewery_batches.each { |batch|
      begin
        batch_beers = BrewerydbApiService.get_beer_batch(batch)
        batch_beers.each { |beer_data|
          beer = Beer.find_by_brewerydb_id(beer_data[:brewerydb_id].to_s)
          beer.assign_attributes(beer_data)
          if beer.changed? and beer.save
            updated_entries += 1
          end
        }
      rescue BrewerydbApiService::BeerNotFound, BrewerydbApiService::RequestFailed => e
        flash.alert = "Failed to sync #{batch.join(', ')}"
      end
    }
    flash.notice = updated_entries.to_s + ' ' + 'beer'.pluralize(updated_entries) + ' updated when syncing with Brewery DB'
    redirect_to beers_url
  end

  # GET SYNC /beers/1/sync
  def sync_beer
    begin
      beer_data = BrewerydbApiService.get_beer(@beer.brewerydb_id)
      @beer.assign_attributes(beer_data)
      if @beer.changed?
        if @beer.save
          flash.notice = 'Beer successfully synced!'
        else
          flash.alert = 'Beer failed to save!'
        end
      else
        flash.notice = 'No new changes to beer!'
      end
    rescue BrewerydbApiService::BeerNotFound, BrewerydbApiService::RequestFailed => e
      flash.alert = 'Failed to fetch latest udpates from BreweryDB'
    end

    redirect_to @beer
  end

  # GET RANDOM /beers/random
  def random
    random_beer = Beer.cold.sample
    respond_to do |format|
      format.html do
        if random_beer
          redirect_to random_beer
        else
          flash.alert = 'No cold beers to choose from!'
          redirect_to beers_path
        end
      end


      format.json do
        if random_beer
          respond_with random_beer
        else
          render nothing: true, status: 404
        end
      end
    end
  end

  private
  # Sets filter params for index
  def set_filter_params
    @in_fridge = '1' if params[:in_fridge] == '1'
    @in_stock = '1' if params[:in_stock] == '1'
    @page_size = params[:page_size] if params[:page_size]
  end

# Use callbacks to share common setup or constraints between actions.
  def set_beer
    @beer = Beer.find(params[:id])
  end

  def set_brewery_from_brewery_id
    @brewery = Brewery.find(params[:brewery_id]) if params[:brewery_id]
  end

  def set_new_review
    @review = Review.new
  end

# Returns ordered and filtered list of beers
  def get_beers_list
    if @brewery
      beers = Beer.where(brewery: @brewery)
    else
      beers = Beer.all
    end

    # Apply filters
    beers = beers.in_stock if @in_stock == '1'
    beers = beers.cold if @in_fridge == '1'

    # Sort beers
    beers.order(sort_column + ' ' + sort_direction)
  end

  def get_brewery_batches
    brewerydb_ids = Beer.all.map(&:brewerydb_id)
    brewerydb_ids.each_slice(BATCH_SIZE).to_a
  end

  def beer_form_params
    params.require(:beer_form).permit(:name, :beer_brewerydb_id, :brewery_name, :brewery_brewerydb_id, :inventory, :in_fridge)
  end

  def beer_params
    params.require(:beer).permit(:inventory, :in_fridge)
  end

# Table sorting helper functions
  def sort_column
    Beer.column_names.include?(params[:sort]) ? params[:sort] : 'name'
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : 'asc'
  end

  def get_open_user
    open_user = current_user
    if open_user && open_user.is_admin? && params[:hipchat_id]
      hipchat_user = User.find_by_hipchat_id(params[:hipchat_id])
      open_user = hipchat_user if hipchat_user
    end
    open_user
  end
end
