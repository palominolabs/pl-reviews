class BreweriesController < ApplicationController
  before_action :set_brewery, only: [:show, :edit, :update, :destroy, :sync_brewery]
  skip_before_action :require_authentication, only: [:index, :show]
  helper_method :sort_column, :sort_direction

  # GET /breweries
  def index
    @breweries = Brewery.order(sort_column + ' ' + sort_direction).page params[:page]

    respond_with @breweries, meta: {total_count: Brewery.all.length, page: params[:page] || 1}
  end

  # GET /breweries/1
  def show
    respond_with @brewery
  end

  # GET /breweries/new
  def new
    @brewery = Brewery.new
  end

  # POST /breweries
  def create
    @brewery = Brewery.new(brewery_params)
    if @brewery.valid?
      begin
        @brewery.assign_attributes(BrewerydbApiService.get_brewery(@brewery.brewerydb_id))

        if @brewery.save
          redirect_to @brewery, notice: 'Brewery was successfully created.' and return
        else
          flash.now.alert = 'Brewery failed to save'
        end

      rescue BrewerydbApiService::BreweryNotFound => e
        @brewery.errors.add(:name, 'Brewery name must match existing brewery in BreweryDB')
      rescue BrewerydbApiService::RequestFailed => e
        @brewery.errors.add(:name, 'Failed to get brewery from BreweryDB')
      end
    end
    render :new
  end

  # DELETE /breweries/1
  def destroy
    @brewery.destroy
    redirect_to breweries_url
  end

  # GET /breweries/sync
  def sync
    updated_entries = 0
    breweries = Brewery.all
    breweries.each { |brewery|
      brewery.assign_attributes(BrewerydbApiService.get_brewery(brewery.brewerydb_id))
      if brewery.changed? and brewery.save
        updated_entries += 1
      end
    }
    flash.notice = updated_entries.to_s + ' ' + 'brewery'.pluralize(updated_entries) + ' updated when syncing with Brewery DB'
    redirect_to breweries_url
  end

  # GET /breweries/1/sync
  def sync_brewery
    begin
      brewery_data = BrewerydbApiService.get_brewery(@brewery.brewerydb_id)
      @brewery.assign_attributes(brewery_data)
      if @brewery.changed?
        if @brewery.save
          flash.notice = 'Brewery successfully synced!'
        else
          flash.alert = 'Brewery failed to save!'
        end
      else
        flash.notice = 'No new changes to brewery!'
      end
    rescue BrewerydbApiService::BreweryNotFound, BrewerydbApiService::RequestFailed => e
      flash.alert = 'Failed to fetch latest udpates from BreweryDB'
    end

    redirect_to @brewery
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_brewery
    @brewery = Brewery.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def brewery_params
    params.require(:brewery).permit(:name, :brewerydb_id)
  end

  def sort_column
    Brewery.column_names.include?(params[:sort]) ? params[:sort] : 'name'
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : 'asc'
  end
end
