class CoffeesController < ApplicationController
  skip_before_action :require_authentication, only: [:index, :show]
  before_action :set_coffee, only: [:show, :edit, :update, :destroy]
  before_action :set_roaster_from_roaster_id, only: [:index, :new, :create]
  before_action :set_roasters, only: [:new, :create, :edit, :update]
  after_filter :set_x_frame_options_header_for_widget, only: [:index], if: lambda { |controller| controller.request.format.widget? }
  
  # GET /coffees
  def index
    @coffees = get_coffees_list

    # Save total_count before applying paging
    metadata = {
        total_count: @coffees.length
    }

    unless params[:no_paging] == '1'
      @coffees = @coffees.page params[:page]
      metadata[:page] = params[:page] || 1
    end

    respond_with @coffees, meta: metadata
  end

  # GET /coffees/1
  def show
    @review = Review.new
    @reviewable = @coffee
    respond_with @coffee
  end

  # GET /coffees/new
  def new
    @coffee = Coffee.new
  end

  # POST /coffees
  def create
    @coffee = Coffee.new(coffee_params)
    @coffee.activities << CoffeeAddedActivity.new(user: current_user, coffee: @coffee)

    if @coffee.save
      redirect_to @coffee, notice: 'Coffee was successfully created.'
    else
      flash.now.alert = 'Failed to save coffee.'
      render :new
    end
  end

  # GET /coffees/1/edit
  def edit
  end

  # PATCH/PUT /coffees/1
  def update
    if @coffee.update(coffee_params)
      redirect_to @coffee, notice: 'Coffee was successfully updated.'
    else
      render action: :edit
    end
  end


  def destroy
    @coffee.destroy
    redirect_to coffees_url
  end

  private
  def coffee_params
    params.require(:coffee).permit(:name, :origin, :variety, :drip_or_espresso, :roaster_id)
  end

  def set_coffee
    @coffee = Coffee.find(params[:id])
  end

  def set_roaster_from_roaster_id
    @roaster = Roaster.find(params[:roaster_id]) if params[:roaster_id]
  end

  def set_roasters
    @roasters = Roaster.order(:name)
  end
  
  def get_coffees_list
    @in_stock = '1' if params[:in_stock] == '1'

    if @roaster
      coffees = Coffee.where(roaster: @roaster)
    else
      coffees = Coffee.all
    end

    # Apply filters
    coffees = coffees.in_stock if @in_stock == '1'

    # Sort coffees
    coffees.order('name asc')
  end
end