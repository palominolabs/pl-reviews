class ReviewsController < ApplicationController
  before_action :set_review, only: [:destroy]
  before_action :set_reviewable, only: [:create, :destroy]

  def create
    @review = @reviewable.reviews.new(review_params)
    @review.user = current_user

    if @reviewable.create_reviewed_activity(@review)
      if @review.save
        flash.notice = 'Review was successfully created.'
      else
        flash.alert = 'Failed to save review.'
      end
    else
      flash.alert = 'Invalid review activity.'
    end

    redirect_to @reviewable
  end

  def destroy
    if @review.user == current_user
      unless @review.destroy
        flash.now.alert = 'Failed to delete review'
      end
    else
      flash.now.alert = 'Failed to delete review: You are not the reviewer!'
    end
    redirect_to @reviewable
  end

  private
  def set_review
    @review = Review.find(params[:id])
  end

  def review_params
    params.require(:review).permit(:rating, :comment)
  end

  def set_reviewable
    resource, id = request.path.split('/')[1,2]
    @reviewable = resource.singularize.classify.constantize.find(id)
  end
end
