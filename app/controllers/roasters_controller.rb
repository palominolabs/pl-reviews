class RoastersController < ApplicationController
  skip_before_action :require_authentication, only: [:index, :show]
  before_action :set_roaster, only: [:show, :edit, :update, :destroy]

  # GET /roasters
  def index
    @roasters = Roaster.all.order('name asc').page params[:page]
  end

  # GET /roasters/1
  def show
  end

  # GET /roasters/new
  def new
    @roaster = Roaster.new
  end

  # POST /roasters
  def create
    @roaster = Roaster.new(roaster_params)
    if @roaster.save
      if params[:roaster][:image]
        if upload_s3_image(params[:roaster][:image], @roaster)
          redirect_to @roaster, notice: 'Roaster was successfully created.' and return
        end
      else
        redirect_to @roaster, notice: 'Roaster was successfully created.' and return
      end
    else
      flash.now.alert = 'Failed to save Roaster.'
    end
    render :new
  end

  # GET /roasters/1/edit
  def edit
  end

  # PATCH/PUT /roasters/1
  def update
    if @roaster.update(roaster_params)
      if params[:roaster][:image]
        if upload_s3_image(params[:roaster][:image], @roaster)
          redirect_to @roaster, notice: 'Roaster was successfully updated.' and return
        end
      else
        redirect_to @roaster, notice: 'Roaster was successfully updated.' and return
      end
    end
    render action: :edit
  end

  def destroy
    @roaster.destroy
    redirect_to roasters_url
  end

  private
  def roaster_params
    params.require(:roaster).permit(:name, :location)
  end

  def set_roaster
    @roaster = Roaster.find(params[:id])
  end

  def upload_s3_image(image, roaster)
    begin
      AwsService.upload_roaster_image(image, roaster)
    rescue AwsService::RoasterSaveFailed => e
      flash.now.alert = 'Failed to save image, please try again' and return false
    rescue AwsService::ImageUploadFailed => e
      flash.now.alert = 'Failed to upload image' and return false
    rescue AwsService::InvalidImageFormat => e
      flash.now.alert = 'Invalid Format: Only JPEGs and PNGs are supported' and return false
    rescue AwsService::NoImageProvided => e
      flash.now.alert = 'No image provided' and return false
    end
  end
end