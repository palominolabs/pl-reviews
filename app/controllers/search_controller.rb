class SearchController < ApplicationController
  skip_before_action :require_authentication, only: [:index, :autocomplete, :beers]
  before_action :set_partial_name, only: [:beers, :autocomplete]
  respond_to :json, only: [:autocomplete, :beers]
  respond_to :html, only: [:index]

  # GET /search/autocomplete
  def autocomplete
    # Beers
    beer_matches = Beer.where('lower(name) like ?', @partial_name) || []
    beer_matches.map! { |beer| {label: beer.name, value: beer.id, category: 'Beers'} }
    
    #Brewery
    brewery_matches = Brewery.where('lower(name) like ?', @partial_name) || []
    brewery_matches.map! { |brewery| {label: brewery.name, value: brewery.id, category: 'Breweries'} }

    #Coffee
    coffee_matches = Coffee.where('lower(name) like ?', @partial_name) || []
    coffee_matches.map! { |coffee| {label: coffee.name, value: coffee.id, category: 'Coffees'} }

    #Roasters
    roaster_matches = Roaster.where('lower(name) like ?', @partial_name) || []
    roaster_matches.map! { |roaster| {label: roaster.name, value: roaster.id, category: 'Roasters'} }

    respond_with (beer_matches + brewery_matches + coffee_matches + roaster_matches).to_json
  end

  def index
  end

  def beers
    beers = Beer.where('lower(name) like ?', @partial_name)

    # apply filtering
    beers = beers.in_stock if params[:in_stock] == '1'
    beers = beers.cold if params[:in_fridge] == '1'

    # apply paging
    @beers = beers.page(params[:page])
    @beers = @beers.per(params[:page_size]) if params[:page_size]

    metadata = {
        total_count: @beers.total_count,
        page: params[:page] || 1
    }

    respond_with @beers, meta: metadata
  end

  private
  def set_partial_name
    @partial_name = '%'
    @partial_name = @partial_name + params[:term].downcase + '%' if params[:term]
    @partial_name
  end
end