class SessionsController < ApplicationController
  skip_before_action :require_authentication, only: [:new, :create]

  def new
    @session_form = SessionForm.new
  end

  def create
    @session_form = SessionForm.new(session_form_params)
    if @session_form.valid?
      user = User.find_by_email(@session_form.email)
      if user && user.authenticate(@session_form.password)
        session[:user_id] = user.id
      else
        flash.now.alert = 'Invalid email or password'
      end
    end

    respond_to do |format|
      format.html do
        if session[:user_id]
          redirect_to root_url, notice: 'Logged in!' and return
        else
          render :new
        end
      end

      format.json do
        if session[:user_id]
          render nothing: true, status: 201
        else
          render nothing: true, status: 404
        end
      end
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to root_url, notice: 'Logged out!'
  end

  private
  def session_form_params
    params.require(:session_form).permit(:email, :password)
  end
end
