module BeersHelper
  def mountain_icon(beer, with_background = false)
    class_name = 'fridge-icon' + (with_background ? ' fridge-icon-with-background' : '')
    tooltip_title = beer.in_fridge ? 'Cold' : 'Warm'
    image_tag (beer.in_fridge ? 'mountains-cold.png' : 'mountains-warm.png'), class: class_name, data: {toggle: 'tooltip', placement: 'bottom', title: tooltip_title}
  end
end
