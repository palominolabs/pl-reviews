class AuthenticatedConstraint
  def matches? (request)
    user_id = request.session[:user_id]
    return user_id && User.find_by_id(user_id)
  end
end