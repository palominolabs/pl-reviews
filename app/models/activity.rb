class Activity < ActiveRecord::Base
  scope :beer_related, -> { where(type: %w(BeerAddedActivity BeerOpenedActivity BeerReviewedActivity)) }
  scope :coffee_related, -> { where(type: %w(CoffeeAddedActivity BagOpenedActivity CoffeeReviewedActivity)) }
end
