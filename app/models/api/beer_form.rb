module Api
  class BeerForm
    include ActiveModel::Model
    attr_accessor :name, :brewery_brewerydb_id, :style_name, :style_brewerydb_id, :abv

    validates_presence_of :name
    validates_presence_of :brewery_brewerydb_id
    validates_presence_of :style_brewerydb_id
    validates_numericality_of :abv, greater_than_or_equal_to: 0, less_than_or_equal_to: 100, allow_nil: true, allow_blank: true
  end
end