class Bag < ActiveRecord::Base
  scope :in_stock, -> { where(finished_at: nil) }

  belongs_to :coffee
  has_one :bag_opened_activity, dependent: :destroy

  validates_presence_of :roasted_on
  validates_presence_of :coffee

  # Opens but does not save bag.
  # Boolean indicating if open successful (i.e. was there an unopened bag)
  def open(current_user)
    return false if self.opened_on

    self.opened_on = Date.parse(Time.now.to_s)
    self.bag_opened_activity = BagOpenedActivity.new(user: current_user, coffee: self.coffee)
    return true
  end

  # Finishes but does not save bag.
  # Boolean indicating if finish successful (i.e. was there an unfinished bag)
  def finish
    return false if self.finished_at || !self.opened_on

    self.finished_at = Date.parse(Time.now.to_s)
    return true
  end
end
