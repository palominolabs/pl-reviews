class BagOpenedActivity < Activity
  include CoffeeRelated
  belongs_to :user
  belongs_to :bag

  validates_presence_of :user
  validates_presence_of :bag
end