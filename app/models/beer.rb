class Beer < ActiveRecord::Base
  scope :cold, -> { where(in_fridge: true) }
  scope :in_stock, -> { where('inventory > 0') }

  belongs_to :brewery, counter_cache: true
  has_many :reviews, as: :reviewable, dependent: :destroy
  has_many :reviewers, through: :reviews, class_name: 'User', source: :user
  has_many :activities, dependent: :destroy

  validates_presence_of :name
  validates_presence_of :brewery
  validates_presence_of :inventory
  validate :must_have_inventory_to_be_in_fridge
  validates_presence_of :brewerydb_id
  validates_uniqueness_of :brewerydb_id
  validates_numericality_of :inventory, only_integer: true, greater_than_or_equal_to: 0

  # Custom validators
  def must_have_inventory_to_be_in_fridge
    if self.inventory && self.inventory.to_i < 1 && self.in_fridge
      errors.add(:in_fridge, 'Must have a beer in inventory to be in the fridge')
    end
  end

  # Methods
  def average_rating
    reviews.average(:rating).round(1) if reviews.any?
  end

  # Opens but does not save beer.
  # Boolean indicating if open successful (i.e. was there a bottle in stock)
  def open(current_user)
    if self.inventory > 0
      self.decrement(:inventory)
      self.in_fridge = false
      self.activities << BeerOpenedActivity.new(user: current_user)
      true
    else
      false
    end
  end

  def create_reviewed_activity(review)
    beer_reviewed_activity = BeerReviewedActivity.new(review: review)
    self.activities << beer_reviewed_activity
    beer_reviewed_activity.valid?
  end

  # Static Methods
  class << self
    def create_from_beer_form_and_brewery(beer_form, brewery)
      Beer.new({
                   name: beer_form.name,
                   brewery: brewery,
                   brewerydb_id: beer_form.beer_brewerydb_id,
                   inventory: beer_form.inventory,
                   in_fridge: beer_form.in_fridge == '1'
               })
    end
  end
end
