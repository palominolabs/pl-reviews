class BeerForm
  include ActiveModel::Model
  attr_accessor :name, :beer_brewerydb_id, :brewery_name, :brewery_brewerydb_id, :inventory, :in_fridge

  validates_presence_of :name
  validates_presence_of :beer_brewerydb_id
  validates_presence_of :brewery_name
  validates_presence_of :brewery_brewerydb_id
  validates_presence_of :inventory
  validates_numericality_of :inventory, only_integer: true, greater_than_or_equal_to: 0
  validate :must_have_inventory_to_be_in_fridge

  def must_have_inventory_to_be_in_fridge
    if self.inventory && self.inventory.to_i < 1 && self.in_fridge && self.in_fridge != '0'
      errors.add(:in_fridge, 'Must have a beer in inventory to be in the fridge')
    end
  end
end