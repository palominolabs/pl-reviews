class BeerOpenedActivity < Activity
  include BeerRelated
  belongs_to :user

  validates_presence_of :user
end