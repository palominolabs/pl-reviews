class BeerReviewedActivity < Activity
  include BeerRelated
  belongs_to :review

  validates_presence_of :review
end