class Coffee < ActiveRecord::Base
  scope :in_stock, -> { joins(:bags).where('bags.finished_at' => nil).uniq }

  belongs_to :roaster, counter_cache: true
  has_many :bags, dependent: :destroy
  has_many :activities, dependent: :destroy
  has_many :reviews, as: :reviewable, dependent: :destroy
  has_many :reviewers, through: :reviews, class_name: 'User', source: :user

  validates_presence_of :name
  validates_presence_of :roaster
  validates_inclusion_of :drip_or_espresso, in: ['drip', 'espresso'], allow_nil: true, allow_blank: true

  def create_reviewed_activity(review)
    coffee_reviewed_activity = CoffeeReviewedActivity.new(review: review, coffee: self)
    self.activities << coffee_reviewed_activity
    coffee_reviewed_activity.valid?
  end

  def bags_count
    self.bags.in_stock.count
  end
end
