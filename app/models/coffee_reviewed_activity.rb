class CoffeeReviewedActivity < Activity
  include CoffeeRelated
  belongs_to :review

  validates_presence_of :review
end