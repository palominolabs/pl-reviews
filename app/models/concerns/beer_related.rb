module BeerRelated
  extend ActiveSupport::Concern

  included do
    belongs_to :beer

    validates_presence_of :beer
    validates_presence_of :type
    validates_inclusion_of :type, in: ['BeerOpenedActivity', 'BeerAddedActivity', 'BeerReviewedActivity']
  end
end