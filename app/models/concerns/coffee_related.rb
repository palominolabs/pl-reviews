module CoffeeRelated
  extend ActiveSupport::Concern

  included do
    belongs_to :coffee

    validates_presence_of :coffee
    validates_presence_of :type
    validates_inclusion_of :type, in: ['BagOpenedActivity', 'CoffeeAddedActivity', 'CoffeeReviewedActivity']
  end
end