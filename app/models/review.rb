class Review < ActiveRecord::Base
  belongs_to :reviewable, polymorphic: true
  belongs_to :user
  has_one :beer_reviewed_activity, dependent: :destroy
  has_one :coffee_reviewed_activity, dependent: :destroy

  validates_presence_of :rating
  validates_numericality_of :rating, only_integer: true, greater_than_or_equal_to: 0, less_than_or_equal_to: 5
  validates_presence_of :reviewable
  validates_presence_of :user
end
