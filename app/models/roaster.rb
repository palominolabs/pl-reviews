class Roaster < ActiveRecord::Base
  has_many :coffees, dependent: :destroy

  validates_presence_of :name
  validates_uniqueness_of :name
end
