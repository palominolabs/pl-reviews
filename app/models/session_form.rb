class SessionForm
  include ActiveModel::Model
  attr_accessor :email, :password

  validates_presence_of :email
  validate :must_be_a_palomino_labs_email
  validates_presence_of :password

  def must_be_a_palomino_labs_email
    unless self.email && self.email.end_with?('@palominolabs.com')
      errors.add(:email, 'Must be a Palomino Labs email address')
    end
  end
end