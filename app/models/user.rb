class User < ActiveRecord::Base
  has_secure_password
  has_many :reviews, dependent: :destroy
  has_many :reviewed_beers, through: :reviews, class_name: 'Beer', source: :beer

  validates_presence_of :name
  validates_presence_of :email
  validates_uniqueness_of :email
  validate :must_be_a_palomino_labs_email
  validates_presence_of :password, on: :create
  validates_presence_of :hipchat_id
  validates_uniqueness_of :hipchat_id

  def must_be_a_palomino_labs_email
    unless self.email && self.email.end_with?('@palominolabs.com')
      errors.add(:email, 'Must be a Palomino Labs email address')
    end
  end

  def is_admin?
    return admin
  end
end
