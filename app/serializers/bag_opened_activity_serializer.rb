class BagOpenedActivitySerializer < ActiveModel::Serializer
  attributes :id, :type, :created_at, :bag_id, :coffee_id
  has_one :user
end
