class BeerOpenedActivitySerializer < ActiveModel::Serializer
  attributes :id, :type, :created_at

  has_one :user
end
