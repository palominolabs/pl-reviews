class BeerReviewedActivitySerializer < ActiveModel::Serializer
  attributes :id, :type, :created_at
  has_one :review, serializer: ReviewBaseSerializer
end
