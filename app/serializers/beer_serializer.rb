class BeerSerializer < ActiveModel::Serializer
  attributes :id, :name, :brewery_id, :inventory, :style, :description, :abv, :ibu, :image_url, :icon_url, :in_fridge
  has_one :brewery, serializer: BreweryBaseSerializer
  has_many :reviews, serializer: ReviewBaseSerializer
  has_many :activities

  def activities
    object.activities.order('activities.created_at DESC')
  end
end
