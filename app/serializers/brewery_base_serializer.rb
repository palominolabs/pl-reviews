class BreweryBaseSerializer < ActiveModel::Serializer
  self.root = :brewery
  attributes :id, :name, :icon_url, :image_url
end