class BrewerySerializer < ActiveModel::Serializer
  attributes :id, :name, :image_url, :icon_url, :beers_count
end
