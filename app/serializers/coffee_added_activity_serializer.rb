class CoffeeAddedActivitySerializer < ActiveModel::Serializer
  attributes :id, :type, :created_at, :coffee_id

  has_one :user
end
