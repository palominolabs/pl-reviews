class CoffeeReviewedActivitySerializer < ActiveModel::Serializer
  attributes :id, :type, :created_at, :coffee_id
  has_one :review
end
