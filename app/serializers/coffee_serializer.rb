class CoffeeSerializer < ActiveModel::Serializer
  attributes :id, :name, :roaster_id, :origin, :variety, :drip_or_espresso, :bags_count
  has_one :roaster
  has_many :activities

  def activities
    object.activities.order('activities.created_at DESC')
  end
end
