class ReviewBaseSerializer < ActiveModel::Serializer
  attributes :id, :rating, :comment

  has_one :user
end
