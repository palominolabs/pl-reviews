class ReviewSerializer < ActiveModel::Serializer
  attributes :id, :rating, :comment, :reviewable_id, :reviewable_type

  has_one :user
end
