module AwsService
  JPEG_FORMAT = 'image/jpeg'
  PNG_FORMAT = 'image/png'
  ACCEPTED_FORMATS = [JPEG_FORMAT, PNG_FORMAT]
  IMAGE_BUCKET_NAME = "pl-reviews-images-#{Rails.env}"
  BASE_BEER_IMAGE_NAME = 'roaster-thumb-'
  MAX_WIDTH = 500
  MAX_HEIGHT = 500

  class ImageUploadFailed < RuntimeError
  end

  class InvalidImageFormat < RuntimeError
  end

  class NoImageProvided < RuntimeError
  end

  class RoasterSaveFailed < RuntimeError
  end

  class << self
    def upload_roaster_image(image, roaster)
      if image
        if ACCEPTED_FORMATS.include?(image.content_type)
          resized_image = RmagickService.resize_image(image, MAX_WIDTH, MAX_HEIGHT)
          s3 = AWS::S3.new
          bucket = s3.buckets[IMAGE_BUCKET_NAME]

          extension = get_image_extension(image)
          s3_filename = BASE_BEER_IMAGE_NAME + roaster.id.to_s + extension

          s3_image = bucket.objects.create(s3_filename, resized_image)

          if s3_image && s3_image.public_url
            roaster.image_url = s3_image.public_url.to_s
            if roaster.save
              s3_image.public_url
            else
              raise RoasterSaveFailed
            end
          else
            raise ImageUploadFailed
          end
        else
          raise InvalidImageFormat
        end
      else
        raise NoImageProvided
      end
    end

    private
    def get_image_extension(image)
      if image.content_type == JPEG_FORMAT
        '.jpeg'
      elsif image.content_type == PNG_FORMAT
        '.png'
      end
    end
  end
end