module BrewerydbApiService

  VALID_PARAMETER_KEYS = [:name, :ids]
  BASE_URL = Rails.configuration.brewerydb.base_url.freeze
  API_KEY = Rails.configuration.brewerydb.api_key.freeze
  BREWERIES_INDEX_ENDPOINT = 'breweries'.freeze
  BREWERY_SHOW_ENDPOINT = 'brewery'.freeze
  BEERS_INDEX_ENDPOINT = 'beers'.freeze
  BEER_SHOW_ENDPOINT = 'beer'.freeze
  STYLES_INDEX_ENDPOINT = 'styles'.freeze

  class RequestFailed < RuntimeError
  end

  class BreweryNotFound < RuntimeError
  end

  class BeerNotFound < RuntimeError
  end

  class BreweryCreationFailed < RuntimeError
  end

  class BeerCreationFailed < RuntimeError
  end

  class << self
    def get_brewery(brewery_id)
      params = generate_request_parameters
      response = RestClient.get brewery_show_url(brewery_id), params: params, accept: :json, &@@handle_get_response
      data = response['data']
      if data
        brewery = {
            brewerydb_id: data['id'],
            name: data['name']
        }
        if data['images']
          brewery[:image_url] = data['images']['large']
          brewery[:icon_url] = data['images']['icon']
        end
        brewery
      else
        raise BreweryNotFound
      end
    end

    def create_brewery(brewery_form)
      params = {name: brewery_form.name}
      url = UrlUtils.add_param(breweries_index_url, 'key', API_KEY)
      response = RestClient.post url, params, &@@handle_get_response
      if response['status'] == 'success' && response['data']
        response['data']['id']
      else
        raise BreweryCreationFailed
      end
    end

    def get_brewery_autocomplete(query_params = {})
      params = generate_request_parameters query_params
      response = RestClient.get breweries_index_url, params: params, accept: :json, &@@handle_get_response
      if response['data']
        response['data'].map { |brewery| {label: brewery['name'], value: brewery['id']} }
      else
        []
      end
    end

    def get_brewery_beer_autocomplete(brewery_id)
      params = generate_request_parameters
      response = RestClient.get brewery_beers_index_url(brewery_id), params: params, accept: :json, &@@handle_get_response
      if response['data']
        response['data'].map { |beer| {label: beer['name'], value: beer['id']} }
      else
        []
      end
    end

    def get_beer(beer_id)
      params = generate_request_parameters
      response = RestClient.get beer_show_url(beer_id), params: params, accept: :json, &@@handle_get_response
      data = response['data']
      if data
        beer = {
            brewerydb_id: data['id'],
            name: data['name'],
            abv: data['abv'],
            ibu: data['ibu'],
            description: data['description'],
        }
        if data['labels']
          beer[:image_url] = data['labels']['large']
          beer[:icon_url] = data['labels']['icon']
        end

        beer[:style] =  data['style']['name'] if data['style']
        beer
      else
        raise BeerNotFound
      end
    end

    def get_beer_batch(beer_ids)
      params = generate_request_parameters ids: beer_ids.join(',')
      response = RestClient.get beers_index_url, params: params, accept: :json, &@@handle_get_response
      data = response['data']

      if data
        beers = data.map { |raw_beer|
          beer = {
              brewerydb_id: raw_beer['id'],
              name: raw_beer['name'],
              abv: raw_beer['abv'],
              ibu: raw_beer['ibu'],
              description: raw_beer['description']
          }
          beer[:style] = raw_beer['style']['name'] if raw_beer['style']

          if raw_beer['labels']
            beer[:image_url] = raw_beer['labels']['large']
            beer[:icon_url] = raw_beer['labels']['icon']
          end
          beer
        }
        beers
      else
        raise BeerNotFound
      end
    end

    def create_beer(beer_form)
      params = {
          name: beer_form.name,
          brewery: beer_form.brewery_brewerydb_id,
          styleId: beer_form.style_brewerydb_id
      }
      params[:abv] = beer_form.abv unless beer_form.abv.nil? || beer_form.abv.empty?

      url = UrlUtils.add_param(beers_index_url, 'key', API_KEY)
      response = RestClient.post url, params, &@@handle_get_response
      if response['status'] == 'success' && response['data']
        response['data']['id']
      else
        raise BeerCreationFailed
      end
    end

    def get_style_autocomplete
      params = generate_request_parameters
      response = RestClient.get styles_index_url, params: params, accept: :json, &@@handle_get_response
      if response['data']
        response['data'].map { |style| {label: style['name'], value: style['id']} }
      else
        []
      end
    end
  end

  @@handle_get_response = -> (response, request, result, &block) {
    case response.code
      when 200
        JSON.parse(response.to_s)
      when 201
        if request.method == :post
          JSON.parse(response.to_s)
        else
          raise RequestFailed
        end
      else
        # Any other response codes are undefined behavior from the API spec
        raise RequestFailed
    end
  }

  def self.build_url(path)
    BASE_URL + path
  end

  private_class_method :build_url

  def self.generate_request_parameters(query_params = {})
    request_params = query_params.slice(*VALID_PARAMETER_KEYS)
    request_params[:key] = API_KEY
    request_params
  end

  private_class_method :generate_request_parameters

  def self.breweries_index_url
    build_url BREWERIES_INDEX_ENDPOINT
  end

  private_class_method :breweries_index_url

  def self.brewery_show_url(brewery_id)
    build_url BREWERY_SHOW_ENDPOINT + "/#{brewery_id}"
  end

  private_class_method :brewery_show_url

  def self.brewery_beers_index_url(brewery_id)
    build_url BREWERY_SHOW_ENDPOINT + "/#{brewery_id}/" + BEERS_INDEX_ENDPOINT
  end

  private_class_method :brewery_beers_index_url

  def self.beer_show_url(beer_id)
    build_url BEER_SHOW_ENDPOINT + "/#{beer_id}"
  end

  private_class_method :beer_show_url

  def self.beers_index_url
    build_url BEERS_INDEX_ENDPOINT
  end

  private_class_method :beers_index_url

  def self.styles_index_url
    build_url STYLES_INDEX_ENDPOINT
  end

  private_class_method :styles_index_url
end