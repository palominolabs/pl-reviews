class HipchatApiService
  API_KEY = Rails.configuration.hipchat.api_key.freeze
  API_VERSION = 'v2'.freeze
  DEFAULT_ROOM = Rails.configuration.hipchat.room.freeze
  DEFAULT_COLOR = 'purple'.freeze
  DEFAULT_FORMAT = 'text'.freeze
  DEFAULT_NOTIFY = Rails.configuration.hipchat.notify.freeze
  USERNAME = 'PL Reviews'.freeze
  HIPCHAT_VIEW_PATH = Rails.root.join('app', 'views', 'admin', 'hipchat').freeze
  BEER_OPEN_FILE = 'beer_opened.text.erb'.freeze

  class HipchatContext < Erubis::Context
    include Rails.application.routes.url_helpers

    def default_url_options
      ActionMailer::Base.default_url_options
    end
  end

  class << self
    def send_beer_opened_message(beer, user)
      message_file = File.read(HIPCHAT_VIEW_PATH.join(BEER_OPEN_FILE))
      beer_opened_file = Erubis::Eruby.new(message_file)
      message = beer_opened_file.evaluate(HipchatContext.new(beer: beer, user: user))

      client = HipChat::Client.new(API_KEY, api_version: API_VERSION)
      client[DEFAULT_ROOM].send(USERNAME, message, notify: DEFAULT_NOTIFY, color: DEFAULT_COLOR, message_format: DEFAULT_FORMAT)
    end
  end
end