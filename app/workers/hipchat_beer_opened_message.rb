class HipchatBeerOpenedMessage
  @queue = :messages_queue

  def self.perform(beer_id, user_id)
    beer = Beer.find(beer_id)
    user = User.find(user_id)

    HipchatApiService::send_beer_opened_message(beer, user)
  end
end