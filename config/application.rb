require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(:default, Rails.env)

module PLReviews
  class Application < Rails::Application
    config.assets.paths << Rails.root.join('app', 'assets', 'fonts')
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run 'rake -D time' for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de

    config.assets.precompile += ['admin/main.js', 'widget/main.js']

    config.brewerydb = ActiveSupport::OrderedOptions.new
    config.brewerydb.api_key = 'YOUR_BREWERY_DB_KEY_HERE'
    config.brewerydb.base_url = 'http://api.brewerydb.com/v2/'

    config.iFrameConfig = ActiveSupport::OrderedOptions.new
    config.iFrameConfig.x_frame_options = 'ALLOW-FROM http://palominolabs.com'

    config.hipchat = ActiveSupport::OrderedOptions.new
    config.hipchat.api_key = 'YOUR_HIPCHAT_API_KEY_HERE'
    config.hipchat.room = 'YOUR_ROOM_NAME_HERE'
    config.hipchat.notify = false
  end
end
