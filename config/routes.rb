PLReviews::Application.routes.draw do
  mount JasmineRails::Engine => '/specs' if defined?(JasmineRails)
  mount JasmineFixtureServer => '/spec/javascripts/fixtures' if defined?(Jasmine::Jquery::Rails::Engine)

  constraints(AuthenticatedConstraint.new) do
    mount Resque::Server, at: '/resque'
  end

  get 'log_in', to: 'sessions#new', as: 'log_in'
  get 'log_out', to: 'sessions#destroy', as: 'log_out'
  get 'sign_up', to: 'users#new', as: 'sign_up'

  # Search routes
  get 'search/autocomplete', to: 'search#autocomplete', as: 'search_autocomplete'
  get 'search/beers', to: 'search#beers', as: 'search_beers'
  get 'search', to: 'search#index', as: 'search'

  # Beer custom routes
  get 'beers/sync', to: 'beers#sync', as: 'sync_beers'
  get 'beers/:id/sync', to: 'beers#sync_beer', as: 'sync_beer'
  get 'breweries/sync', to: 'breweries#sync', as: 'sync_breweries'
  get 'breweries/:id/sync', to: 'breweries#sync_brewery', as: 'sync_brewery'

  get 'beers/random', to: 'beers#random', as: 'random_beer'
  post 'beers/:id/open', to: 'beers#open', as: 'open_beer'

  root to: 'beers#index'

  # BreweryDB API routes
  namespace :api do
    get 'brewery/autocomplete', to: 'breweries#brewery_autocomplete', as: 'brewery_autocomplete'
    get 'brewery/:id/beers', to: 'breweries#brewery_beer_autocomplete', as: 'brewery_beer_autocomplete'
    resources :breweries, only: [:create, :new] do
      resources :beers, only: [:create, :new]
    end
    get 'beers/beer_styles_autocomplete', to: 'beers#beer_styles_autocomplete', as: 'beer_styles_autocomplete'
  end

  # Beer and brewery routes
  resources :beers do
    resources :reviews, only: [:create, :destroy]
  end

  resources :breweries, only: [:index, :show, :new, :create, :destroy] do
    resources :beers, only: [:index, :new]
  end

  # Coffee and Roaster routes
  resources :roasters do
    resources :coffees, only: [:index, :new]
  end

  resources :coffees do
    resources :bags, only: [:index, :new, :create]
    resources :reviews, only: [:create, :destroy]
  end

  post 'bags/:id/open', to: 'bags#open', as: 'open_bag'
  post 'bags/:id/finish', to: 'bags#finish', as: 'finish_bag'
  resources :bags, only: [:destroy]

  # Common routes
  resources :activities, only: [:index, :show]
  resources :sessions, only: [:new, :create, :destroy]
end