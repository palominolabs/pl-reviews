class AddBrewerydbIdToBreweries < ActiveRecord::Migration
  def change
    add_column :breweries, :brewerydb_id, :string
  end
end
