class AddBrewerydbIdToBeers < ActiveRecord::Migration
  def change
    add_column :beers, :brewerydb_id, :string
  end
end
