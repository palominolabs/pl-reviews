class AddIconUrlToBreweries < ActiveRecord::Migration
  def change
    add_column :breweries, :icon_url, :string
  end
end
