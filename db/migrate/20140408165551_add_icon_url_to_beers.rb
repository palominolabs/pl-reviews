class AddIconUrlToBeers < ActiveRecord::Migration
  def change
    add_column :beers, :icon_url, :string
  end
end
