class RemoveDescriptionFromBeers < ActiveRecord::Migration
  def change
    remove_column :beers, :description, :string
    add_column :beers, :description, :text
  end
end
