class AddInFridgeToBeers < ActiveRecord::Migration
  def change
    add_column :beers, :in_fridge, :boolean
  end
end
