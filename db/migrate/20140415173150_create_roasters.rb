class CreateRoasters < ActiveRecord::Migration
  def change
    create_table :roasters do |t|
      t.string :name
      t.string :location
      t.string :image_url

      t.timestamps
    end
  end
end
