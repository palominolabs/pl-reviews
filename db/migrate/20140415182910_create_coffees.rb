class CreateCoffees < ActiveRecord::Migration
  def change
    create_table :coffees do |t|
      t.string :name
      t.string :origin
      t.string :variety
      t.string :drip_or_espresso
      t.references :roaster, index: true

      t.timestamps
    end
  end
end
