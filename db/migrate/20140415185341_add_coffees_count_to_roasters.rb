class AddCoffeesCountToRoasters < ActiveRecord::Migration
  def change
    add_column :roasters, :coffees_count, :integer
  end
end
