class CreateBags < ActiveRecord::Migration
  def change
    create_table :bags do |t|
      t.date :roasted_on
      t.date :opened_on
      t.references :coffee, index: true

      t.timestamps
    end
  end
end
