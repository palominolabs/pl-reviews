class AddBagsCountToCoffee < ActiveRecord::Migration
  def change
    add_column :coffees, :bags_count, :integer
  end
end
