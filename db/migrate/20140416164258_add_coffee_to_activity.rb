class AddCoffeeToActivity < ActiveRecord::Migration
  def change
    add_reference :activities, :coffee, index: true
  end
end
