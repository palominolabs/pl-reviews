class AddBagToActivity < ActiveRecord::Migration
  def change
    add_reference :activities, :bag, index: true
  end
end
