class AddReviewableIdAndReviewableTypeToReview < ActiveRecord::Migration
  def change
    add_belongs_to :reviews, :reviewable, polymorphic: true
    add_index :reviews, [:reviewable_id, :reviewable_type]
  end
end
