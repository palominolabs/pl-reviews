class MoveBeersToReviewableInReviews < ActiveRecord::Migration
  def up
    reviews = select_all('SELECT * FROM reviews')
    reviews.each do |review|
      update("UPDATE reviews SET reviewable_id=#{review['beer_id']}, reviewable_type='Beer' WHERE id=#{review['id']}")
    end
  end

  def down
    reviews = select_all('SELECT * FROM reviews')
    reviews.each do |review|
      update("UPDATE reviews SET beer_id=#{review['reviewable_id']}, reviewable_type=NULL WHERE id=#{review['id']}")
    end
  end
end
