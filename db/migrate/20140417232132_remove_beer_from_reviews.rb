class RemoveBeerFromReviews < ActiveRecord::Migration
  def change
    remove_reference :reviews, :beer, index: true
  end
end
