class AddFinishedAtToBags < ActiveRecord::Migration
  def change
    add_column :bags, :finished_at, :date
  end
end
