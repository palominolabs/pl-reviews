class RemoveBagsCountFromCoffees < ActiveRecord::Migration
  def change
    remove_column :coffees, :bags_count, :integer
  end
end
