class AddHipchatIdToUsers < ActiveRecord::Migration
  def change
    add_column :users, :hipchat_id, :string
  end
end
