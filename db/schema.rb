# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140429155914) do

  create_table "activities", force: true do |t|
    t.string   "type"
    t.integer  "beer_id"
    t.integer  "user_id"
    t.integer  "review_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "coffee_id"
    t.integer  "bag_id"
  end

  add_index "activities", ["bag_id"], name: "index_activities_on_bag_id"
  add_index "activities", ["beer_id"], name: "index_activities_on_beer_id"
  add_index "activities", ["coffee_id"], name: "index_activities_on_coffee_id"
  add_index "activities", ["review_id"], name: "index_activities_on_review_id"
  add_index "activities", ["user_id"], name: "index_activities_on_user_id"

  create_table "bags", force: true do |t|
    t.date     "roasted_on"
    t.date     "opened_on"
    t.integer  "coffee_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.date     "finished_at"
  end

  add_index "bags", ["coffee_id"], name: "index_bags_on_coffee_id"

  create_table "beers", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "brewery_id"
    t.integer  "inventory"
    t.integer  "reviews_count"
    t.string   "image_url"
    t.string   "brewerydb_id"
    t.decimal  "abv"
    t.string   "icon_url"
    t.string   "style"
    t.integer  "ibu"
    t.text     "description"
    t.boolean  "in_fridge"
  end

  add_index "beers", ["brewery_id"], name: "index_beers_on_brewery_id"

  create_table "breweries", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "beers_count"
    t.string   "brewerydb_id"
    t.string   "image_url"
    t.string   "icon_url"
  end

  create_table "coffees", force: true do |t|
    t.string   "name"
    t.string   "origin"
    t.string   "variety"
    t.string   "drip_or_espresso"
    t.integer  "roaster_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "coffees", ["roaster_id"], name: "index_coffees_on_roaster_id"

  create_table "reviews", force: true do |t|
    t.integer  "rating"
    t.text     "comment"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.integer  "reviewable_id"
    t.string   "reviewable_type"
  end

  add_index "reviews", ["reviewable_id", "reviewable_type"], name: "index_reviews_on_reviewable_id_and_reviewable_type"
  add_index "reviews", ["user_id"], name: "index_reviews_on_user_id"

  create_table "roasters", force: true do |t|
    t.string   "name"
    t.string   "location"
    t.string   "image_url"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "coffees_count"
  end

  create_table "users", force: true do |t|
    t.string   "email"
    t.string   "password_digest"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.string   "hipchat_id"
    t.boolean  "admin"
  end

end
