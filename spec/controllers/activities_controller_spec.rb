require 'spec_helper'

describe ActivitiesController do
  describe 'GET index' do
    it 'does not require authentication' do
      controller.should_not_receive :require_authentication
      get :index, {}
    end

    it 'assigns @activities all activities' do
      beer_added_activity = FactoryGirl.create(:beer_added_activity)
      get :index, {}
      assigns(:activities).should eq [beer_added_activity]
    end

    it 'assigns @filter to All' do
      get :index, {}
      assigns(:filter).should eq 'All'
    end

    context 'supports paging' do

      context 'page size of 1' do
        before do
          @page_size = Activity.instance_variable_get(:@_default_per_page)
          Activity.paginates_per(1)
        end

        after do
          Activity.paginates_per(@page_size)
        end

        it 'returns specified page of activities' do
          activity_a = FactoryGirl.create(:beer_added_activity)
          activity_b = FactoryGirl.create(:beer_added_activity)
          get :index, {page: 1}
          assigns(:activities).should eq [activity_b]
          get :index, {page: 2}
          assigns(:activities).should eq [activity_a]
          get :index, {page: 3}
          assigns(:activities).should eq []
        end
      end

      it 'sets metadata for total_count and page' do
        FactoryGirl.create(:beer_added_activity)
        FactoryGirl.create(:beer_added_activity)
        FactoryGirl.create(:beer_added_activity)
        controller.should_receive(:respond_with).with(anything(), {meta: {total_count: 3, page: 1}})
        get :index
      end
    end

    context 'supports filtering by type' do
      it 'assigns @activities only the activities of the specified type' do
        beer_opened_activity = FactoryGirl.create(:beer_opened_activity)
        FactoryGirl.create(:beer_added_activity)
        FactoryGirl.create(:beer_reviewed_activity)
        get :index, {type: 'BeerOpenedActivity'}
        assigns(:activities).should eq [beer_opened_activity]
      end
    end

    context 'supports filtering by category' do
      before do
        @beer_added_activity = FactoryGirl.create(:beer_added_activity)
        @beer_opened_activity= FactoryGirl.create(:beer_opened_activity)
        @beer_reviewed_activity = FactoryGirl.create(:beer_reviewed_activity)
        @bag_opened_activity = FactoryGirl.create(:bag_opened_activity)
        @coffee_added_activity = FactoryGirl.create(:coffee_added_activity)
        @coffee_reviewed_activity = FactoryGirl.create(:coffee_reviewed_activity)
      end

      it 'assigns @activities only the coffee activities' do
        get :index, {coffee: '1'}
        assigns(:activities).should eq [@coffee_reviewed_activity, @coffee_added_activity, @bag_opened_activity]
        assigns(:filter).should eq 'Coffee'
      end

      it 'assigns @activities only the beer activities' do
        get :index, {beer: '1'}
        assigns(:activities).should eq [@beer_reviewed_activity, @beer_opened_activity, @beer_added_activity]
        assigns(:filter).should eq 'Beer'
      end
    end
  end

  describe 'GET show' do
    before do
      @beer_added_activity = FactoryGirl.create(:beer_added_activity)
    end

    it 'does not require authentication' do
      controller.should_not_receive :require_authentication
      get :show, {id: @beer_added_activity.id}
    end

    it 'should set @activity' do
      get :show, {id: @beer_added_activity.id}
      assigns(:activity).should be_a BeerAddedActivity
      assigns(:activity).should eql @beer_added_activity
    end
  end
end