require 'spec_helper'

describe Api::BeersController do
  describe 'GET new' do
    before do
      BrewerydbApiService.stub(:get_brewery).and_return({name: 'The Rare Barrel'})
    end

    it 'requires authentication' do
      controller.should_receive :require_authentication
      get :new, brewery_id: 'bd_id'
    end

    context 'with authenticated user' do
      before do
        controller.stub(:require_authentication)
      end

      it 'assigns @brewery_name' do
        BrewerydbApiService.should_receive(:get_brewery).with('bd_id').and_return({name: 'The Rare Barrel'})
        get :new, brewery_id: 'bd_id'
      end

      it 'assigns @beer_form to a BeerForm' do
        get :new, brewery_id: 'bd_id'
        assigns(:beer_form).should be_a Api::BeerForm
      end

      it 'assigns @brewery_name to an input parameter' do
        get :new, brewery_name: 'The Rare Barrel', brewery_id: 'bd_id'
        assigns(:brewery_name).should eql 'The Rare Barrel'
      end
    end
  end

  describe 'POST create' do
    before do
      BrewerydbApiService.stub(:get_brewery).and_return({name: 'The Rare Barrel'})
      BrewerydbApiService.stub(:create_beer).and_return('fake_id')
      @beer_form_attributes = FactoryGirl.attributes_for(:api_beer_form)
    end

    it 'requires authentication' do
      controller.should_receive :require_authentication
      post :create, api_beer_form: @beer_form_attributes, brewery_id: 'bd_id'
    end

    context 'with authenticated user' do
      before do
        controller.stub(:require_authentication)
      end

      it 'assigns @brewery_name' do
        BrewerydbApiService.should_receive(:get_brewery).with('bd_id').and_return({name: 'The Rare Barrel'})
        post :create, api_beer_form: @beer_form_attributes, brewery_id: 'bd_id'
      end

      it 'assigns @beer_form' do
        post :create, api_beer_form: @beer_form_attributes, brewery_id: 'bd_id'
        assigns(:beer_form).should be_a Api::BeerForm
      end

      it 'makes a request to BrewerydbApiService.create_beer' do
        BrewerydbApiService.should_receive(:create_beer).and_return('fake_id')
        post :create, api_beer_form: @beer_form_attributes, brewery_id: 'bd_id'
      end

      it 'flashes a notice and redirects to new_beer_path' do
        redirect_params = {
            from_api: true,
            beer_form: {
                name: @beer_form_attributes[:name],
                beer_brewerydb_id: 'fake_id',
                brewery_name: 'The Rare Barrel',
                brewery_brewerydb_id: 'bd_id'
            }
        }
        post :create, api_beer_form: @beer_form_attributes, brewery_id: 'bd_id'
        should set_the_flash[:notice].to 'Beer successfully added to BreweryDB'
        should redirect_to new_beer_path(redirect_params)
      end

      it 'rescues RequestFailed RuntimeError and flashes an alert' do
        BrewerydbApiService.should_receive(:create_beer).and_raise(BrewerydbApiService::RequestFailed)
        post :create, api_beer_form: @beer_form_attributes, brewery_id: 'bd_id'
        should set_the_flash[:alert].now.to 'Failed to save beer to BreweryDB'
        should render_template :new
      end

      it 'rescues BeerCreationFailed RuntimeError and flashes an alert' do
        BrewerydbApiService.should_receive(:create_beer).and_raise(BrewerydbApiService::BeerCreationFailed)
        post :create, api_beer_form: @beer_form_attributes, brewery_id: 'bd_id'
        should set_the_flash[:alert].now.to 'Failed to save beer to BreweryDB'
        should render_template :new
      end

      it 'renders new if beer_form invalid' do
        post :create, api_beer_form: FactoryGirl.attributes_for(:api_beer_form, name: nil), brewery_id: 'bd_id'
        should render_template :new
      end
    end
  end

  describe 'GET get_style_autocomplete' do
    it 'requires authentication' do
      BrewerydbApiService.stub(:get_style_autocomplete).and_return('')
      controller.should_receive :require_authentication
      get :beer_styles_autocomplete, format: :json
    end

    it 'calls get_style_autocomplete' do
      controller.stub(:require_authentication)
      BrewerydbApiService.should_receive(:get_style_autocomplete).and_return([])
      get :beer_styles_autocomplete, format: :json
    end
  end
end