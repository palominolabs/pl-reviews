require 'spec_helper'

describe Api::BreweriesController do
  describe 'GET brewery_autocomplete' do
    before do
      @api_response = ['Stone Brewery', 'The Bruery', 'The Rare Barrel']
      BrewerydbApiService.should_receive(:get_brewery_autocomplete).with(name: '*test*').and_return(@api_response)
    end
    it 'requires authentication' do
      session[:user_id] = nil
      controller.should_receive :require_authentication
      get :brewery_autocomplete, format: :json, term: 'test'
    end

    context 'with authenticated session' do
      before do
        controller.stub(:require_authentication)
      end

      it 'should response with a json array of brewery names' do
        get :brewery_autocomplete, format: :json, term: 'test'

        expect(response.body).to eql @api_response.to_json
      end
    end
  end

  describe 'GET brewery_beer_names' do
    before do
      @api_response = ['Ruination', 'IPA', 'Arrogant Bastard']
      BrewerydbApiService.should_receive(:get_brewery_beer_autocomplete).with('test_id').and_return(@api_response)
    end
    it 'requires authentication' do
      session[:user_id] = nil
      controller.should_receive :require_authentication
      get :brewery_beer_autocomplete, format: :json, id: 'test_id'
    end

    context 'with authenticated session' do
      before do
        controller.stub(:require_authentication)
      end

      it 'should response with a json array of brewery names' do
        get :brewery_beer_autocomplete, format: :json, id: 'test_id'

        expect(response.body).to eql @api_response.to_json
      end
    end
  end

  describe 'GET new' do
    it 'requires authentication' do
      controller.should_receive :require_authentication
      get :new
    end

    it 'assigns @brewery_form to a brewery_form' do
      controller.stub(:require_authentication)
      get :new
      assigns(:brewery_form).should be_a Api::BreweryForm
    end
  end

  describe 'POST create' do
    before do
      BrewerydbApiService.stub(:create_brewery).and_return('id')
      @brewery_form_attributes = FactoryGirl.attributes_for(:api_brewery_form)
    end

    it 'requires authentication' do
      controller.should_receive :require_authentication
      post :create, api_brewery_form: @brewery_form_attributes
    end

    context 'with authorized user' do
      before do
        controller.stub(:require_authentication)
      end

      it 'assigns @brewery_form to a brewery_form' do
        post :create, api_brewery_form: @brewery_form_attributes
        assigns(:brewery_form).should be_a Api::BreweryForm
        assigns(:brewery_form).name.should eql 'The Rare Barrel'
      end

      it 'calls BrewerydbApiService.create_brewery' do
        BrewerydbApiService.should_receive(:create_brewery).and_return('id')
        post :create, api_brewery_form: @brewery_form_attributes
      end

      it 'flashes a notice and redirects on success' do
        post :create, api_brewery_form: @brewery_form_attributes
        should set_the_flash[:notice].to 'Brewery successfully added to BreweryDB'
        should redirect_to new_api_brewery_beer_path('id')
      end

      it 'renders new if brewery_form invalid' do
        post :create, api_brewery_form: {name: nil}
        should render_template :new
      end

      it 'rescues RequestFailed, flashes notice, and renders new' do
        BrewerydbApiService.stub(:create_brewery).and_raise(BrewerydbApiService::RequestFailed)
        post :create, api_brewery_form: @brewery_form_attributes
        should set_the_flash[:alert].now.to 'Failed to save brewery to BreweryDB'
        should render_template :new
      end

      it 'rescues BreweryCreationFailed, flashes notice, and renders new' do
        BrewerydbApiService.stub(:create_brewery).and_raise(BrewerydbApiService::BreweryCreationFailed)
        post :create, api_brewery_form: @brewery_form_attributes
        should set_the_flash[:alert].now.to 'Failed to save brewery to BreweryDB'
        should render_template :new
      end
    end
  end
end