require 'spec_helper'

describe ApplicationController do
  class SpecApplicationController < ApplicationController ; end

  controller(SpecApplicationController) do
    def index
      flash.now[:info] = 'Authenticated'
      render nothing: true
    end
  end

  describe '#require_authentication' do
    context 'session has no :user_id' do
      it 'should do nothing' do
        session[:user_id] = nil
        get :index
        should set_the_flash[:error].to 'You must be logged in to access this section.'
        should redirect_to log_in_url
      end

      it 'returns a status of 401 for JSON' do
        get :index, format: :json
        response.status.should eql 401
      end
    end

    context 'session has a :user_id' do
      it 'should flash and redirect to log_in_url' do
        user = FactoryGirl.create(:user)
        session[:user_id] = user.id
        get :index
        should set_the_flash[:info].now.to 'Authenticated'
        assigns[:current_user].should be_a User
        assigns[:current_user].email.should eql user.email
        should_not redirect_to log_in_url
      end
    end
  end

  describe '#json_request?' do
    it 'should skip :verify_authenticity_token if format is json' do
      controller.should_not_receive(:verify_authenticity_token)
      get :index, format: :json
    end

    it 'should call :verify_authenticity_token for other formats' do
      controller.should_receive(:verify_authenticity_token)
      get :index
    end
  end
end