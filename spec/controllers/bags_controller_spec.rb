require 'spec_helper'

describe BagsController do
  before do
    @coffee = FactoryGirl.create(:coffee)
  end

  describe 'GET index' do

    it 'does not require authentication' do
      controller.should_not_receive :require_authentication
      get :index, coffee_id: @coffee.id
    end

    it 'assigns @bags' do
      bag = FactoryGirl.create(:bag, coffee: @coffee)
      get :index, coffee_id: @coffee.id
      assigns(:bags).should eq [bag]
    end

    it 'assigns @coffee' do
      get :index, coffee_id: @coffee.id
      assigns(:coffee).should eq @coffee
    end

    it 'filters the bags by coffee' do
      FactoryGirl.create(:bag)
      FactoryGirl.create(:bag)
      bag = FactoryGirl.create(:bag, coffee: @coffee)
      get :index, coffee_id: @coffee.id
      assigns(:bags).should eq [bag]
    end
  end

  describe 'GET new' do
    it 'requires authentication' do
      controller.should_receive :require_authentication
      get :new, coffee_id: @coffee.id
    end

    context 'with authenticated user' do
      before do
        controller.stub(:require_authentication)
      end

      it 'assigns @bag to a new bag' do
        get :new, coffee_id: @coffee.id
        assigns(:bag).should be_a_new Bag
      end
    end
  end

  describe 'POST create' do
    it 'requires authentication' do
      controller.should_receive :require_authentication
      post :create, bag: FactoryGirl.attributes_for(:bag), coffee_id: @coffee.id
    end

    context 'with authenticated user' do
      before do
        controller.stub(:require_authentication)
      end

      it 'assigns @coffee' do
        post :create, bag: FactoryGirl.attributes_for(:bag), coffee_id: @coffee.id
        assigns(:coffee).should eq @coffee
      end

      it 'assigns @bag to a new bag with the provided parameters' do
        bag_attributes = FactoryGirl.attributes_for(:bag)
        post :create, bag: bag_attributes, coffee_id: @coffee.id
        assigns(:bag).should be_a Bag
        assigns(:bag).roasted_on.to_s.should eql bag_attributes[:roasted_on]
        assigns(:bag).opened_on.to_s.should eql bag_attributes[:opened_on]
      end

      it 'redirects to @bag with a notice on successful save' do
        Bag.any_instance.should_receive(:save).and_return(true)
        post :create, bag: FactoryGirl.attributes_for(:bag), coffee_id: @coffee.id
        should set_the_flash[:notice].to 'Bag was successfully created.'
        should redirect_to(Bag.last)
      end

      it 'flashes an alert and renders new on failed save' do
        Bag.any_instance.should_receive(:save).and_return(false)
        post :create, bag: FactoryGirl.attributes_for(:bag), coffee_id: @coffee.id
        should set_the_flash[:alert].now.to 'Failed to save bag.'
        should render_template :new
      end

      context 'if opened' do
        it 'creates a BagOpenedActivity' do
          post :create, bag: FactoryGirl.attributes_for(:bag), coffee_id: @coffee.id
          assigns(:bag).bag_opened_activity.should be_a BagOpenedActivity
        end
      end
    end
  end

  describe 'DELETE destroy' do
    before do
      @bag = FactoryGirl.create(:bag, coffee: @coffee)
    end

    it 'requires authentication' do
      controller.should_receive :require_authentication
      delete :destroy, id: @bag.to_param, coffee_id: @coffee.id
    end

    context 'with authenticated session' do
      before do
        controller.stub(:require_authentication)
      end

      it 'destroys the requested bag' do
        expect {
          delete :destroy, {id: @bag.to_param, coffee_id: @coffee.id}
        }.to change(Bag, :count).by(-1)
      end

      it 'redirects to the bag list' do
        delete :destroy, {id: @bag.to_param, coffee_id: @coffee.id}
        response.should redirect_to(coffee_bags_path(@coffee))
      end
    end
  end

  describe 'GET open' do
    before do
      @bag = FactoryGirl.create(:bag)
    end

    it 'requires authentication' do
      controller.should_receive :require_authentication
     get :open, id: @bag.to_param
    end

    context 'with authenticated session' do
      before do
        controller.stub(:require_authentication)
      end

      it 'assigns @bag to the specified bag' do
        get :open, id: @bag.to_param
        assigns(:bag).should eql @bag
      end

      it 'flashes a notice if the bag is already opened' do
        Bag.any_instance.stub(:open).and_return(false)
        get :open, id: @bag.to_param
        should set_the_flash[:notice].to 'Bag was already opened.'
      end

      context 'with an unopened bag' do
        before do
          Bag.any_instance.stub(:open).and_return(true)
          @bag.opened_on = nil
          @bag.save
        end

        it 'flashes a notice when successfully saved' do
          get :open, id: @bag.to_param
          should set_the_flash[:notice].to 'Bag was successfully opened.'
        end

        it 'flashes an alert when bag fails to save' do
          Bag.any_instance.stub(:save).and_return(false)
          get :open, id: @bag.to_param
          should set_the_flash[:alert].to 'Failed to open bag.'
        end

        it 'redirects to the coffee bags index' do
          get :open, id: @bag.to_param
          should redirect_to(coffee_bags_path(@bag.coffee))
        end
      end
    end
  end

  describe 'GET finish' do
    before do
      @bag = FactoryGirl.create(:bag)
    end

    it 'requires authentication' do
      controller.should_receive :require_authentication
      get :finish, id: @bag.to_param
    end

    context 'with authenticated session' do
      before do
        controller.stub(:require_authentication)
      end

      it 'assigns @bag to the specified bag' do
        get :finish, id: @bag.to_param
        assigns(:bag).should eql @bag
      end

      it 'flashes a notice if the bag is already finished' do
        Bag.any_instance.stub(:finish).and_return(false)
        get :finish, id: @bag.to_param
        should set_the_flash[:notice].to 'Bag was already finished or not opened yet.'
      end

      context 'with an unfinished bag' do
        before do
          Bag.any_instance.stub(:finish).and_return(true)
          @bag.finished_at = nil
          @bag.save
        end

        it 'flashes a notice when successfully saved' do
          get :finish, id: @bag.to_param
          should set_the_flash[:notice].to 'Bag was successfully finished.'
        end

        it 'flashes an alert when bag fails to save' do
          Bag.any_instance.stub(:save).and_return(false)
          get :finish, id: @bag.to_param
          should set_the_flash[:alert].to 'Failed to finish bag.'
        end

        it 'redirects to the coffee bags index' do
          get :finish, id: @bag.to_param
          should redirect_to(coffee_bags_path(@bag.coffee))
        end
      end
    end
  end
end