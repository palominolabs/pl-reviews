require 'spec_helper'

describe BeersController do

  describe 'GET index' do
    it 'does not require authentication' do
      controller.should_not_receive :require_authentication
      get :index, {}
    end

    it 'assigns all beers as @beers' do
      beer = FactoryGirl.create(:beer)
      get :index, {}
      assigns(:beers).should eq [beer]
    end

    context 'brewery_id specified' do
      before do
        @brewery = FactoryGirl.create(:brewery)
        @beer_in_brewery = FactoryGirl.create(:beer, {brewery: @brewery})
        FactoryGirl.create(:beer, {brewery: FactoryGirl.build(:brewery)})
        get :index, {brewery_id: @brewery.id}
      end

      it 'assigns @brewery to the specified brewery' do
        assigns(:brewery).should eq @brewery
      end

      it 'assigns all beers associated with the brewery to @beers' do
        assigns(:beers).should eq [@beer_in_brewery]
      end
    end

    context 'in_stock filter set' do
      before do
        @beer_with_inventory = FactoryGirl.create(:beer, {inventory: 1})
        FactoryGirl.create(:beer, {inventory: 0})
        get :index, {in_stock: 1}
      end

      it 'assigns all beers that have inventory > 0 to @beers' do
        assigns(:beers).should eq [@beer_with_inventory]
      end

      it 'assigns @in_stock to the string 1' do
        assigns(:in_stock).should eq '1'
      end
    end

    context 'in_fridge filter set' do
      before do
        @beer_in_fridge = FactoryGirl.create(:beer, {inventory: 1, in_fridge: true})
        FactoryGirl.create(:beer, {inventory: 1})
        get :index, {in_fridge: 1}
      end

      it 'assigns all beers that have in_fridge == true to @beers' do
        assigns(:beers).should eq [@beer_in_fridge]
      end

      it 'assigns @in_fridge to the string 1' do
        assigns(:in_fridge).should eq '1'
      end
    end

    context 'supports paging' do
      context 'restricting page size to 1' do
        before do
          @page_size = Beer.instance_variable_get(:@_default_per_page)
          Beer.paginates_per(1)
        end

        after do
          Beer.paginates_per(@page_size)
        end

        it 'returns specified page of beers' do
          beer_a = FactoryGirl.create(:beer, {name: 'a'})
          beer_b = FactoryGirl.create(:beer, {name: 'b'})
          get :index, {page: 1}
          assigns(:beers).should eq [beer_a]
          get :index, {page: 2}
          assigns(:beers).should eq [beer_b]
          get :index, {page: 3}
          assigns(:beers).should eq []
        end
      end

      it 'sets metadata for total_count and page' do
        FactoryGirl.create(:beer)
        FactoryGirl.create(:beer)
        FactoryGirl.create(:beer)
        controller.should_receive(:respond_with).with(anything(), {meta: {total_count: 3, page: 1}})
        get :index
      end

      it 'supports page_size parameter' do
        beer_1 = FactoryGirl.create(:beer)
        beer_2 = FactoryGirl.create(:beer)
        FactoryGirl.create(:beer)
        FactoryGirl.create(:beer)

        expected_beers = [beer_1, beer_2]
        controller.should_receive(:respond_with).with(expected_beers, {meta: {total_count: 4, page: 1}})
        get :index, page_size: 2
      end
    end

    context 'paging disabled' do
      before do
        @page_size = Beer.instance_variable_get(:@_default_per_page)
        Beer.paginates_per(1)
      end

      after do
        Beer.paginates_per(@page_size)
      end
      it 'should set @beers to be all beers' do
        beer_a = FactoryGirl.create(:beer, {name: 'a'})
        beer_b = FactoryGirl.create(:beer, {name: 'b'})
        get :index, {page: 1, no_paging: 1}
        assigns(:beers).should eq [beer_a, beer_b]
      end
    end
  end

  describe 'GET show' do
    it 'does not require authentication' do
      controller.should_not_receive :require_authentication
      beer = FactoryGirl.create(:beer)
      get :show, {id: beer.to_param}
    end

    it 'assigns the requested beer as @beer' do
      beer = FactoryGirl.create(:beer)
      get :show, {id: beer.to_param}
      assigns(:beer).should eq beer
    end

    it 'assigns the requested beer as @reviewable' do
      beer = FactoryGirl.create(:beer)
      get :show, {id: beer.to_param}
      assigns(:reviewable).should eq beer
    end

    it 'assigns a new review to @review' do
      beer = FactoryGirl.create(:beer)
      get :show, {id: beer.to_param}
      assigns(:review).should be_a_new Review
    end
  end

  describe 'GET new' do
    it 'requires authentication' do
      controller.should_receive :require_authentication
      get :new, {}
    end

    it 'assigns a beer_form as @beer_form' do
      controller.stub(:require_authentication)
      get :new, {}
      assigns(:beer_form).should be_a BeerForm
    end

    it 'assigns the beer_form parameter in as @beer_form and @from_api' do
      controller.stub(:require_authentication)
      beer_form_attributes = FactoryGirl.attributes_for(:beer_form)
      get :new, {from_api: true, beer_form: beer_form_attributes}
      assigns(:beer_form).should be_a BeerForm
      assigns(:beer_form).name.should eql beer_form_attributes[:name]
    end
  end

  describe 'GET edit' do
    it 'requires authentication' do
      beer = FactoryGirl.create(:beer)
      controller.should_receive :require_authentication
      get :edit, {id: beer.id}
    end

    it 'assigns the requested beer as @beer' do
      controller.stub(:require_authentication)
      beer = FactoryGirl.create(:beer)
      get :edit, {id: beer.to_param}
      assigns(:beer).should eq beer
    end
  end

  describe 'POST create' do
    before do
      BrewerydbApiService.stub(:get_brewery).and_return(FactoryGirl.attributes_for(:brewery))
      BrewerydbApiService.stub(:get_beer)
      @brewery = FactoryGirl.create(:brewery)
      @beer_form_attributes = FactoryGirl.attributes_for(:beer_form, brewery_id: @brewery.id)
    end

    it 'requires authentication' do
      controller.should_receive :require_authentication
      post :create, {beer_form: @beer_form_attributes}
    end

    context 'with authorized user' do
      before do
        controller.stub(:require_authentication)
      end

      context 'with valid params' do
        before do
          user = FactoryGirl.build(:user)
          controller.stub(:current_user).and_return(user)
          controller.stub(:set_brewery_beer)
          controller.instance_variable_set('@brewery_beer', {
              'name' => 'db_name',
              'id' => 'db_id',
              'labels' => {
                  'large' => 'db_large_image'
              }
          })
        end

        context 'with new brewery' do
          it 'creates and saves a new brewery' do
            expect {
              post :create, {beer_form: @beer_form_attributes}
            }.to change(Brewery, :count).by 1
          end

          context 'new brewery fails to save' do
            it 'flashes an alert and renders new' do
              Brewery.any_instance.stub(:save).and_return(false)
              post :create, {beer_form: @beer_form_attributes}
              should set_the_flash[:alert].now.to 'Failed to save brewery'
              should render_template :new
            end
          end
        end

        context 'with a known brewery' do
          before do
            Brewery.should_receive(:find_by_brewerydb_id).and_return(FactoryGirl.build(:brewery))
          end

          it 'creates a new Beer' do
            expect {
              post :create, {beer_form: @beer_form_attributes}
            }.to change(Beer, :count).by 1
          end

          it 'creates a beer_added_activity' do
            Brewery.stub(:save).and_return(true)
            expect {
              post :create, {beer_form: @beer_form_attributes}
            }.to change(BeerAddedActivity, :count).by 1
          end

          it 'redirects to the created beer' do
            post :create, {beer_form: @beer_form_attributes}
            should redirect_to Beer.last
          end
        end
      end

      context 'with invalid beer_added_activity' do
        it 're-renders the \'new\' template' do
          controller.stub(:current_user).and_return(nil)
          post :create, {beer_form: @beer_form_attributes}
          should render_template :new
        end
      end
    end

    context 'with invalid params' do
      before do
        controller.stub(:require_authentication)
      end

      it 'assigns a newly created but unsaved beer_form as @beer_form' do
        # Trigger the behavior that occurs when invalid params are submitted
        Beer.any_instance.stub(:save).and_return(false)
        post :create, {beer_form: {name: 'invalid value'}}
        assigns(:beer_form).should be_a BeerForm
      end

      it 're-renders the \'new\' template' do
        # Trigger the behavior that occurs when invalid params are submitted
        Beer.any_instance.stub(:save).and_return(false)
        post :create, {beer_form: {name: 'invalid value'}}
        should render_template :new
      end
    end

    context 'with BrewerydbApiService RuntimeErrors' do
      before do
        controller.stub(:require_authentication)
      end

      it 'rescues BeerNotFound and adds an error' do
        BrewerydbApiService.stub(:get_beer).and_raise(BrewerydbApiService::BeerNotFound)
        post :create, {beer_form: @beer_form_attributes}
        assigns(:beer_form).errors[:beer_name].should eql ['Beer name must match existing beer for the specified brewery in BreweryDB']
      end

      it 'rescues BreweryNotFound and adds an error' do
        BrewerydbApiService.stub(:get_brewery).and_raise(BrewerydbApiService::BreweryNotFound)
        post :create, {beer_form: @beer_form_attributes}
        assigns(:beer_form).errors[:brewery_name].should eql ['Brewery name must match existing brewery in BreweryDB']
      end

      it 'rescues RequestFailed and flashes an alert' do
        BrewerydbApiService.stub(:get_brewery).and_raise(BrewerydbApiService::RequestFailed)
        post :create, {beer_form: @beer_form_attributes}
        should set_the_flash[:alert].now.to 'Failed to get brewery or beer from BreweryDB. Please check that both names are valid'
      end
    end
  end

  describe 'PUT update' do
    it 'requires authentication' do
      @brewery = FactoryGirl.create(:beer)
      @beer = FactoryGirl.create(:beer)
      controller.should_receive :require_authentication
      put :update, {id: @beer.to_param, beer: {inventory: 1}}
    end

    context 'with valid params' do
      before do
        controller.stub(:require_authentication)
        @beer = FactoryGirl.create(:beer)
        @beer_attributes = FactoryGirl.attributes_for(:beer_form, {inventory: 1})
      end

      it 'updates the requested beer' do
        Beer.any_instance.should_receive(:save)
        put :update, {id: @beer.id, beer: @beer_attributes}
      end

      it 'assigns the requested beer as @beer' do
        put :update, {id: @beer.to_param, beer: @beer_attributes}
        assigns(:beer).should be_a Beer
        assigns(:beer).id.should eql @beer.id
      end

      it 'redirects to the beer' do
        put :update, {id: @beer.to_param, beer: @beer_attributes}
        should redirect_to @beer
      end
    end

    context 'with invalid params' do
      before do
        controller.stub(:require_authentication)
        @beer = FactoryGirl.create(:beer)
      end

      it 'assigns the beer as @beer' do
        # Trigger the behavior that occurs when invalid params are submitted
        Beer.any_instance.stub(:save).and_return(false)
        put :update, {id: @beer.to_param, beer: {inventory: 'invalid value'}}
        assigns(:beer).should eq @beer
      end

      it 're-renders the \'edit\' template' do
        # Trigger the behavior that occurs when invalid params are submitted
        Beer.any_instance.stub(:save).and_return(false)
        put :update, {id: @beer.to_param, beer: {inventory: 'invalid value'}}
        should render_template :edit
      end
    end
  end

  describe 'GET open' do
    before do
      @beer = FactoryGirl.create(:beer, name: 'test_beer')
    end
    it 'requires authentication' do
      controller.should_receive :require_authentication
      get :open, id: @beer.id
    end

    context 'with authorized user' do
      before do
        @user = FactoryGirl.create(:user, admin: true)
        controller.stub(:require_authentication)
        controller.stub(:current_user).and_return(@user)
        Resque.stub(:enqueue)
        Beer.any_instance.stub(:open)
        Beer.any_instance.stub(:save)
      end

      it 'assigns @beer to the specified beer' do
        get :open, id: @beer.id
        assigns(:beer).should be_a Beer
        assigns(:beer).name.should eql @beer.name
      end

      it 'assigns @review to a new review' do
        get :open, id: @beer.id
        assigns(:review).should be_new_record
      end

      it 'redirects to @beer' do
        get :open, id: @beer.id
        should redirect_to(@beer)
      end

      it 'calls @beer.open' do
        Beer.any_instance.should_receive(:open).with(@user)
        get :open, id: @beer.id
      end

      context 'user is admin and they pass a hipchat_id' do
        it 'calls @beer.open with the specified user' do
          hipchat_user = FactoryGirl.create(:user)
          Beer.any_instance.should_receive(:open).with(hipchat_user)
          get :open, id: @beer.id, hipchat_id: hipchat_user.hipchat_id
        end
      end

      context 'with a successful open' do
        before do
          Beer.any_instance.stub(:open).and_return(true)
        end

        it 'calls @beer.save' do
          Beer.any_instance.should_receive(:save)
          get :open, id: @beer.id
        end

        context 'with a successful save' do
          before do
            Beer.any_instance.stub(:save).and_return(true)
          end

          it 'queues a hipchat message' do
            Resque.should_receive(:enqueue).with(HipchatBeerOpenedMessage, @beer.id, @user.id)
            get :open, id: @beer.id
          end

          context 'user is admin and they pass a hipchat_id' do
            it 'queues a hipchat message with specified user' do
              hipchat_user = FactoryGirl.create(:user)
              Resque.should_receive(:enqueue).with(HipchatBeerOpenedMessage, @beer.id, hipchat_user.id)
              get :open, id: @beer.id, hipchat_id: hipchat_user.hipchat_id
            end
          end

          it 'flashes a notice' do
            get :open, id: @beer.id
            should set_the_flash[:notice].to 'A bottle of test_beer was successfully opened.'
          end

          it 'responds with a JSON blob of the beer' do
            get :open, id: @beer.id, format: :json

            serializer = BeerSerializer.new @beer
            response.body.should eq serializer.to_json
          end
        end

        context 'a with failed save' do
          it 'flashes a failure alert' do
            Beer.any_instance.stub(:save).and_return(false)
            get :open, id: @beer.id
            should set_the_flash[:alert].to 'Failed to open a bottle of test_beer.'
          end

          it 'responds with a 500 to JSON requests' do
            get :open, id: @beer.id, format: :json
            response.response_code.should eql 500
          end
        end
      end

      context 'with a failed open' do
        it 'flashes an alert' do
          Beer.any_instance.stub(:open).and_return(false)
          get :open, id: @beer.id
          should set_the_flash[:alert].to 'No bottles of test_beer left to open.'
        end

        it 'responds with a 422 to JSON requests' do
          get :open, id: @beer.id, format: :json
          response.response_code.should eql 422
        end
      end
    end
  end

  describe 'DELETE destroy' do
    before do
      brewery = FactoryGirl.create(:brewery)
      @beer = FactoryGirl.create(:beer, brewery_id: brewery.id)
    end

    it 'requires authentication' do
      controller.should_receive :require_authentication

      delete :destroy, id: @beer.id
    end

    context 'with authenticated user' do
      before do
        controller.stub(:require_authentication)
      end

      it 'destroys the requested beer' do
        expect {
          delete :destroy, id: @beer.id
        }.to change(Beer, :count).by -1
      end

      it 'flashes a notice' do
        delete :destroy, id: @beer.id
        should set_the_flash[:notice].to 'Beer successfully deleted'
      end

      it 'flashes an alert if delete fails' do
        Beer.any_instance.stub(:destroy).and_return(false)
        delete :destroy, id: @beer.id
        should set_the_flash[:alert].to 'Failed to delete beer'
      end

      it 'redirects to the beers list' do
        delete :destroy, id: @beer.id
        should redirect_to beers_url
      end

      it 'redirects to the brewery beers list if brewery_id set' do
        delete :destroy, id: @beer.id, brewery_id: @beer.brewery_id
        should redirect_to brewery_beers_path(@beer.brewery)
      end
    end
  end

  describe 'GET sync' do
    it 'requires authentication' do
      FactoryGirl.create(:beer, brewerydb_id: 'db_id')
      BrewerydbApiService.should_receive(:get_beer_batch).and_return([FactoryGirl.attributes_for(:beer, brewerydb_id: 'db_id')])
      controller.should_receive :require_authentication
      get :sync
    end

    context 'with authenticated user' do
      before do
        Beer.any_instance.stub(:save).and_return(true)
        Beer.any_instance.stub(:changed?).and_return(true)
        controller.stub(:require_authentication)
      end

      context 'successful sync' do
        before do
          FactoryGirl.create(:beer, brewerydb_id: 'db_id')
          BrewerydbApiService.should_receive(:get_beer_batch).and_return([FactoryGirl.attributes_for(:beer, brewerydb_id: 'db_id')])
        end

        it 'should set a notice flash' do
          get :sync
          should set_the_flash.to '1 beer updated when syncing with Brewery DB'
        end

        it 'should redirect to beers_url' do
          get :sync
          should redirect_to beers_url
        end
      end

      context 'batch request fails' do
        it 'rescues BeerNotFound and flashes an alert' do
          FactoryGirl.create(:beer, {brewerydb_id: 'bd_id'})
          BrewerydbApiService.stub(:get_beer_batch).and_raise(BrewerydbApiService::BeerNotFound)
          get :sync
          should set_the_flash[:alert].to 'Failed to sync bd_id'
        end
      end
    end
  end

  describe 'GET sync_beer' do
    before do
      @beer = FactoryGirl.create(:beer, brewerydb_id: 'db_id')
    end

    it 'requires authentication' do
      BrewerydbApiService.should_receive(:get_beer).and_return(FactoryGirl.attributes_for(:beer, brewerydb_id: 'db_id'))
      controller.should_receive :require_authentication
      get :sync_beer, id: @beer.id
    end

    context 'with authorized user' do
      before do
        controller.stub(:require_authentication)
        Beer.any_instance.stub(:save).and_return(true)
        Beer.any_instance.stub(:changed?).and_return(true)
      end

      context 'with successful response from brewerydb' do
        before do
          BrewerydbApiService.should_receive(:get_beer).and_return(FactoryGirl.attributes_for(:beer, brewerydb_id: 'db_id'))
        end

        it 'assigns the specified beer to @beer' do
          get :sync_beer, id: @beer.id
          assigns(:beer).should eql @beer
        end

        it 'should flash a notice if there are no new changes' do
          Beer.any_instance.stub(:changed?).and_return(false)
          get :sync_beer, id: @beer.id
          should set_the_flash[:notice].to 'No new changes to beer!'
        end

        it 'should flash an alert if the beer fails to save' do
          Beer.any_instance.stub(:save).and_return(false)
          get :sync_beer, id: @beer.id
          should set_the_flash[:alert].to 'Beer failed to save!'
        end

        it 'should flash a notice if the beer updates succesfully' do
          get :sync_beer, id: @beer.id
          should set_the_flash[:notice].to 'Beer successfully synced!'
        end

        it 'should redirect to @beer' do
          get :sync_beer, id: @beer.id
          should redirect_to @beer
        end
      end

      context 'with RuntimeErrors from brewerydb' do
        it 'rescues BeerNotFound, flashes an alert, and redirects to @beer' do
          BrewerydbApiService.should_receive(:get_beer).and_raise(BrewerydbApiService::BeerNotFound)
          get :sync_beer, id: @beer.id
          should set_the_flash[:alert].to 'Failed to fetch latest udpates from BreweryDB'
          should redirect_to @beer
        end

        it 'rescues RequestFailed, flashes an alert, and redirects to @beer' do
          BrewerydbApiService.should_receive(:get_beer).and_raise(BrewerydbApiService::RequestFailed)
          get :sync_beer, id: @beer.id
          should set_the_flash[:alert].to 'Failed to fetch latest udpates from BreweryDB'
          should redirect_to @beer
        end
      end
    end
  end

  describe 'GET random' do
    before do
      @beer = FactoryGirl.create(:beer, {inventory: 1, in_fridge: true})
    end

    it 'does not require authentication' do
      controller.should_not_receive :require_authentication
      get :random
    end

    context 'html format' do
      it 'redirects to a random cold beer' do
        get :random
        should redirect_to @beer
      end

      it 'flashes an alert and redirects to beers_path if no cold beers' do
        @beer.in_fridge = false
        @beer.save

        get :random
        should set_the_flash[:alert].to 'No cold beers to choose from!'
        should redirect_to beers_path
      end
    end

    context 'json format' do
      it 'redirects to a random cold beer' do
        get(:random, format: :json)
        serializer = BeerSerializer.new @beer
        response.body.should eq serializer.to_json
      end

      it 'return a 404' do
        @beer.in_fridge = false
        @beer.save

        get :random, format: :json
        response.response_code.should eql 404
      end
    end
  end
end
