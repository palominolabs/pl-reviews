require 'spec_helper'

describe BreweriesController do
  before do
    @valid_attributes = FactoryGirl.attributes_for(:brewery)
  end

  describe 'GET index' do
    it 'does not require authentication' do
      controller.should_not_receive :require_authentication
      get :index, {}
    end

    it 'assigns all breweries as @breweries' do
      brewery = FactoryGirl.create(:brewery)
      get :index, {}
      assigns(:breweries).should eq([brewery])
    end

    context 'supports paging' do
      context 'set paging to 1' do
        before do
          @page_size = Brewery.instance_variable_get(:@_default_per_page)
          Brewery.paginates_per(1)
        end

        after do
          Brewery.paginates_per(@page_size)
        end

        it 'returns specified page of breweries' do
          brewery_a = FactoryGirl.create(:brewery, {name: 'a'})
          brewery_b = FactoryGirl.create(:brewery, {name: 'b'})
          get :index, {page: 1}
          assigns(:breweries).should eq [brewery_a]
          get :index, {page: 2}
          assigns(:breweries).should eq [brewery_b]
          get :index, {page: 3}
          assigns(:breweries).should eq []
        end
      end

      it 'sets metadata for total_count and page' do
        FactoryGirl.create(:brewery)
        FactoryGirl.create(:brewery)
        FactoryGirl.create(:brewery)
        controller.should_receive(:respond_with).with(anything(), {meta: {total_count: 3, page: 1}})
        get :index
      end
    end
  end

  describe 'GET show' do
    before do
      @brewery = Brewery.create! @valid_attributes
    end

    it 'does not require authentication' do
      controller.should_not_receive :require_authentication
      get :show, {id: @brewery.to_param}
    end

    it 'assigns the requested brewery as @brewery' do
      get :show, {id: @brewery.to_param}
      assigns(:brewery).should eq(@brewery)
    end
  end

  describe 'GET new' do
    it 'requires authentication' do
      session[:user_id] = nil
      controller.should_receive :require_authentication
      get :new, {}
    end

    context 'with authenticated session' do
      it 'assigns a new brewery as @brewery' do
        controller.stub(:require_authentication)
        get :new, {}
        assigns(:brewery).should be_a_new(Brewery)
      end
    end
  end

  describe 'POST create' do
    before do
      Brewery.any_instance.stub(:get_latest_from_brewerydb)
    end

    it 'requires authentication' do
      controller.should_receive :require_authentication
      post :create, {brewery: @valid_attributes}
    end

    context 'with authenticated session' do
      before do
        controller.stub(:require_authentication)
      end

      describe 'with valid params' do
        it 'calls get_latest_from_brewerydb' do
          BrewerydbApiService.should_receive(:get_brewery)
          post :create, {brewery: @valid_attributes}
        end

        context 'breweryDB finds the specified brewery' do
          before do
            BrewerydbApiService.stub(:get_brewery)
          end
          it 'creates a new Brewery' do
            expect {
              post :create, {brewery: @valid_attributes}
            }.to change(Brewery, :count).by(1)
          end



          it 'assigns a newly created brewery as @brewery' do
            post :create, {brewery: @valid_attributes}
            assigns(:brewery).should be_a(Brewery)
            assigns(:brewery).should be_persisted
          end

          it 'redirects to the created brewery' do
            post :create, {brewery: @valid_attributes}
            response.should redirect_to(Brewery.last)
          end
        end

        context 'breweryDB throws an error' do
          it 'rescues BreweryNotFound RuntimeError and adds an error to @brewery' do
            BrewerydbApiService.stub(:get_brewery).and_raise(BrewerydbApiService::BreweryNotFound)
            post :create, {brewery: @valid_attributes}
            assigns(:brewery).errors[:name].should eql ['Brewery name must match existing brewery in BreweryDB']
          end

          it 'rescues BreweryNotFound RuntimeError and adds an error to @brewery' do
            BrewerydbApiService.stub(:get_brewery).and_raise(BrewerydbApiService::RequestFailed)
            post :create, {brewery: @valid_attributes}
            assigns(:brewery).errors[:name].should eql ['Failed to get brewery from BreweryDB']
          end
        end
      end

      describe 'with invalid params' do
        it 'assigns a newly created but unsaved brewery as @brewery' do
          # Trigger the behavior that occurs when invalid params are submitted
          Brewery.any_instance.stub(:save).and_return(false)
          post :create, {brewery: {name: 'invalid value'}}
          assigns(:brewery).should be_a_new(Brewery)
        end

        it 're-renders the \'new\' template' do
          # Trigger the behavior that occurs when invalid params are submitted
          Brewery.any_instance.stub(:save).and_return(false)
          post :create, {brewery: {name: 'invalid value'}}
          response.should render_template('new')
        end
      end
    end
  end

  describe 'DELETE destroy' do
    before do
      @brewery = Brewery.create! @valid_attributes
    end

    it 'requires authentication' do
      session[:user_id] = nil
      controller.should_receive :require_authentication
      delete :destroy, {id: @brewery.to_param}
    end

    context 'with authenticated session' do
      before do
        controller.stub(:require_authentication)
      end
      it 'destroys the requested brewery' do
        expect {
          delete :destroy, {id: @brewery.to_param}
        }.to change(Brewery, :count).by(-1)
      end

      it 'redirects to the breweries list' do
        delete :destroy, {id: @brewery.to_param}
        response.should redirect_to(breweries_url)
      end
    end
  end

  describe 'GET sync' do
    before do
      BrewerydbApiService.stub(:get_brewery).and_return(FactoryGirl.attributes_for(:brewery))
    end

    it 'requires authentication' do
      controller.should_receive :require_authentication
      get :sync
    end

    context 'with authenticated user' do
      before do
        FactoryGirl.create(:brewery)
        FactoryGirl.create(:brewery)
        FactoryGirl.create(:brewery)
        FactoryGirl.create(:brewery)
        Brewery.any_instance.stub(:save).and_return(true)
        Brewery.any_instance.stub(:changed?).and_return(true)
        controller.stub(:require_authentication)
      end

      it 'should set a notice flash' do
        get :sync
        should set_the_flash.to '4 breweries updated when syncing with Brewery DB'
      end

      it 'should redirect to breweries_url' do
        get :sync
        should redirect_to breweries_url
      end
    end
  end

  describe 'GET sync_brewery' do
    before do
      @brewery = FactoryGirl.create(:brewery, brewerydb_id: 'db_id')
    end

    it 'requires authentication' do
      BrewerydbApiService.should_receive(:get_brewery).and_return(FactoryGirl.attributes_for(:brewery, brewerydb_id: 'db_id'))
      controller.should_receive :require_authentication
      get :sync_brewery, id: @brewery.id
    end

    context 'with authorized user' do
      before do
        controller.stub(:require_authentication)
        Brewery.any_instance.stub(:save).and_return(true)
        Brewery.any_instance.stub(:changed?).and_return(true)
      end

      context 'with successful response from brewerydb' do
        before do
          BrewerydbApiService.should_receive(:get_brewery).and_return(FactoryGirl.attributes_for(:brewery, brewerydb_id: 'db_id'))
        end

        it 'assigns the specified brewery to @brewery' do
          get :sync_brewery, id: @brewery.id
          assigns(:brewery).should eql @brewery
        end

        it 'should flash a notice if there are no new changes' do
          Brewery.any_instance.stub(:changed?).and_return(false)
          get :sync_brewery, id: @brewery.id
          should set_the_flash[:notice].to 'No new changes to brewery!'
        end

        it 'should flash an alert if the brewery fails to save' do
          Brewery.any_instance.stub(:save).and_return(false)
          get :sync_brewery, id: @brewery.id
          should set_the_flash[:alert].to 'Brewery failed to save!'
        end

        it 'should flash a notice if the brewery updates succesfully' do
          get :sync_brewery, id: @brewery.id
          should set_the_flash[:notice].to 'Brewery successfully synced!'
        end

        it 'should redirect to @brewery' do
          get :sync_brewery, id: @brewery.id
          should redirect_to @brewery
        end
      end

      context 'with RuntimeErrors from brewerydb' do
        it 'rescues breweryNotFound, flashes an alert, and redirects to @brewery' do
          BrewerydbApiService.should_receive(:get_brewery).and_raise(BrewerydbApiService::BreweryNotFound)
          get :sync_brewery, id: @brewery.id
          should set_the_flash[:alert].to 'Failed to fetch latest udpates from BreweryDB'
          should redirect_to @brewery
        end

        it 'rescues RequestFailed, flashes an alert, and redirects to @brewery' do
          BrewerydbApiService.should_receive(:get_brewery).and_raise(BrewerydbApiService::RequestFailed)
          get :sync_brewery, id: @brewery.id
          should set_the_flash[:alert].to 'Failed to fetch latest udpates from BreweryDB'
          should redirect_to @brewery
        end
      end
    end
  end
end
