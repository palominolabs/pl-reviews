require 'spec_helper'

describe CoffeesController do
  describe 'GET index' do
    it 'does not require authentication' do
      controller.should_not_receive :require_authentication
      get :index, {}
    end

    it 'assigns @coffees' do
      coffee = FactoryGirl.create(:coffee)
      get :index
      assigns(:coffees).should eq [coffee]
    end

    it 'assigns @roaster' do
      roaster = FactoryGirl.create(:roaster)
      get :index, roaster_id: roaster.id
      assigns(:roaster).should eq roaster
    end

    it 'filters the coffees by roaster' do
      FactoryGirl.create(:coffee)
      FactoryGirl.create(:coffee)
      roaster = FactoryGirl.create(:roaster)
      coffee = FactoryGirl.create(:coffee, roaster: roaster)
      get :index, roaster_id: roaster.id
      assigns(:coffees).should eq [coffee]
    end

    context 'supports paging' do
      context 'page size of 1' do
        before do
          @page_size = Coffee.instance_variable_get(:@_default_per_page)
          Coffee.paginates_per(1)
        end

        after do
          Coffee.paginates_per(@page_size)
        end

        it 'returns specified page of coffees' do
          coffee_a = FactoryGirl.create(:coffee)
          coffee_b = FactoryGirl.create(:coffee)
          get :index, {page: 1}
          assigns(:coffees).should eq [coffee_a]
          get :index, {page: 2}
          assigns(:coffees).should eq [coffee_b]
          get :index, {page: 3}
          assigns(:coffees).should eq []
        end
      end

      it 'sets metadata for total_count and page' do
        FactoryGirl.create(:coffee)
        FactoryGirl.create(:coffee)
        FactoryGirl.create(:coffee)
        controller.should_receive(:respond_with).with(anything(), {meta: {total_count: 3, page: 1}})
        get :index
      end
    end

    context 'paging disabled' do
      before do
        @page_size = Coffee.instance_variable_get(:@_default_per_page)
        Coffee.paginates_per(1)
      end

      after do
        Coffee.paginates_per(@page_size)
      end
      it 'should set @beers to be all beers' do
        coffee_a = FactoryGirl.create(:coffee)
        coffee_b = FactoryGirl.create(:coffee)
        get :index, {page: 1, no_paging: 1}
        assigns(:coffees).should eq [coffee_a, coffee_b]
      end
    end

    context 'supports in_stock filtering' do
      it 'should set @in_stock to 1' do
        get :index, in_stock: '1'
        assigns(:in_stock).should eq '1'
      end

      it 'returns only coffees that are in_stock' do
        coffee = FactoryGirl.create(:coffee)
        FactoryGirl.create(:bag, coffee: coffee)
        FactoryGirl.create(:coffee)
        FactoryGirl.create(:coffee)
        get :index, in_stock: '1'
        assigns(:coffees).should eq [coffee]
      end
    end
  end

  describe 'GET show' do
    before do
      @coffee = FactoryGirl.create(:coffee)
    end

    it 'does not require authentication' do
      controller.should_not_receive :require_authentication
      get :show, id: @coffee.id
    end

    it 'assigns @coffee to the specified coffee' do
      get :show, id: @coffee.id
      assigns(:coffee).should eql @coffee
    end

    it 'assigns @review to the specified coffee' do
      get :show, id: @coffee.id
      assigns(:review).should be_a_new Review
    end

    it 'assigns @reviewable to the specified coffee' do
      get :show, id: @coffee.id
      assigns(:reviewable).should eql @coffee
    end
  end

  describe 'GET new' do
    it 'requires authentication' do
      controller.should_receive :require_authentication
      get :new, {}
    end

    context 'with authenticated user' do
      before do
        controller.stub(:require_authentication)
      end

      it 'assigns @coffee to a new coffee' do
        get :new
        assigns(:coffee).should be_a_new Coffee
      end

      it 'assigns @roasters to all roasters' do
        roaster = FactoryGirl.create(:roaster)
        get :new
        assigns(:roasters).should eq [roaster]
      end

      it 'assigns @roaster' do
        roaster = FactoryGirl.create(:roaster)
        post :new, roaster_id: roaster.id
        assigns(:roaster).should eq roaster
      end
    end
  end

  describe 'POST create' do
    it 'requires authentication' do
      controller.should_receive :require_authentication
      post :create, coffee: FactoryGirl.attributes_for(:coffee)
    end

    context 'with authenticated user' do
      before do
        controller.stub(:require_authentication)
      end

      it 'assigns @coffee to a new coffee with the provided parameters' do
        coffee_attributes = FactoryGirl.attributes_for(:coffee)
        post :create, coffee: coffee_attributes
        assigns(:coffee).should be_a Coffee
        assigns(:coffee).name.should eql coffee_attributes[:name]
        assigns(:coffee).origin.should eql coffee_attributes[:origin]
        assigns(:coffee).roaster_id.should eql coffee_attributes[:roaster_id]
        assigns(:coffee).variety.should eql coffee_attributes[:variety]
        assigns(:coffee).drip_or_espresso.should eql coffee_attributes[:drip_or_espresso]
      end

      it 'assigns @roasters to all roasters' do
        roaster = FactoryGirl.create(:roaster)
        post :create, coffee: FactoryGirl.attributes_for(:coffee)
        assigns(:roasters).should eq [roaster]
      end

      it 'assigns @roaster' do
        roaster = FactoryGirl.create(:roaster)
        post :create, coffee: FactoryGirl.attributes_for(:coffee), roaster_id: roaster.id
        assigns(:roaster).should eq roaster
      end

      it 'creates a CoffeeAddedActivity associated with the coffee' do
        coffee_attributes = FactoryGirl.attributes_for(:coffee)
        post :create, coffee: coffee_attributes
        assigns(:coffee).activities.last.should be_a CoffeeAddedActivity
      end

      it 'redirects to @coffee with a notice on successful save' do
        Coffee.any_instance.should_receive(:save).and_return(true)
        post :create, coffee: FactoryGirl.attributes_for(:coffee)
        should set_the_flash[:notice].to 'Coffee was successfully created.'
        should redirect_to(Coffee.last)
      end

      it 'flashes an alert and renders new on failed save' do
        Coffee.any_instance.should_receive(:save).and_return(false)
        post :create, coffee: FactoryGirl.attributes_for(:coffee)
        should set_the_flash[:alert].now.to 'Failed to save coffee.'
        should render_template :new
      end
    end
  end

  describe 'GET edit' do
    before do
      @coffee = FactoryGirl.create(:coffee)
    end

    it 'requires authentication' do
      controller.should_receive :require_authentication
      get :edit, {id: @coffee.to_param}
    end

    context 'with authenticated session' do
      before do
        controller.stub(:require_authentication)
      end

      it 'assigns @coffee to the specified coffee' do
        get :edit, {id: @coffee.to_param}
        assigns(:coffee).should eql @coffee
      end

      it 'assigns @roasters to all roasters' do
        get :edit, {id: @coffee.to_param}
        assigns(:roasters).should eq [@coffee.roaster]
      end
    end
  end

  describe 'PUT update' do
    before do
      @coffee = FactoryGirl.create(:coffee)
      @update_properties = FactoryGirl.attributes_for(:coffee, {name: 'updated name'})
    end

    it 'requires authentication' do
      controller.should_receive :require_authentication
      post :update, {id: @coffee.to_param, coffee: @update_properties}
    end

    context 'with authenticated session' do
      before do
        controller.stub(:require_authentication)
      end

      it 'assigns @coffee to the specified coffee' do
        post :update, {id: @coffee.to_param, coffee: @update_properties}
        assigns(:coffee).should eql @coffee
      end

      it 'assigns @roasters to all roasters' do
        post :update, {id: @coffee.to_param, coffee: @update_properties}
        assigns(:roasters).should eq [@coffee.roaster]
      end

      context 'on successful update' do
        it 'updates the requested coffee' do
          post :update, {id: @coffee.to_param, coffee: @update_properties}
          assigns(:coffee).name.should eq 'updated name'
        end

        it 'redirects to @coffee with notice' do
          post :update, {id: @coffee.to_param, coffee: @update_properties}
          should redirect_to @coffee
          should set_the_flash[:notice].to 'Coffee was successfully updated.'
        end
      end

      context 'on failed update' do
        it 'renders edit' do
          Coffee.any_instance.should_receive(:update).and_return(false)
          post :update, {id: @coffee.to_param, coffee: @update_properties}
          should render_template :edit
        end
      end
    end
  end

  describe 'DELETE destroy' do
    before do
      @coffee = FactoryGirl.create(:coffee)
    end

    it 'requires authentication' do
      controller.should_receive :require_authentication
      delete :destroy, {id: @coffee.to_param}
    end

    context 'with authenticated session' do
      before do
        controller.stub(:require_authentication)
      end

      it 'destroys the requested coffee' do
        expect {
          delete :destroy, {id: @coffee.to_param}
        }.to change(Coffee, :count).by(-1)
      end

      it 'redirects to the coffee list' do
        delete :destroy, {id: @coffee.to_param}
        response.should redirect_to(coffees_url)
      end
    end
  end
end
