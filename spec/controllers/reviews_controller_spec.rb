require 'spec_helper'

describe ReviewsController do

  describe 'POST create' do
    before do
      @beer = FactoryGirl.create(:beer)
    end

    it 'requires authentication' do
      valid_attributes = FactoryGirl.attributes_for(:review, { beer: nil, beer_id: @beer.id })
      controller.should_receive :require_authentication
      post :create, review: valid_attributes, beer_id: @beer.id
    end

    context 'with valid params' do
      before do
        controller.stub(:require_authentication)
        @user = FactoryGirl.create(:user)
        @valid_attributes = FactoryGirl.attributes_for(:review, { beer: nil, beer_id: @beer.id })
        session[:user_id] = @user.id
      end

      it 'creates a new Review' do
        expect {
          post :create, review: @valid_attributes, beer_id: @beer.id
        }.to change(Review, :count).by 1
      end

      it 'assigns a newly created review as @review' do
        post :create, review: @valid_attributes, beer_id: @beer.id
        assigns(:review).should be_a Review
        assigns(:review).should be_persisted
        assigns(:review).reviewable.should eq @beer
      end

      it 'assigns @reviewable to the specified model' do
        post :create, review: @valid_attributes, beer_id: @beer.id
        assigns(:reviewable).should eq @beer
      end

      it 'creates a BeerReviewedActivity' do
        post :create, review: @valid_attributes, beer_id: @beer.id
        @beer.activities.length.should be 1
        @beer.activities.first.should be_a BeerReviewedActivity
      end

      it 'flashes a notice on success' do
        post :create, review: @valid_attributes, beer_id: @beer.id
        should set_the_flash[:notice].to 'Review was successfully created.'
      end

      it 'flashes an alert on failure' do
        Review.any_instance.stub(:save).and_return(false)
        post :create, review: @valid_attributes, beer_id: @beer.id
        should set_the_flash[:alert].to 'Failed to save review.'
      end

      it 'flashes an alert on invalid activity' do
        Beer.any_instance.stub(:create_reviewed_activity).and_return(false)
        post :create, review: @valid_attributes, beer_id: @beer.id
        should set_the_flash[:alert].to 'Invalid review activity.'
      end

      it 'redirects to the associated beer' do
        post :create, review: @valid_attributes, beer_id: @beer.id
        should redirect_to @beer
      end
    end
  end

  describe 'DELETE destroy' do
    before do
      @beer = FactoryGirl.create(:beer)
      @review = FactoryGirl.create(:review, { reviewable: @beer })
    end

    it 'requires authentication' do
      controller.should_receive :require_authentication
      delete :destroy, id: @review.id, beer_id: @beer.id
    end

    context 'the logged in user is the review creator' do
      before do
        session[:user_id] = @review.user.id
        controller.stub(:require_authentication)
      end


      it 'destroys the requested review' do
        expect {
          delete :destroy, id: @review.id, beer_id: @beer.id
        }.to change(Review, :count).by -1
      end

      it 'assigns the target beer to @reviewable' do
        delete :destroy, id: @review.id, beer_id: @beer.id

        assigns(:reviewable).should eq @beer
      end

      it 'render beer#show' do
        delete :destroy, id: @review.id, beer_id: @beer.id

        should redirect_to @beer
      end

      context 'review fails to be destroyed' do
        it 'should flash an alert' do
          Review.any_instance.stub(:destroy).and_return(false)
          delete :destroy, id: @review.id, beer_id: @beer.id
          should set_the_flash[:alert].now.to 'Failed to delete review'
        end
      end
    end

    context 'the logged in user is not the reviewer' do
      it 'renders beer#show and flashes an alert' do
        controller.stub(:require_authentication)
        session[:user_id] = nil
        delete :destroy, id: @review.id, beer_id: @beer.id

        should redirect_to @beer
        should set_the_flash[:alert].now.to 'Failed to delete review: You are not the reviewer!'
      end
    end
  end
end
