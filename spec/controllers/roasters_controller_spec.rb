require 'spec_helper'

describe RoastersController do
  describe 'GET index' do
    it 'does not require authentication' do
      controller.should_not_receive :require_authentication
      get :index, {}
    end

    it 'assigns @roasters' do
      roaster = FactoryGirl.create(:roaster)
      get :index
      assigns(:roasters).should eq [roaster]
    end
  end

  describe 'GET show' do
    before do
      @roaster = FactoryGirl.create(:roaster)
    end

    it 'does not require authentication' do
      controller.should_not_receive :require_authentication
      get :show, id: @roaster.id
    end

    it 'assigns @roaster to the specified roaster' do
      get :show, id: @roaster.id
      assigns(:roaster).should eql @roaster
    end
  end

  describe 'GET new' do
    it 'requires authentication' do
      controller.should_receive :require_authentication
      get :new, {}
    end

    context 'with authenticated user' do
      before do
        controller.stub(:require_authentication)
      end

      it 'assigns @roaster to a new roaster' do
        get :new
        assigns(:roaster).should be_a_new Roaster
      end
    end
  end

  describe 'POST create' do
    it 'requires authentication' do
      controller.should_receive :require_authentication
      post :create, roaster: FactoryGirl.attributes_for(:roaster)
    end

    context 'with authenticated user' do
      before do
        controller.stub(:require_authentication)
      end

      it 'assigns @roaster to a new roaster with the provided parameters' do
        roaster_attributes = FactoryGirl.attributes_for(:roaster)
        post :create, roaster: roaster_attributes
        assigns(:roaster).should be_a Roaster
        assigns(:roaster).name.should eql roaster_attributes[:name]
        assigns(:roaster).location.should eql roaster_attributes[:location]
      end

      it 'redirects to @roaster with a notice on successful save' do
        Roaster.any_instance.should_receive(:save).and_return(true)
        post :create, roaster: FactoryGirl.attributes_for(:roaster)
        should set_the_flash[:notice].to 'Roaster was successfully created.'
        should redirect_to(Roaster.last)
      end

      it 'flashes an alert and renders new on failed save' do
        Roaster.any_instance.should_receive(:save).and_return(false)
        post :create, roaster: FactoryGirl.attributes_for(:roaster)
        should set_the_flash[:alert].now.to 'Failed to save Roaster.'
        should render_template :new
      end

      context 'with image' do
        before do
          @roaster_attributes = FactoryGirl.attributes_for(:roaster)
          @roaster_attributes[:image] = 'Image'
        end

        it 'should redirect to @roaster with notice' do
          controller.should_receive(:upload_s3_image).with('Image',anything).and_return(true)
          post :create, roaster: @roaster_attributes
          should set_the_flash[:notice].to 'Roaster was successfully created.'
          should redirect_to(Roaster.last)
        end

        it 'should render :new if it fails to upload the image' do
          controller.should_receive(:upload_s3_image).with('Image',anything).and_return(false)
          post :create, roaster: @roaster_attributes
          should render_template :new
        end
      end
    end
  end

  describe 'GET edit' do
    before do
      @roaster = FactoryGirl.create(:roaster)
    end

    it 'requires authentication' do
      controller.should_receive :require_authentication
      get :edit, {id: @roaster.to_param}
    end

    context 'with authenticated session' do
      before do
        controller.stub(:require_authentication)
      end

      it 'assigns @roaster to the specified roaster' do
        get :edit, {id: @roaster.to_param}
        assigns(:roaster).should eql @roaster
      end
    end
  end

  describe 'PUT update' do
    before do
      @roaster = FactoryGirl.create(:roaster)
      @update_properties = FactoryGirl.attributes_for(:roaster, {name: 'updated name'})
    end

    it 'requires authentication' do
      controller.should_receive :require_authentication
      post :update, {id: @roaster.to_param, roaster: @update_properties}
    end

    context 'with authenticated session' do
      before do
        controller.stub(:require_authentication)
      end

      it 'assigns @roaster to the specified roaster' do
        post :update, {id: @roaster.to_param, roaster: @update_properties}
        assigns(:roaster).should eql @roaster
      end

      context 'on successful update' do
        it 'updates the requested roaster' do
          post :update, {id: @roaster.to_param, roaster: @update_properties}
          assigns(:roaster).name.should eq 'updated name'
        end

        it 'redirects to @roaster with notice' do
          post :update, {id: @roaster.to_param, roaster: @update_properties}
          should redirect_to @roaster
          should set_the_flash[:notice].to 'Roaster was successfully updated.'
        end

        context 'with image' do
          before do
            @update_properties[:image] = 'Image'
          end

          it 'should redirect to @roaster with notice' do
            controller.should_receive(:upload_s3_image).with('Image',anything).and_return(true)
            post :update, {id: @roaster.to_param, roaster: @update_properties}
            should set_the_flash[:notice].to 'Roaster was successfully updated.'
            should redirect_to(Roaster.last)
          end

          it 'should render :new if it fails to upload the image' do
            controller.should_receive(:upload_s3_image).with('Image',anything).and_return(false)
            post :update, {id: @roaster.to_param, roaster: @update_properties}
            should render_template :edit
          end
        end
      end

      context 'on failed update' do
        it 'renders edit' do
          Roaster.any_instance.should_receive(:update).and_return(false)
          post :update, {id: @roaster.to_param, roaster: @update_properties}
          should render_template :edit
        end
      end
    end
  end
  
  describe 'DELETE destroy' do
    before do
      @roaster = FactoryGirl.create(:roaster)
    end
    it 'requires authentication' do
      controller.should_receive :require_authentication
      delete :destroy, {id: @roaster.to_param}
    end

    context 'with authenticated session' do
      before do
        controller.stub(:require_authentication)
      end
      it 'destroys the requested roaster' do
        expect {
          delete :destroy, {id: @roaster.to_param}
        }.to change(Roaster, :count).by(-1)
      end

      it 'redirects to the roaster list' do
        delete :destroy, {id: @roaster.to_param}
        response.should redirect_to(roasters_url)
      end
    end
  end

  describe '#upload_s3_image' do
    before do
      @roaster_attributes = FactoryGirl.attributes_for(:roaster)
      @roaster_attributes[:image] = 'Image'
      controller.stub(:require_authentication)
    end

    it 'should call upload_s3_image' do
      AwsService.should_receive(:upload_roaster_image).and_return(true)
      post :create, roaster: @roaster_attributes
    end

    it 'Should handle RoasterSaveFailed runtime error' do
      AwsService.should_receive(:upload_roaster_image).and_raise(AwsService::RoasterSaveFailed)
      post :create, roaster: @roaster_attributes
      should set_the_flash[:alert].now.to 'Failed to save image, please try again'
    end

    it 'Should handle ImageUploadFailed runtime error' do
      AwsService.should_receive(:upload_roaster_image).and_raise(AwsService::ImageUploadFailed)
      post :create, roaster: @roaster_attributes
      should set_the_flash[:alert].now.to 'Failed to upload image'
    end

    it 'Should handle InvalidImageFormat runtime error' do
      AwsService.should_receive(:upload_roaster_image).and_raise(AwsService::InvalidImageFormat)
      post :create, roaster: @roaster_attributes
      should set_the_flash[:alert].now.to 'Invalid Format: Only JPEGs and PNGs are supported'
    end

    it 'Should handle NoImageProvided runtime error' do
      AwsService.should_receive(:upload_roaster_image).and_raise(AwsService::NoImageProvided)
      post :create, roaster: @roaster_attributes
      should set_the_flash[:alert].now.to 'No image provided'
    end
  end
end