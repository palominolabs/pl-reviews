require 'spec_helper'

describe SearchController do
  describe 'GET autocomplete' do
    it 'doesn\'t require authentication' do
      controller.should_not_receive(:require_authentication)
      get :autocomplete, format: :json, term: 'search_term'
    end

    it 'queries Beer for a name match' do
      Beer.should_receive(:where).with('lower(name) like ?', '%search_term%')
      get :autocomplete, format: :json, term: 'SeaRch_terM'
    end

    it 'queries Brewery for a name match' do
      Brewery.should_receive(:where).with('lower(name) like ?', '%search_term%')
      get :autocomplete, format: :json, term: 'sEarch_tERm'
    end

    it 'queries Coffee for a name match' do
      Coffee.should_receive(:where).with('lower(name) like ?', '%search_term%')
      get :autocomplete, format: :json, term: 'sEarch_tERm'
    end

    it 'queries Roaster for a name match' do
      Roaster.should_receive(:where).with('lower(name) like ?', '%search_term%')
      get :autocomplete, format: :json, term: 'sEarch_tERm'
    end

    it 'returns jsonified merge of beer and brewery results' do
      @brewery = FactoryGirl.create(:brewery)
      @beer = FactoryGirl.create(:beer, brewery: @brewery)
      @roaster = FactoryGirl.create(:roaster)
      @coffee = FactoryGirl.create(:coffee, roaster: @roaster)


      expected_result = [
          {
              label: @beer.name,
              value: @beer.id,
              category: 'Beers'
          },
          {
              label: @brewery.name,
              value: @brewery.id,
              category: 'Breweries'
          },
          {
              label: @coffee.name,
              value: @coffee.id,
              category: 'Coffees'
          },
          {
              label: @roaster.name,
              value: @roaster.id,
              category: 'Roasters'
          }
      ]

      get :autocomplete, format: :json, term: ''
      expect(response.body).to eql expected_result.to_json
    end
  end

  describe 'GET beer' do
    it 'doesn\'t require authentication' do
      controller.should_not_receive(:require_authentication)
      get :beers, format: :json, term: 'search_term'
    end

    it 'queries Beer for a name match' do
      Beer.should_receive(:where).with('lower(name) like ?', '%search_term%').and_return(Beer.all)
      get :beers, format: :json, term: 'search_term'
    end

    it 'returns expected metadata' do
      FactoryGirl.create(:beer)
      get :beers, format: :json

      expected_metadata = {
          'total_count' => 1,
          'page' => 1
      }
      expect((JSON.parse response.body)['meta']).to eql expected_metadata
    end

    it 'returns expected data' do
      beer = FactoryGirl.create(:beer)
      get :beers, format: :json

      beer_serializer = BeerSerializer.new beer

      expected_search_data = [
          JSON.parse(beer_serializer.to_json)['beer']
      ]
      expect((JSON.parse response.body)['search']).to eql expected_search_data
    end

    context 'supports filtering' do
      it 'filters to beers in_stock' do
        FactoryGirl.create(:beer, inventory: 0)
        beer = FactoryGirl.create(:beer, inventory: 1)
        get :beers, format: :json, in_stock: '1'

        assigns(:beers).should eq [beer]
      end

      it 'filters to beers in_fridge' do
        FactoryGirl.create(:beer, inventory: 1)
        beer = FactoryGirl.create(:beer, inventory: 1, in_fridge: true)
        get :beers, format: :json, in_fridge: '1'

        assigns(:beers).should eq [beer]
      end
    end

    context 'supports paging' do
      before do
        FactoryGirl.create(:beer)
        FactoryGirl.create(:beer)
        FactoryGirl.create(:beer)
        FactoryGirl.create(:beer)
      end

      it 'applies custom page_size' do
        get :beers, format: :json, page_size: 2

        expect((JSON.parse response.body)['search'].count).to eq 2
      end

      it 'returns different pages' do
        get :beers, format: :json, page_size: 2, page: 2

        expect((JSON.parse response.body)['meta']['page']).to eq 2
      end
    end
  end
end
