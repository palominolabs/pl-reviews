require 'spec_helper'

describe SessionsController do

  describe 'GET new' do
    it 'returns http success' do
      get 'new'
      response.should be_success
    end
  end

  describe 'POST create' do
    before do
      @user = FactoryGirl.create(:user)
      @login_form_params = {
          email: @user.email,
          password: @user.password
      }
    end

    it 'does not require authentication' do
      controller.should_not_receive :require_authentication
      post :create, session_form: @login_form_params
    end

    context 'successful authentication' do
      before do
        User.any_instance.should_receive(:authenticate).and_return(true)
      end

      it 'sets the session :user_id' do
        post :create, session_form: @login_form_params
        session[:user_id].should eql @user.id
      end

      it 'redirects to root_url with notice' do
        post :create, session_form: @login_form_params
        should redirect_to root_url
        should set_the_flash[:notice].to 'Logged in!'
      end

      it 'returns a 201 for JSON requests' do
        post :create, session_form: @login_form_params, format: :json
        response.response_code.should == 201
      end
    end

    context 'fails authentication' do
      before do
        User.any_instance.should_receive(:authenticate).and_return(false)
      end
      it 'renders :new with flash alert' do
        post :create, session_form: @login_form_params
        should set_the_flash[:alert].now.to 'Invalid email or password'
        should render_template :new
      end

      it 'returns a 404 for JSON requests' do
        post :create, session_form: @login_form_params, format: :json
        response.response_code.should == 404
      end
    end
  end

  describe 'DELETE destroy' do
    before do
      @user = FactoryGirl.create(:user)
      session[:user_id] = @user.id
    end

    it 'requires authentication' do
      controller.should_receive :require_authentication
      delete :destroy
    end

    it 'should clear the current session :user_id' do
      controller.stub(:require_authentication)
      delete :destroy
      session[:user_id].should be_nil
    end

    it 'should redirect to root_url with notice' do
      controller.stub(:require_authentication)
      delete :destroy
      should redirect_to root_url
      should set_the_flash[:notice].to 'Logged out!'
    end
  end
end
