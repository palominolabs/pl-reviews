# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :beer_opened_activity do
    user
    beer
  end

  factory :beer_added_activity do
    user
    beer
  end

  factory :beer_reviewed_activity do
    review
    beer
  end

  factory :coffee_added_activity do
    coffee
    user
  end

  factory :coffee_reviewed_activity do
    coffee
    review
  end

  factory :bag_opened_activity do
    coffee
    bag
    user
  end
end
