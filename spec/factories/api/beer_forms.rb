FactoryGirl.define do
  factory :api_beer_form, class: Api::BeerForm do |f|
    f.name 'Egregious'
    f.brewery_brewerydb_id 'bdid'
    f.style_brewerydb_id 5
    f.abv 15.3
  end
end