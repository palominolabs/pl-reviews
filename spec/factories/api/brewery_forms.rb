FactoryGirl.define do
  factory :api_brewery_form, class: Api::BreweryForm do |f|
    f.name 'The Rare Barrel'
  end
end