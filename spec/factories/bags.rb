# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :bag do
    roasted_on '2014-04-15'
    opened_on '2014-04-15'
    coffee
  end
end
