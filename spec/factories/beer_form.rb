FactoryGirl.define do
  factory :beer_form do
    name 'Ensorcelled'
    beer_brewerydb_id 'beer_brewerydb_id'
    brewery_name 'The Rare Barrel'
    brewery_brewerydb_id 'brewery_brewerydb_id'
    inventory 1
    in_fridge false
  end
end