# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :beer do
    name 'Enscorcelled'
    inventory 1
    sequence(:brewerydb_id) { |s| "brewery-id-#{s}" }
    abv 1.1
    ibu 100
    description 'beer description'
    style 'beer style'
    image_url 'some_image.jpg'
    icon_url 'some_icon.jpg'
    in_fridge false
    brewery

  end
end
