# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :brewery do
    sequence(:name) { |s| "brewery-#{s}" }
    sequence(:brewerydb_id) { |s| "brewery-id-#{s}" }
    image_url 'large image'
    icon_url 'icon image'
  end
end
