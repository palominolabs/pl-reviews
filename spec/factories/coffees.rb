FactoryGirl.define do
  factory :coffee do
    name 'Kilimanjaro'
    origin 'El Salvador'
    variety 'Bourbon and Kenyan'
    drip_or_espresso 'drip'
    roaster
  end
end
