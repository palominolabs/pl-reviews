FactoryGirl.define do
  factory :roaster do
    sequence(:name) { |s| "roaster-#{s}" }
    location 'Portland, OR'
    image_url 'stumptown_image'
  end
end
