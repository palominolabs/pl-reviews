require 'spec_helper'

describe ApplicationHelper do
  describe '#flash_class' do
    it 'returns correct alert for :notice level' do
      helper.flash_class(:notice).should eql 'alert alert-dismissable alert-info'
    end

    it 'returns correct alert for :success level' do
      helper.flash_class(:success).should eql 'alert alert-dismissable alert-success'
    end

    it 'returns correct alert for :error level' do
      helper.flash_class(:error).should eql 'alert alert-dismissable alert-danger'
    end

    it 'returns correct alert for :alert level' do
      helper.flash_class(:alert).should eql 'alert alert-dismissable alert-danger'
    end

    it 'returns default alert for unknown level' do
      helper.flash_class(nil).should eql 'alert alert-dismissable '
    end
  end

  describe '#current_sort_class' do
    it 'returns down arrow glyph for desc sort' do
      helper.current_sort_class('desc').should eql 'glyphicon glyphicon-chevron-down'
    end

    it 'returns up arrow glyph for asc sort' do
      helper.current_sort_class('asc').should eql 'glyphicon glyphicon-chevron-up'
    end
  end

  describe '#title' do
    it 'sets content_for :title with the given parameter' do
      helper.title('Test Title')
      helper.content_for(:title).should eql 'Test Title'
    end
  end
end
