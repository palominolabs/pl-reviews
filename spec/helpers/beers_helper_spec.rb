require 'spec_helper'

# Specs in this file have access to a helper object that includes
# the BeersHelper. For example:
#
# describe BeersHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       expect(helper.concat_strings("this","that")).to eq("this that")
#     end
#   end
# end
describe BeersHelper do

  describe '#mountain_icon' do
    it 'it returns a cold image_tag' do
      beer = FactoryGirl.build(:beer, in_fridge: true)
      mountain_icon(beer).should eql image_tag 'mountains-cold.png', class: 'fridge-icon',  data: {toggle: 'tooltip', placement: 'bottom', title: 'Cold'}
    end

    it 'it returns a warm image_tag with background' do
      beer = FactoryGirl.build(:beer, in_fridge: false)
      mountain_icon(beer, true).should eql image_tag 'mountains-warm.png', class: 'fridge-icon fridge-icon-with-background',  data: {toggle: 'tooltip', placement: 'bottom', title: 'Warm'}
    end
  end
end
