//= require ../support/spec_helper

describe('application', function () {
    beforeEach(function () {
        loadFixtures('application_form.html');
    });

    describe('#setAutocomplete', function () {
        it('sets autocomplete with source data-autocomplete-source on the beer_form_brewery_name', function () {
            var autocompleteField = $('#autocomplete-field');
            spyOn($.fn, 'autocomplete');
            autocompleteField.setAutocomplete();
            expect(autocompleteField.autocomplete).toHaveBeenCalled();
            expect(autocompleteField.autocomplete).toHaveBeenCalledWith({source: 'autocomplete-source-url'});
        });
    });

    describe('#setAutocompleteFocusHandler', function () {
        it('assigns the expected handler for autocompletefocus', function () {
            var autocompleteField = $('#autocomplete-field');
            autocompleteField.setAutocompleteFocusHandler();
            expect(autocompleteField).toHandleWith('autocompletefocus', PlReviews.Application.onAutocompleteFocusHandler);
        });
    });

    describe('#onAutocompleteFocusHandler', function () {
        it('assigns the value of the callee to be the ui.item.label', function () {
            var autocompleteField = $('#autocomplete-field'),
                stubUi = {
                    item: {
                        label: 'label'
                    }
                };
            spyOn($.fn, 'val');
            PlReviews.Application.onAutocompleteFocusHandler.call(autocompleteField, null, stubUi);
            expect(autocompleteField.val).toHaveBeenCalledWith('label');
        });
    });

    describe('#setFridgeTooltip', function () {
        it('calls tooltip on .fridge-icon class elements', function () {
            spyOn($.fn, 'tooltip');
            PlReviews.Application.setFridgeTooltip();
            expect($.fn.tooltip).toHaveBeenCalledOnSelector('.fridge-icon');
        });
    });
});