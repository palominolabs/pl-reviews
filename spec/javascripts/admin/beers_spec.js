//= require ../support/spec_helper

describe('beers', function () {
    beforeEach(function () {
        loadFixtures('beer_form.html');
    });

    describe("#setBreweryAutocompleteSelectHandler", function () {
        it('setsautocompleteselect handler', function () {
            PlReviews.Beers.setBreweryAutocompleteSelectHandler();
            expect($('#beer_form_brewery_name')).toHandleWith('autocompleteselect', PlReviews.Beers.onBreweryAutocompleteSelect);
        });
    });

    describe("#onBreweryAutocompleteSelect", function () {
        beforeEach(function () {
            var uiStub = {
                item: {
                    label: 'brewery_name',
                    value: 'brewerydb_id'
                }
            };
            spyOn(PlReviews.Beers, 'updateFormFromBrewerySelection');
            spyOn(PlReviews.Beers, 'getBeerAutocompleteData');
            PlReviews.Beers.onBreweryAutocompleteSelect(null, uiStub);
        });

        it('calls updateFormFromBrewerySelection with breweryName and breweryDbId', function () {
            expect(PlReviews.Beers.updateFormFromBrewerySelection).toHaveBeenCalledWith('brewery_name', 'brewerydb_id');
        });

        it('calls getBeerAutocompleteData with breweryDbId', function () {
            expect(PlReviews.Beers.getBeerAutocompleteData).toHaveBeenCalledWith('brewerydb_id');
        });
    });

    describe('#updateFormFromBrewerySelection', function () {
        beforeEach(function () {
            spyOn($.fn, 'toggle');
            spyOn($.fn, 'val');
            spyOn($.fn, 'html');
            spyOn($.fn, 'attr');
            PlReviews.Beers.updateFormFromBrewerySelection('brewery_name', 'breweryDbId');
        });

        it('assigns the label to the value of the brewery name and toggles it\'s visiblity', function () {
            expect($.fn.toggle).toHaveBeenCalledOnSelector('#beer_form_brewery_name');
            expect($.fn.val).toHaveBeenCalledOnSelectorWith('#beer_form_brewery_name', 'brewery_name');
        });

        it('toggles the selected_brewery div', function () {
            expect($.fn.toggle).toHaveBeenCalledOnSelector('#selected_brewery');
        });

        it('assigns the brewery name div\'s html to the breweryName', function () {
            expect($.fn.html).toHaveBeenCalledOnSelectorWith('#selected_brewery_name', 'brewery_name');
        });

        it('assigns the href on the brewerydb link and toggle it', function () {
            expect($.fn.toggle).toHaveBeenCalledOnSelector('#create_brewerydb_beer_link');
            expect($.fn.attr).toHaveBeenCalledOnSelectorWith('#create_brewerydb_beer_link', 'href', 'test_url/breweryDbId');
        });
    });


    describe('#getBeerAutocompleteData', function () {
        beforeEach(function () {
            var stubData = [
                {
                    label: 'test',
                    value: 'data'
                },
                {
                    label: 'foo',
                    value: 'bar'
                }
            ];

            spyOn($, 'ajax').and.callFake(function (req) {
                var d = $.Deferred();
                d.resolve(stubData);
                return d.promise();
            });
        });

        it('should be called with expected url', function () {
            expected_url = 'source_url/breweryDbId/';
            PlReviews.Beers.getBeerAutocompleteData('breweryDbId');
            expect($.ajax).toHaveBeenCalledWith(expected_url);
        });

        it('should call autocomplete on #beer_form_name with expected source', function () {
            var expectedResult = {
                source: [
                    {
                        label: 'test',
                        value: 'data'
                    },
                    {
                        label: 'foo',
                        value: 'bar'
                    }
                ]
            };
            spyOn($.fn, 'autocomplete');
            PlReviews.Beers.getBeerAutocompleteData('breweryDbId');
            expect($.fn.autocomplete).toHaveBeenCalledOnSelectorWith('#beer_form_name', expectedResult);
        });
    });

    describe('#autoPopulateBrewery', function () {
        it('checks #beer_form_brewery_name value', function () {
            spyOn($.fn, 'val').and.returnValue(false);
            PlReviews.Beers.autoPopulateBrewery();
            expect($.fn.val).toHaveBeenCalledOnSelector('#beer_form_brewery_name');
        });

        it('triggers autocompleteselect on #beer_form_brewery_name if it has a value', function () {
            var expectedAutocompleteParams = {
                item: {
                    label: 'val_stub',
                    value: 'val_stub'
                }
            };
            spyOn($.fn, 'val').and.returnValue('val_stub');
            spyOn($.fn, 'trigger');
            PlReviews.Beers.autoPopulateBrewery();
            expect($.fn.val).toHaveBeenCalledOnSelector('#beer_form_brewery_brewerydb_id');
            expect($.fn.trigger).toHaveBeenCalledOnSelectorWith('#beer_form_brewery_name', 'autocompleteselect', expectedAutocompleteParams);
        });
    });

    describe('#setBeerFormReadyHandler', function () {
        it('should set ready to call autoPopulateBrewery', function () {
            spyOn($.fn, 'ready');
            PlReviews.Beers.setBeerFormReadyHandler();
            expect($.fn.ready).toHaveBeenCalledOnSelector('#new_beer_form');
        });
    });

    describe('#clearBeerForm', function () {
        beforeEach(function () {
            spyOn($.fn, 'toggle');
            spyOn($.fn, 'val');
            spyOn($.fn, 'prop');
            spyOn($.fn, 'attr');

            PlReviews.Beers.clearBeerForm();
        });

        it('clears the value of #beer_form_brewery_name and toggles it', function () {
            expect($.fn.val).toHaveBeenCalledOnSelectorWith('#beer_form_brewery_name', '');
            expect($.fn.toggle).toHaveBeenCalledOnSelector('#beer_form_brewery_name');
        });

        it('toggles #selected_brewery', function () {
            expect($.fn.toggle).toHaveBeenCalledOnSelector('#selected_brewery');
        });

        it('clears the value of #beer_form_brewery_brewerydb_id', function () {
            expect($.fn.val).toHaveBeenCalledOnSelectorWith('#beer_form_brewery_brewerydb_id', '');
        });

        it('disables #beer_fields', function () {
            expect($.fn.prop).toHaveBeenCalledOnSelectorWith('#beer_fields', 'disabled', true);
        });

        it('clears the value of #beer_form_name', function () {
            expect($.fn.val).toHaveBeenCalledOnSelectorWith('#beer_form_name', '');
        });

        it('clears the value of #create_brewerydb_beer_link href', function () {
            expect($.fn.attr).toHaveBeenCalledOnSelectorWith('#create_brewerydb_beer_link', 'href', '');
        });

        it('toggles #create_brewerydb_beer_link', function () {
            expect($.fn.toggle).toHaveBeenCalledOnSelector('#create_brewerydb_beer_link');
        });
    });

    describe('#setClearButtonClick', function () {
        it('sets on click handler for clear_brewery button', function () {
            PlReviews.Beers.setClearButtonClick();
            expect($('#clear_brewery')).toHandleWith('click', PlReviews.Beers.clearBeerForm);
        });
    });

    describe('#onBeerAutocompleteSelect', function () {
        it('sets the value of beer_form_name and beer_form_beer_brewerydb_id', function () {
            var stubUI = {
                item: {
                    label: 'label',
                    value: 'value'
                }
            };
            spyOn($.fn, 'val');
            PlReviews.Beers.onBeerAutocompleteSelect(null, stubUI);
            expect($.fn.val).toHaveBeenCalledOnSelectorWith('#beer_form_name', 'label');
            expect($.fn.val).toHaveBeenCalledOnSelectorWith('#beer_form_beer_brewerydb_id', 'value');
        });
    });

    describe('#setBeerAutocompleteSelectHandler', function () {
        it('sets the autocompleteselect handler for beer_form_name', function () {
            PlReviews.Beers.setBeerAutocompleteSelectHandler();
            expect($('#beer_form_name')).toHandleWith('autocompleteselect', PlReviews.Beers.onBeerAutocompleteSelect);
        });
    });
});

