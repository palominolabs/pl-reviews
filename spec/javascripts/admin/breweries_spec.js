//= require ../support/spec_helper

describe('breweries', function () {
   describe('#setBreweryNameAutocompleteselectHandler', function () {
       beforeEach(function () {
           loadFixtures('breweries_form.html');
       });

       it('assigns an autocompleteselect handler to #brewery_name', function () {
           PlReviews.Breweries.setBreweryNameAutocompleteselectHandler();
           expect($('#brewery_name')).toHandleWith('autocompleteselect', PlReviews.Breweries.oBreweryNamenAutocompleteselect);
       });
   });

   describe('#oBreweryNamenAutocompleteselect', function () {
       it('sets the values of brewery_name and brewery_brewerydb_id', function () {
           var uiStub = {
               item: {
                   label: 'ui_label',
                   value: 'ui_value'
               }
           };
           spyOn($.fn, 'val');
           PlReviews.Breweries.oBreweryNamenAutocompleteselect(null, uiStub);
           expect($.fn.val).toHaveBeenCalledOnSelectorWith('#brewery_name', 'ui_label');
           expect($.fn.val).toHaveBeenCalledOnSelectorWith('#brewery_brewerydb_id', 'ui_value');
       });
   });
});