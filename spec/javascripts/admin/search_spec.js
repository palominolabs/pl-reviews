//= require ../support/spec_helper

describe('search', function () {
    beforeEach(function () {
        loadFixtures('search_form.html');
    });

    describe('#loadCustomCatcompleteWidget', function () {
        it('calls widget to add custom catcomplete functionality', function () {
            spyOn($, 'widget');
            PlReviews.Search.loadCustomCatcompleteWidget();
            expect($.widget).toHaveBeenCalledWith("custom.catcomplete", $.ui.autocomplete, {
                _renderMenu: PlReviews.Search.renderMenu
            });
        });
    });

    describe('#renderMenu', function () {
        it('loops through all of the items', function () {
            spyOn($, 'each');
            PlReviews.Search.renderMenu(null, 'items');
            expect($.each).toHaveBeenCalledWith('items', jasmine.any(Function));
        });
    });

    describe('#renderMenuItem', function () {
        var menu,
            ul,
            item,
            currentCategory;

        beforeEach(function () {
            menu = {
                _renderItemData: jasmine.createSpy()
            };
            ul = $('<ul></ul>');
            item = {
                category: 'category_text'
            };
            currentCategory = 'category_text';
        });

        it('appends a li to the ul if item.category and currentCategory don\'t match', function () {
            PlReviews.Search.renderMenuItem(menu, ul, item, 'other_category');
            expect(ul.html()).toEqual("<li class=\"ui-autocomplete-category\">category_text</li>");
        });

        it('appends nothing if the item.category matches the currentCategory', function () {
            PlReviews.Search.renderMenuItem(menu, ul, item, currentCategory);
            expect(ul.html()).toEqual('');
        });

        it('calls _renderItemData on the menu', function () {
            PlReviews.Search.renderMenuItem(menu, ul, item, currentCategory);
            expect(menu._renderItemData).toHaveBeenCalledWith(ul, item);
        });

        it('returns the new currentCategory', function () {
            returnedCategory = PlReviews.Search.renderMenuItem(menu, ul, item, 'other_category');
            expect(returnedCategory).toEqual('category_text');
        });
    });

    describe('#setCatcomplete', function () {
        it('calls catcomplete on the target with it\'s data-autocomplete-source', function () {
            var expectedParams = {
                    delay: 0,
                    source: 'auto-source'
                };
            spyOn($.fn, 'catcomplete');
            $('#search').setCatcomplete();
            expect($.fn.catcomplete).toHaveBeenCalledOnSelectorWith('#search', expectedParams);
        });
    });

    describe('#setSearchSelectHandler', function () {
        it('sets handler for catcompleteselect on #search', function () {
            PlReviews.Search.setSearchSelectHandler();
            expect($('#search')).toHandleWith('catcompleteselect', PlReviews.Search.onSearchSelect);
        });
    });

    describe('#onSearchSelect', function () {
        beforeEach(function () {
            spyOn(PlReviews.Search, 'redirectToUrl');
        });

        it('sets the window location to direct to the beer', function () {
            var uiStub = {
                item: {
                    category: 'Beers',
                    value: 'beer_id'
                }
            };

            PlReviews.Search.onSearchSelect(null, uiStub);
            expect(PlReviews.Search.redirectToUrl).toHaveBeenCalledWith('beers_url/beer_id');
        });

        it('sets the window location to direct to the brewery', function () {
            var uiStub = {
                item: {
                    category: 'Breweries',
                    value: 'brewery_id'
                }
            };

            PlReviews.Search.onSearchSelect(null, uiStub);
            expect(PlReviews.Search.redirectToUrl).toHaveBeenCalledWith('breweries_url/brewery_id');
        });
    });

    describe('#setSearchFocusHandler', function () {
        it('sets handler for catcompletefocus on #search', function () {
            PlReviews.Search.setSearchFocusHandler();
            expect($('#search')).toHandleWith('catcompletefocus', PlReviews.Search.onSearchFocus);
        });
    });

    describe('#onSearchFocus', function () {
        it('sets the value of search to the label', function () {
            var uiStub = {
                item: {
                    label: 'label_text'
                }
            };
            spyOn($.fn, 'val');
            PlReviews.Search.onSearchFocus(null, uiStub);
            expect($.fn.val).toHaveBeenCalledOnSelectorWith('#search', 'label_text');
        });
    });
});