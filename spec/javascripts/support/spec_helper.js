//= require jasmine-jquery

beforeEach(function () {
    jasmine.addMatchers({
        toHaveBeenCalledOnSelector: function () {
            return {
                compare: function (actual, selector) {
                    var i,
                        calls = actual.calls.all(),
                        result = { pass: false };

                    if (!jasmine.isSpy(actual)) {
                        throw new Error('Expected a spy, but got ' + jasmine.pp(actual) + '.');
                    }

                    if (arguments.length != 2) {
                        throw new Error('toHaveBeenCalledOnSelector only takes a selector argument, use toHaveBeenCalledOnSelectorWith to specify expected inputs');
                    }

                    for (i = 0; i < calls.length; i++) {
                        var call = calls[i];
                        if (call.object && call.object.selector === selector) {
                            result.pass = true;
                            break;
                        }
                    }

                    result.message = result.pass ?
                        "Expected spy " + actual.and.identity() + " not to have been called on " + selector + "." :
                        "Expected spy " + actual.and.identity() + " to have been called on " + selector + ".";

                    return result;

                }
            }
        },
        toHaveBeenCalledOnSelectorWith: function (util) {
            return {
                compare: function () {
                    var i,
                        args = Array.prototype.slice.call(arguments, 0),
                        actual = args[0],
                        calls = actual.calls.all(),
                        selector = args[1],
                        expectedArgs = args.slice(2),
                        result = {
                            pass: false,
                            message: "Expected spy " + actual.and.identity() + " to have been called on " + selector + " with " + jasmine.pp(expectedArgs) + " but it was never called."
                        };

                    if (!jasmine.isSpy(actual)) {
                        throw new Error('Expected a spy, but got ' + jasmine.pp(actual) + '.');
                    }

                    for (i = 0; i < calls.length; i++) {
                        var call = calls[i];
                        if (call.object && call.object.selector === selector) {
                            if (util.equals(call.args, expectedArgs)) {
                                result.pass = true;
                                result.message = "Expected spy " + actual.and.identity() + " not to have been called on " + selector + " with " + jasmine.pp(expectedArgs) + " but it was.";
                                break;
                            } else {
                                result.message = "Expected spy " + actual.and.identity() + " to have been called on " + selector + " with " + jasmine.pp(expectedArgs) + " but actual calls were " + jasmine.pp(call.args).replace(/^\[ | \]$/g, '') + ".";
                            }
                        }
                    }

                    return result;
                }
            }
        }
    });
});