//= require ../support/spec_helper

describe('beer modal', function () {
    beforeEach(function () {
        loadFixtures('beer_modal_form.html');
    });

    describe('#setViewBeerLinkClickHandler', function () {
        it('sets click handler for view-beer-links', function () {
            PlReviewsWidget.BeerModal.setViewBeerLinkClickHandler();
            expect($('.view-beer-link')).toHandleWith('click', PlReviewsWidget.BeerModal.onViewBeerLinkClicked);
        });
    });

    describe('#onViewBeerLinkClicked', function () {
        beforeEach(function () {
            spyOn($, 'ajax').and.callFake(function (req) {
                var d = $.Deferred();
                d.resolve('stub_data');
                return d.promise();
            });
            spyOn(PlReviewsWidget.BeerModal, 'clearContent');
            spyOn(PlReviewsWidget.BeerModal, 'onBeersAjaxResponse');
            spyOn($.fn, 'show');
            spyOn($.fn, 'hide');
            stubEvent = {
                preventDefault: jasmine.createSpy()
            };
            PlReviewsWidget.BeerModal.onViewBeerLinkClicked.call($('#test-beer'), stubEvent);
        });

        it('sets beerId', function () {
            expect(PlReviewsWidget.BeerModal.beerId).toEqual('5');
        });

        it('calls clearContent', function () {
            expect(PlReviewsWidget.BeerModal.clearContent).toHaveBeenCalled();
        });

        it('triggers an ajax request', function () {
            expect($.ajax).toHaveBeenCalledWith('/beers/5.json');
        });

        it('calls #onBeersAjaxResponse on successful response', function () {
            expect(PlReviewsWidget.BeerModal.onBeersAjaxResponse).toHaveBeenCalledWith('stub_data');
        });

        it('shows the #beer-modal-dialog', function () {
            expect($.fn.show).toHaveBeenCalledOnSelector('#beer-modal-dialog');
        });

        it('hides the #list-content', function () {
            expect($.fn.hide).toHaveBeenCalledOnSelector('#list-content');
        });

        it('calls prevent default on the event', function () {
            expect(stubEvent.preventDefault).toHaveBeenCalled();
        });
    });

    describe('#clearContent', function () {
        beforeEach(function () {
            spyOn($.fn, 'text');
            spyOn($.fn, 'attr');
            spyOn($.fn, 'html');
            spyOn($.fn, 'hide');
            PlReviewsWidget.BeerModal.clearContent();
        });

        it('clears #beer-modal-dialog-brewery', function () {
            expect($.fn.text).toHaveBeenCalledOnSelectorWith('#beer-modal-dialog-brewery', '');
        });

        it('clears #beer-modal-dialog-name', function () {
            expect($.fn.text).toHaveBeenCalledOnSelectorWith('#beer-modal-dialog-name', '');
        });

        it('clears #beer-modal-dialog-abv', function () {
            expect($.fn.text).toHaveBeenCalledOnSelectorWith('#beer-modal-dialog-abv', 'Unknown');
        });

        it('clears #beer-modal-dialog-ibu', function () {
            expect($.fn.text).toHaveBeenCalledOnSelectorWith('#beer-modal-dialog-ibu', 'Unknown');
        });

        it('clears #beer-modal-dialog-inventory', function () {
            expect($.fn.text).toHaveBeenCalledOnSelectorWith('#beer-modal-dialog-inventory', '');
        });

        it('clears #beer-modal-dialog-style', function () {
            expect($.fn.text).toHaveBeenCalledOnSelectorWith('#beer-modal-dialog-style', 'Unknown');
        });

        it('clears #beer-modal-dialog-description', function () {
            expect($.fn.text).toHaveBeenCalledOnSelectorWith('#beer-modal-dialog-description', '');
        });

        it('Sets the src of #beer-modal-dialog-image', function () {
            expect($.fn.attr).toHaveBeenCalledOnSelectorWith('#beer-modal-dialog-image', 'src', '/assets/spinner.gif');
        });

        it('clears #beer-modal-dialog-activities', function () {
            expect($.fn.html).toHaveBeenCalledOnSelectorWith('#beer-modal-dialog-activities', '');
            expect($('#beer-modal-dialog-activities').html).toHaveBeenCalledWith('');
        });

        it('Hides #beer-modal-dialog-activities', function () {
            expect($.fn.hide).toHaveBeenCalledOnSelector('#beer-modal-dialog-activities');
        });

        it('Hides #beer-modal-dialog-title', function () {
            expect($.fn.hide).toHaveBeenCalledOnSelector('#beer-modal-dialog-activity-title');
        });
    });

    describe('#onBeersAjaxResponse', function () {
        it('calls setContent if there\'s a beer', function () {
            var stubData = {
                beer: {
                    id: 'test_id'
                }
            };

            PlReviewsWidget.BeerModal.beerId = 'test_id';
            spyOn(PlReviewsWidget.BeerModal, 'setContent');
            PlReviewsWidget.BeerModal.onBeersAjaxResponse(stubData);
            expect(PlReviewsWidget.BeerModal.setContent).toHaveBeenCalledWith(stubData.beer);
        });

        it('does nothing if the beer doesn\'t match', function () {
            var stubData = {
                beer: {
                    id: 'test_id'
                }
            };

            PlReviewsWidget.BeerModal.beerId = 'other_test_id';
            spyOn(PlReviewsWidget.BeerModal, 'setContent');
            PlReviewsWidget.BeerModal.onBeersAjaxResponse(stubData);
            expect(PlReviewsWidget.BeerModal.setContent).not.toHaveBeenCalledWith(stubData.beer);
        });
    });

    describe('#setContent', function () {
        beforeEach(function () {
            spyOn($.fn, 'text');
            spyOn($.fn, 'append');
            spyOn($.fn, 'attr');
            spyOn($.fn, 'show');
            var stubBeer = {
                name: 'test_name',
                brewery: {
                    name: 'test_brewery_name'
                },
                in_fridge: true,
                icon_url: 'test_icon_url',
                abv: '5',
                ibu: '100',
                style: 'test_style',
                description: 'test_description',
                inventory: '3',
                activities: 'test_activities'
            };
            spyOn(PlReviewsWidget.BeerModal, 'setActivities');
            PlReviewsWidget.BeerModal.setContent(stubBeer);
        });

        it('should set the text of #beer-modal-dialog-name', function () {
            expect($.fn.text).toHaveBeenCalledOnSelectorWith('#beer-modal-dialog-name', 'test_name');
        });

        it('should set the text of #beer-modal-dialog-brewery', function () {
            expect($.fn.text).toHaveBeenCalledOnSelectorWith('#beer-modal-dialog-brewery', 'test_brewery_name');
        });

        it('should append to #beer-modal-dialog-name', function () {
            expect($.fn.append).toHaveBeenCalledOnSelectorWith('#beer-modal-dialog-name', ' <span class="cold-beer-badge"><img alt="Snowflake" class="snowflake-image" src="/assets/snowflake.png" /> In fridge</a>');
        });

        it('should set the src of #beer-modal-dialog-image', function () {
            expect($.fn.attr).toHaveBeenCalledOnSelectorWith('#beer-modal-dialog-image', 'src', 'test_icon_url');
        });

        it('should set the text of #beer-modal-dialog-abv', function () {
            expect($.fn.text).toHaveBeenCalledOnSelectorWith('#beer-modal-dialog-abv', '5%');
        });

        it('should set the text of #beer-modal-dialog-ibu', function () {
            expect($.fn.text).toHaveBeenCalledOnSelectorWith('#beer-modal-dialog-ibu', '100');
        });

        it('should set the text of #beer-modal-dialog-description', function () {
            expect($.fn.text).toHaveBeenCalledOnSelectorWith('#beer-modal-dialog-description', 'test_description');
        });

        it('should show #beer-modal-dialog-description', function () {
            expect($.fn.show).toHaveBeenCalledOnSelector('#beer-modal-dialog-description');
        });

        it('should set the text of #beer-modal-dialog-style', function () {
            expect($.fn.text).toHaveBeenCalledOnSelectorWith('#beer-modal-dialog-style', 'test_style');
        });

        it('should set the text of #beer-modal-dialog-inventory', function () {
            expect($.fn.text).toHaveBeenCalledOnSelectorWith('#beer-modal-dialog-inventory', '3');
        });

        it('should call setActivities', function () {
            expect(PlReviewsWidget.BeerModal.setActivities).toHaveBeenCalledWith('test_activities');
        });
    });

    describe('#setActivities', function () {
        beforeEach(function () {
            spyOn($.fn, 'each');
            spyOn($.fn, 'show');
        });

        it('iterates through each activity calling #addActivity', function () {
            var activities = [1, 2];
            PlReviewsWidget.BeerModal.setActivities(activities);
            expect($(activities).each).toHaveBeenCalledWith(PlReviewsWidget.BeerModal.addActivity);
        });

        it('shows #modal-dialog-activity-title', function () {
            PlReviewsWidget.BeerModal.setActivities([]);
            expect($.fn.show).toHaveBeenCalledOnSelector('#beer-modal-dialog-activity-title');
        });

        it('shows #modal-dialog-activities', function () {
            PlReviewsWidget.BeerModal.setActivities([]);
            expect($.fn.show).toHaveBeenCalledOnSelector('#beer-modal-dialog-activities');
        });
    });

    describe('#addActivity', function () {
        var activity = null,
            moment_date = null;

        beforeEach(function () {
            spyOn($.fn, 'append');
            spyOn($.fn, 'prepend');

            moment_date = {
                format: jasmine.createSpy()
            };
            moment = jasmine.createSpy().and.returnValue(moment_date);

            activity = {
                type: 'BeerReviewedActivity',
                created_at: new Date('11-12-13'),
                review: {
                    user: {
                        name: 'review_user_name'
                    },
                    rating: 'review_rating'
                }
            };
        });

        // Created_at timestamp
        it('calls moment with the created_at value', function () {
            PlReviewsWidget.BeerModal.addActivity(0, activity);
            expect(moment).toHaveBeenCalledWith((activity.created_at));
        });

        it('calls moment_date.format with "MMMM Do', function () {
            PlReviewsWidget.BeerModal.addActivity(0, activity);
            expect(moment_date.format).toHaveBeenCalledWith(('MMMM Do'));
        });

        // BeerReviewedActivity
        it('appends image for BeerReviewedActivity', function () {
            PlReviewsWidget.BeerModal.addActivity(0, activity);
            expectedAddition = ['<img alt="Activity rating" class="modal-dialog-activity-icon" src="/assets/activity-rating.png" />'];
            expect($.fn.append.calls.argsFor(0)).toEqual(expectedAddition);
        });

        it('prepends text for BeerReviewedActivity', function () {
            PlReviewsWidget.BeerModal.addActivity(0, activity);
            expectedAddition = [ '<strong>review_user_name</strong> rated it <strong>review_rating out of 5</strong>' ];
            expect($.fn.prepend.calls.argsFor(0)).toEqual(expectedAddition);
        });

        // BeerOpenedActivity
        it('activity for BeerOpenedActivity', function () {
            activity.type = 'BeerOpenedActivity';
            PlReviewsWidget.BeerModal.addActivity(0, activity);
            expectedAddition = ['<img alt="Activity open" class="modal-dialog-activity-icon" src="/assets/activity-open.png" />'];
            expect($.fn.append.calls.argsFor(0)).toEqual(expectedAddition);
        });

        it('prepends text for BeerOpenedActivity', function () {
            activity.type = 'BeerOpenedActivity';
            PlReviewsWidget.BeerModal.addActivity(0, activity);
            expectedAddition = [ 'Opened' ];
            expect($.fn.prepend.calls.argsFor(0)).toEqual(expectedAddition);
        });

        // BeerAddedActivity
        it('activity for BeerAddedActivity', function () {
            activity.type = 'BeerAddedActivity';
            PlReviewsWidget.BeerModal.addActivity(0, activity);
            expectedAddition = ['<img alt="Activity add" class="modal-dialog-activity-icon" src="/assets/activity-add.png" />'];
            expect($.fn.append.calls.argsFor(0)).toEqual(expectedAddition);
        });

        it('prepends text for BeerAddedActivity', function () {
            activity.type = 'BeerAddedActivity';
            PlReviewsWidget.BeerModal.addActivity(0, activity);
            expectedAddition = [ 'Added to the inventory' ];
            expect($.fn.prepend.calls.argsFor(0)).toEqual(expectedAddition);
        });

        // Adds to #modal-dialog-activities
        it('activity for BeerAddedActivity', function () {
            PlReviewsWidget.BeerModal.addActivity(0, activity);
            expect($.fn.append).toHaveBeenCalledOnSelector('#beer-modal-dialog-activities');
            expect($('#modal-dialog-activities').append).toHaveBeenCalled();
        });

    });

    describe('#setCloseDialogClickHandler', function () {
        it('Should set the click handler on #modal-dialog-close', function () {
            PlReviewsWidget.BeerModal.setCloseDialogClickHandler();
            expect($('#beer-modal-dialog-close')).toHandleWith('click', PlReviewsWidget.BeerModal.closeDialog);
        });
    });

    describe('#closeDialog', function () {
        beforeEach(function () {
            spyOn($.fn, 'hide');
            spyOn($.fn, 'show');
            stubEvent = {
                preventDefault: jasmine.createSpy()
            };
            PlReviewsWidget.BeerModal.closeDialog(stubEvent);
        });

        it('hides the modal dialog', function () {
            expect($.fn.hide).toHaveBeenCalledOnSelector('#beer-modal-dialog');
        });

        it('shows the list content', function () {
            expect($.fn.show).toHaveBeenCalledOnSelector('#list-content');
        });

        it('clears the beerId', function () {
            expect(PlReviewsWidget.BeerModal.beerId).toBeNull();

        });

        it('calls prevent default on the event', function () {
            expect(stubEvent.preventDefault).toHaveBeenCalled();
        });
    });
});