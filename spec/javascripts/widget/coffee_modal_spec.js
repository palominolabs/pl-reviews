//= require ../support/spec_helper

describe('coffee modal', function () {
    beforeEach(function () {
        loadFixtures('coffee_modal_form.html');
    });

    describe('#setViewCoffeeLinkClickHandler', function () {
        it('sets click handler for view-coffee-links', function () {
            PlReviewsWidget.CoffeeModal.setViewCoffeeLinkClickHandler();
            expect($('.view-coffee-link')).toHandleWith('click', PlReviewsWidget.CoffeeModal.onViewCoffeeLinkClicked);
        });
    });

    describe('#onViewCoffeeLinkClicked', function () {
        beforeEach(function () {
            spyOn($, 'ajax').and.callFake(function (req) {
                var d = $.Deferred();
                d.resolve('stub_data');
                return d.promise();
            });
            spyOn(PlReviewsWidget.CoffeeModal, 'clearContent');
            spyOn(PlReviewsWidget.CoffeeModal, 'onCoffeesAjaxResponse');
            spyOn($.fn, 'show');
            spyOn($.fn, 'hide');
            stubEvent = {
                preventDefault: jasmine.createSpy()
            };
            PlReviewsWidget.CoffeeModal.onViewCoffeeLinkClicked.call($('#test-coffee'), stubEvent);
        });

        it('sets coffeeId', function () {
            expect(PlReviewsWidget.CoffeeModal.coffeeId).toEqual('5');
        });

        it('calls clearContent', function () {
            expect(PlReviewsWidget.CoffeeModal.clearContent).toHaveBeenCalled();
        });

        it('triggers an ajax request', function () {
            expect($.ajax).toHaveBeenCalledWith('/coffees/5.json');
        });

        it('calls #onCoffeesAjaxResponse on successful response', function () {
            expect(PlReviewsWidget.CoffeeModal.onCoffeesAjaxResponse).toHaveBeenCalledWith('stub_data');
        });

        it('shows the #modal-dialog', function () {
            expect($.fn.show).toHaveBeenCalledOnSelector('#coffee-modal-dialog');
        });

        it('hides the #list-content', function () {
            expect($.fn.hide).toHaveBeenCalledOnSelector('#list-content');
        });

        it('calls prevent default on the event', function () {
            expect(stubEvent.preventDefault).toHaveBeenCalled();
        });
    });

    describe('#clearContent', function () {
        beforeEach(function () {
            spyOn($.fn, 'text');
            spyOn($.fn, 'attr');
            spyOn($.fn, 'html');
            spyOn($.fn, 'hide');
            PlReviewsWidget.CoffeeModal.clearContent();
        });

        it('clears #coffee-modal-dialog-roaster', function () {
            expect($.fn.text).toHaveBeenCalledOnSelectorWith('#coffee-modal-dialog-roaster', '');
        });

        it('clears #coffee-modal-dialog-name', function () {
            expect($.fn.text).toHaveBeenCalledOnSelectorWith('#coffee-modal-dialog-name', '');
        });

        it('clears #coffee-modal-dialog-origin', function () {
            expect($.fn.text).toHaveBeenCalledOnSelectorWith('#coffee-modal-dialog-origin', 'Unknown');
        });

        it('clears #coffee-modal-dialog-variety', function () {
            expect($.fn.text).toHaveBeenCalledOnSelectorWith('#coffee-modal-dialog-variety', 'Unknown');
        });

        it('clears #coffee-modal-dialog-type', function () {
            expect($.fn.text).toHaveBeenCalledOnSelectorWith('#coffee-modal-dialog-type', '');
        });

        it('Sets the src of #coffee-modal-dialog-image', function () {
            expect($('#coffee-modal-dialog-image').attr).toHaveBeenCalledWith('src', '/assets/spinner.gif');
        });

        it('clears #coffee-modal-dialog-activities', function () {
            expect($.fn.html).toHaveBeenCalledOnSelectorWith('#coffee-modal-dialog-activities', '');
        });

        it('Hides #coffee-modal-dialog-activities', function () {
            expect($.fn.hide).toHaveBeenCalledOnSelector('#coffee-modal-dialog-activities');
        });

        it('Hides #coffee-modal-dialog-activity-title', function () {
            expect($.fn.hide).toHaveBeenCalledOnSelector('#coffee-modal-dialog-activity-title');
        });
    });

    describe('#onCoffeesAjaxResponse', function () {
        it('calls setContent if there\'s a coffee', function () {
            var stubData = {
                coffee: {
                    id: 'test_id'
                }
            };

            PlReviewsWidget.CoffeeModal.coffeeId = 'test_id';
            spyOn(PlReviewsWidget.CoffeeModal, 'setContent');
            PlReviewsWidget.CoffeeModal.onCoffeesAjaxResponse(stubData);
            expect(PlReviewsWidget.CoffeeModal.setContent).toHaveBeenCalledWith(stubData.coffee);
        });

        it('does nothing if the coffee doesn\'t match', function () {
            var stubData = {
                coffee: {
                    id: 'test_id'
                }
            };

            PlReviewsWidget.CoffeeModal.coffeeId = 'other_test_id';
            spyOn(PlReviewsWidget.CoffeeModal, 'setContent');
            PlReviewsWidget.CoffeeModal.onCoffeesAjaxResponse(stubData);
            expect(PlReviewsWidget.CoffeeModal.setContent).not.toHaveBeenCalledWith(stubData.coffee);
        });
    });

    describe('#setContent', function () {
        beforeEach(function () {
            spyOn($.fn, 'text');
            spyOn($.fn, 'append');
            spyOn($.fn, 'attr');
            spyOn($.fn, 'show');
            var stubCoffee = {
                name: 'test_name',
                roaster: {
                    name: 'test_roaster_name',
                    image_url: 'test_image_url'
                },
                origin: 'test_origin',
                variety: 'test_variety',
                drip_or_espresso: 'drip',
                bags_count: '3',
                activities: 'test_activities'
            };
            spyOn(PlReviewsWidget.CoffeeModal, 'setActivities');
            PlReviewsWidget.CoffeeModal.setContent(stubCoffee);
        });

        it('should set the text of #coffee-modal-dialog-name', function () {
            expect($.fn.text).toHaveBeenCalledOnSelectorWith('#coffee-modal-dialog-name', 'test_name');
        });

        it('should set the text of #coffee-modal-dialog-roaster', function () {
            expect($.fn.text).toHaveBeenCalledOnSelectorWith('#coffee-modal-dialog-roaster', 'test_roaster_name');
        });

        it('should set the src of #coffee-modal-dialog-image', function () {
            expect($.fn.attr).toHaveBeenCalledOnSelectorWith('#coffee-modal-dialog-image', 'src', 'test_image_url');
        });

        it('should set the text of #coffee-modal-dialog-origin', function () {
            expect($.fn.text).toHaveBeenCalledOnSelectorWith('#coffee-modal-dialog-origin', 'test_origin');
        });

        it('should set the text of #coffee-modal-dialog-variety', function () {
            expect($.fn.text).toHaveBeenCalledOnSelectorWith('#coffee-modal-dialog-variety', 'test_variety');
        });

        it('should set the text of #coffee-modal-dialog-type', function () {
            expect($.fn.text).toHaveBeenCalledOnSelectorWith('#coffee-modal-dialog-type', 'drip');
        });

        it('should call setActivities', function () {
            expect(PlReviewsWidget.CoffeeModal.setActivities).toHaveBeenCalledWith('test_activities');
        });
    });

    describe('#setActivities', function () {
        beforeEach(function () {
            spyOn($, 'each');
            spyOn($.fn, 'show');
        });

        it('iterates through each activity calling #addActivity', function () {
            var activities = [1, 2];
            PlReviewsWidget.CoffeeModal.setActivities(activities);
            expect($.each).toHaveBeenCalledWith(activities, PlReviewsWidget.CoffeeModal.addActivity);
        });

        it('shows #coffee-modal-dialog-activity-title', function () {
            PlReviewsWidget.CoffeeModal.setActivities([]);
            expect($.fn.show).toHaveBeenCalledOnSelector('#coffee-modal-dialog-activity-title');
        });

        it('shows #coffee-modal-dialog-activities', function () {
            PlReviewsWidget.CoffeeModal.setActivities([]);
            expect($.fn.show).toHaveBeenCalledOnSelector('#coffee-modal-dialog-activity-title');
        });
    });

    describe('#addActivity', function () {
        var activity = null,
            moment_date = null;

        beforeEach(function () {
            spyOn($.fn, 'append');
            spyOn($.fn, 'prepend');

            moment_date = {
                format: jasmine.createSpy()
            };
            moment = jasmine.createSpy().and.returnValue(moment_date);

            activity = {
                type: 'CoffeeReviewedActivity',
                created_at: new Date('11-12-13'),
                review: {
                    user: {
                        name: 'review_user_name'
                    },
                    rating: 'review_rating'
                }
            };
        });

        // Created_at timestamp
        it('calls moment with the created_at value', function () {
            PlReviewsWidget.CoffeeModal.addActivity(0, activity);
            expect(moment).toHaveBeenCalledWith((activity.created_at));
        });

        it('calls moment_date.format with "MMMM Do', function () {
            PlReviewsWidget.CoffeeModal.addActivity(0, activity);
            expect(moment_date.format).toHaveBeenCalledWith(('MMMM Do'));
        });

        // CoffeeReviewedActivity
        it('appends image for CoffeeReviewedActivity', function () {
            PlReviewsWidget.CoffeeModal.addActivity(0, activity);
            expectedAddition = ['<img alt="Activity rating" class="modal-dialog-activity-icon" src="/assets/activity-rating.png" />'];
            expect($.fn.append.calls.argsFor(0)).toEqual(expectedAddition);
        });

        it('prepends text for CoffeeReviewedActivity', function () {
            PlReviewsWidget.CoffeeModal.addActivity(0, activity);
            expectedAddition = [ '<strong>review_user_name</strong> rated it <strong>review_rating out of 5</strong>' ];
            expect($.fn.prepend.calls.argsFor(0)).toEqual(expectedAddition);
        });

        // CoffeeOpenedActivity
        it('activity for BagOpenedActivity', function () {
            activity.type = 'BagOpenedActivity';
            PlReviewsWidget.CoffeeModal.addActivity(0, activity);
            expectedAddition = ['<img alt="Activity coffee" class="modal-dialog-activity-icon" src="/assets/activity-coffee.png" />'];
            expect($.fn.append.calls.argsFor(0)).toEqual(expectedAddition);
        });

        it('prepends text for BagOpenedActivity', function () {
            activity.type = 'BagOpenedActivity';
            PlReviewsWidget.CoffeeModal.addActivity(0, activity);
            expectedAddition = [ 'Opened' ];
            expect($.fn.prepend.calls.argsFor(0)).toEqual(expectedAddition);
        });

        // CoffeeAddedActivity
        it('activity for CoffeeAddedActivity', function () {
            activity.type = 'CoffeeAddedActivity';
            PlReviewsWidget.CoffeeModal.addActivity(0, activity);
            expectedAddition = ['<img alt="Activity add" class="modal-dialog-activity-icon" src="/assets/activity-add.png" />'];
            expect($.fn.append.calls.argsFor(0)).toEqual(expectedAddition);
        });

        it('prepends text for CoffeeAddedActivity', function () {
            activity.type = 'CoffeeAddedActivity';
            PlReviewsWidget.CoffeeModal.addActivity(0, activity);
            expectedAddition = [ 'Added to the inventory' ];
            expect($.fn.prepend.calls.argsFor(0)).toEqual(expectedAddition);
        });

        // Adds to #modal-dialog-activities
        it('activity for CoffeeAddedActivity', function () {
            PlReviewsWidget.CoffeeModal.addActivity(0, activity);
            expect($.fn.append).toHaveBeenCalledOnSelector('#coffee-modal-dialog-activities');
        });

    });

    describe('#setCloseDialogClickHandler', function () {
        it('Should set the click handler on #modal-dialog-close', function () {
            PlReviewsWidget.CoffeeModal.setCloseDialogClickHandler();
            expect($('#coffee-modal-dialog-close')).toHandleWith('click', PlReviewsWidget.CoffeeModal.closeDialog);
        });
    });

    describe('#closeDialog', function () {
        beforeEach(function () {
            spyOn($.fn, 'hide');
            spyOn($.fn, 'show');
            stubEvent = {
                preventDefault: jasmine.createSpy()
            };
            PlReviewsWidget.CoffeeModal.closeDialog(stubEvent);
        });

        it('hides the modal dialog', function () {
            expect($.fn.hide).toHaveBeenCalledOnSelector('#coffee-modal-dialog');
        });

        it('shows the list content', function () {
            expect($.fn.show).toHaveBeenCalledOnSelector('#list-content');
        });

        it('clears the coffeeId', function () {
            expect(PlReviewsWidget.CoffeeModal.coffeeId).toBeNull();

        });

        it('calls prevent default on the event', function () {
            expect(stubEvent.preventDefault).toHaveBeenCalled();
        });
    });
});