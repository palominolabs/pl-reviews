require 'spec_helper'

describe AuthenticatedConstraint do
  describe '#matches?' do
    it 'returns true if the current user is authenticated' do
      user = FactoryGirl.create(:user)
      request = double(session: {
          user_id: user.id
      })
      AuthenticatedConstraint.new.matches?(request).should be_true
    end

    it 'returns false if the current user isn\'t authenticated' do
      request = double(session: {
          user_id: nil
      })
      AuthenticatedConstraint.new.matches?(request).should be_false
    end
  end
end