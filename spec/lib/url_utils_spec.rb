require 'spec_helper'

describe UrlUtils do
  describe '#add_param' do
    it 'adds query string parameters to url if there are none' do
      expected_url = 'http://www.someurl.com/beer?beer=Egregious'
      UrlUtils.add_param('http://www.someurl.com/beer', 'beer', 'Egregious').should eql expected_url
    end

    it 'appends query string parameters to url if there are already parameters' do
      expected_url = 'http://www.someurl.com/beer?brewery=TheRareBarrel&beer=Egregious'
      UrlUtils.add_param('http://www.someurl.com/beer?brewery=TheRareBarrel', 'beer', 'Egregious').should eql expected_url
    end
  end
end