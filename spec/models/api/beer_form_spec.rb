require 'spec_helper'
module Api
  describe BeerForm do
    it { should validate_presence_of :name }
    it { should validate_presence_of :brewery_brewerydb_id }
    it { should validate_presence_of :style_brewerydb_id }
    it { should validate_numericality_of(:abv).is_greater_than_or_equal_to(0).is_less_than_or_equal_to(100) }
    it { should allow_value(nil).for(:abv) }
    it { should allow_value('').for(:abv) }
  end
end