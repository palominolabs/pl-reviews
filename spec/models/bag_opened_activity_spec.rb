require 'spec_helper'

describe BagOpenedActivity do
  it_should_behave_like 'coffee_related'
  it { should validate_presence_of :user }
  it { should validate_presence_of :bag }
end
