require 'spec_helper'

describe Bag do
  it { should validate_presence_of :roasted_on }
  it { should validate_presence_of :coffee }
  it { should belong_to :coffee }
  it { should have_one :bag_opened_activity }

  describe '#open' do
    it 'returns false if the bag is already opened' do
      bag = FactoryGirl.build(:bag, opened_on: '2014-11-10')
      bag.open(nil).should be_false
    end

    context 'an unopened bag' do
      before do
        @bag = FactoryGirl.build(:bag, opened_on: nil)
      end

      it 'assigns a date to opened_on' do
        Date.stub(:parse).and_return('2014-12-11')
        @bag.open nil
        @bag.opened_on.to_s.should eql '2014-12-11'
      end

      it 'creates a BagOpenedActivity' do
        user = FactoryGirl.build(:user)
        @bag.open user
        @bag.bag_opened_activity.should be_a_new BagOpenedActivity
        @bag.bag_opened_activity.user.should eql user
        @bag.bag_opened_activity.coffee.should eql @bag.coffee
      end

      it 'should return true' do
        @bag.open(nil).should be_true
      end
    end
  end

  describe '#finish' do
    it 'returns false if the bag is already finished' do
      bag = FactoryGirl.build(:bag, finished_at: '2014-11-10')
      bag.finish.should be_false
    end

    it 'returns false if the bag is unopened' do
      bag = FactoryGirl.build(:bag, opened_on: nil)
      bag.finish.should be_false
    end

    context 'an unfinished bag' do
      before do
        @bag = FactoryGirl.build(:bag, finished_at: nil)
      end

      it 'assigns a date to finished_on' do
        Date.stub(:parse).and_return('2014-12-11')
        @bag.finish
        @bag.finished_at.to_s.should eql '2014-12-11'
      end

      it 'should return true' do
        @bag.finish.should be_true
      end
    end
  end
end
