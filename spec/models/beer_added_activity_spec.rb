require 'spec_helper'

describe BeerAddedActivity do
  it_should_behave_like 'beer_related'
  it { should validate_presence_of :user }
end
