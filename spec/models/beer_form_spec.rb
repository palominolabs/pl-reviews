require 'spec_helper'

describe BeerForm do
  it { should validate_presence_of :name }
  it { should validate_presence_of :beer_brewerydb_id }
  it { should validate_presence_of :brewery_name }
  it { should validate_presence_of :brewery_brewerydb_id }
  it { should validate_presence_of :inventory }
  it { should validate_numericality_of(:inventory).is_greater_than_or_equal_to(0).only_integer }

  describe '#must_have_inventory_to_be_in_fridge' do
    it 'raises an error if inventory is 0 and in_fridge is true' do
      beer_form = FactoryGirl.build(:beer_form, {inventory: '0', in_fridge: 1})
      beer_form.valid?
      beer_form.errors[:in_fridge].should include('Must have a beer in inventory to be in the fridge')
    end
  end
end
