require 'spec_helper'

describe BeerOpenedActivity do
  it_should_behave_like 'beer_related'
  it { should validate_presence_of :user }
end
