require 'spec_helper'

describe BeerReviewedActivity do
  it_should_behave_like 'beer_related'

  it { should validate_presence_of :review }
end
