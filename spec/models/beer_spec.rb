require 'spec_helper'

describe Beer do
  it { should belong_to :brewery }
  it { should have_many(:reviews).dependent :destroy }
  it { should have_many(:reviewers) }
  it { should have_many(:activities).dependent :destroy }

  it { should validate_presence_of :name }
  it { should validate_presence_of :brewery }
  it { should validate_presence_of :inventory }
  it { should validate_presence_of :brewerydb_id }
  it { should validate_uniqueness_of :brewerydb_id }
  it { should validate_numericality_of(:inventory).is_greater_than_or_equal_to(0).only_integer }

  describe '#average_rating' do
    it 'should return the average rating for the given beer' do
      beer = FactoryGirl.create(:beer)
      otherBeer = FactoryGirl.create(:beer)

      FactoryGirl.create(:review, { reviewable: beer, rating: 5 })
      FactoryGirl.create(:review, { reviewable: beer, rating: 3 })
      FactoryGirl.create(:review, { reviewable: otherBeer, rating: 1 })

      beer.average_rating.should eql 4
    end

    it 'should return nil if the given beer has no reviews' do
      beer = FactoryGirl.create(:beer)
      beer.average_rating.should be_nil
    end
  end

  describe '#must_have_inventory_to_be_in_fridge' do
    it 'raises an error if inventory is 0 and in_fridge is true' do
      beer = FactoryGirl.build(:beer, {inventory: '0', in_fridge: 1})
      beer.valid?
      beer.errors[:in_fridge].should include('Must have a beer in inventory to be in the fridge')
    end
  end

  describe '#open' do
    it 'returns false if beer has no inventory' do
      beer = FactoryGirl.build(:beer, {inventory: 0})
      beer.open(nil).should be_false
    end

    it 'returns true if beer has inventory' do
      beer = FactoryGirl.build(:beer, {inventory: 1})
      beer.open(nil).should be_true
    end

    it 'decrements the beer\'s inventory' do
      beer = FactoryGirl.build(:beer, {inventory: 1})
      beer.open nil
      beer.inventory.should eql 0
    end

    it 'sets in_fridge to false' do
      beer = FactoryGirl.build(:beer, {inventory: 1, in_fridge: 1})
      beer.open nil
      beer.in_fridge.should be_false
    end

    it 'creates a new BeerOpenedActivity' do
      beer = FactoryGirl.build(:beer, {inventory: 1})
      expect {
        beer.open nil
      }.to change(beer.activities, :length).by 1
    end
  end

  describe '#create_reviewed_activity' do
    before do
      @beer = FactoryGirl.build(:beer)
      @review = FactoryGirl.create(:review)  
    end
    
    it 'creates a new BeerReviewedActivity' do
      expect{
        @beer.create_reviewed_activity(@review)
      }.to change(@beer.activities, :length).by 1
      @beer.activities.last.should be_a BeerReviewedActivity
      @beer.activities.last.review.should eq @review
    end

    it 'returns true if the BeerReviewedActivity is valid' do
      BeerReviewedActivity.any_instance.should_receive(:valid?).and_return(true)
      @beer.create_reviewed_activity(@review).should be_true
    end

    it 'returns false if the BeerReviewedActivity is invalid' do
      BeerReviewedActivity.any_instance.should_receive(:valid?).and_return(false)
      @beer.create_reviewed_activity(@review).should be_false
    end
  end
end
