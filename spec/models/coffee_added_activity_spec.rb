require 'spec_helper'

describe CoffeeAddedActivity do
  it_should_behave_like 'coffee_related'
  it { should validate_presence_of :user }
end
