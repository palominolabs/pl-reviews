require 'spec_helper'

describe Coffee do
  it { should belong_to :roaster }
  it { should have_many(:bags).dependent :destroy }
  it { should have_many(:activities).dependent :destroy }
  it { should have_many(:reviews).dependent :destroy }
  it { should have_many(:reviewers) }

  it { should validate_presence_of :name }
  it { should validate_presence_of :roaster }
  it { should ensure_inclusion_of(:drip_or_espresso).in_array(['drip', 'espresso', nil, '']) }

  describe '#create_reviewed_activity' do
    before do
      @review = FactoryGirl.create(:review)
      @coffee = FactoryGirl.build(:coffee)
    end

    it 'creates a new CoffeeReviewedActivity' do
      expect{
        @coffee.create_reviewed_activity(@review)
      }.to change(@coffee.activities, :length).by 1

      @coffee.activities.last.should be_a CoffeeReviewedActivity
      @coffee.activities.last.review.should eq @review
    end

    it 'returns true if the CoffeeReviewedActivity is valid' do
      CoffeeReviewedActivity.any_instance.should_receive(:valid?).and_return(true)
      @coffee.create_reviewed_activity(@review).should be_true
    end

    it 'returns false if the CoffeeReviewedActivity is invalid' do
      CoffeeReviewedActivity.any_instance.should_receive(:valid?).and_return(false)
      @coffee.create_reviewed_activity(@review).should be_false
    end
  end
end
