require 'spec_helper'

describe Review do
  it { should belong_to :reviewable  }
  it { should belong_to :user }
  it { should have_one(:beer_reviewed_activity).dependent :destroy }
  it { should have_one(:coffee_reviewed_activity).dependent :destroy }

  it { should validate_presence_of :rating }
  it { should validate_numericality_of(:rating).is_greater_than_or_equal_to(0).is_less_than_or_equal_to(5).only_integer }
  it { should validate_presence_of :reviewable }
end
