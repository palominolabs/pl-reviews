require 'spec_helper'

describe SessionForm do
  it { should validate_presence_of :email }
  it { should validate_presence_of :password }

  describe '#must_be_a_palomino_labs_email' do
    it 'raises an error if the provided email address doesn\'t end with @palominolabs.com' do
      session_form = FactoryGirl.build(:session_form, {email: 'test'})
      session_form.valid?
      session_form.errors[:email].should include('Must be a Palomino Labs email address')
    end
  end
end
