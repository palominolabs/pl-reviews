require 'spec_helper'

describe User do
  it { should have_many(:reviews).dependent :destroy }
  it { should have_many(:reviewed_beers) }

  it { should validate_presence_of :name }
  it { should validate_presence_of :email }
  it { should validate_presence_of :password }
  it { should validate_presence_of :hipchat_id }

  context 'existing users' do
    subject { FactoryGirl.create(:user) }
    it { should_not validate_presence_of :password }
  end

  describe '#must_be_a_palomino_labs_email' do
    it 'raises an error if the provided email address doesn\'t end with @palominolabs.com' do
      user = FactoryGirl.build(:user, {email: 'test'})
      user.valid?
      user.errors[:email].should include('Must be a Palomino Labs email address')
    end
  end

  describe '#is_admin?' do
    it 'returns true if the user is an admin' do
      user = FactoryGirl.create(:user, {admin: true})
      expect(user.is_admin?).to be_true
    end

    it 'returns false if the user is not an admin' do
      user = FactoryGirl.create(:user, {admin: false})
      expect(user.is_admin?).to be_false
    end
  end
end
