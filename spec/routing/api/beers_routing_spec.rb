require 'spec_helper'

describe Api::BeersController do
  describe 'routing' do
    it 'routes to #new' do
      get('/api/breweries/5/beers/new').should route_to('api/beers#new', brewery_id: '5')
    end

    it 'routes to #create' do
      post('/api/breweries/5/beers').should route_to('api/beers#create', brewery_id: '5')
    end
  end
end