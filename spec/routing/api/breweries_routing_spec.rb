require 'spec_helper'

describe Api::BreweriesController do
  describe 'routing' do
    it 'routes to #brewery_autocomplete' do
      get('/api/brewery/autocomplete.json').should route_to('api/breweries#brewery_autocomplete', format: 'json')
    end

    it 'routes to #brewery_beer_autocomplete' do
      get('/api/brewery/5/beers.json').should route_to('api/breweries#brewery_beer_autocomplete', id: '5', format: 'json')
    end

    it 'routes to #new' do
      get('/api/breweries/new').should route_to('api/breweries#new')
    end

    it 'routes to #create' do
      post('/api/breweries').should route_to('api/breweries#create')
    end
  end
end