require 'spec_helper'

describe BagsController do
  describe 'routing' do

    it 'routes to #index' do
      get('/coffees/1/bags').should route_to('bags#index', coffee_id: '1')
      get('/coffees/1/bags.json').should route_to('bags#index', format: 'json', coffee_id: '1')
    end

    it 'routes to #new' do
      get('/coffees/1/bags/new').should route_to('bags#new', coffee_id: '1')
      get('/coffees/1/bags/new.json').should route_to('bags#new', format: 'json', coffee_id: '1')
    end

    it 'routes to #show' do
      get('/bags/1').should_not be_routable
      get('/bags/1.json').should_not be_routable
    end

    it 'routes to #create' do
      post('/coffees/1/bags').should route_to('bags#create', coffee_id: '1')
      post('/coffees/1/bags.json').should route_to('bags#create', format: 'json', coffee_id: '1')
    end

    it 'routes to #destroy' do
      delete('/bags/1').should route_to('bags#destroy', id: '1')
      delete('/bags/1.json').should route_to('bags#destroy', id: '1', format: 'json')
    end

    it 'routes to #open' do
      post('/bags/1/open').should route_to('bags#open', id: '1')
      post('/bags/1/open.json').should route_to('bags#open', id: '1', format: 'json')
    end

    it 'routes to #finish' do
      post('/bags/1/finish').should route_to('bags#finish', id: '1')
      post('/bags/1/finish.json').should route_to('bags#finish', id: '1', format: 'json')
    end
  end
end
