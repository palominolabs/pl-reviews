require 'spec_helper'

describe CoffeesController do
  describe 'routing' do

    it 'routes to #index' do
      get('/coffees').should route_to('coffees#index')
      get('/coffees.json').should route_to('coffees#index', format: 'json')
      get('roasters/1/coffees').should route_to('coffees#index', roaster_id: '1')
    end

    it 'routes to #new' do
      get('/coffees/new').should route_to('coffees#new')
      get('/coffees/new.json').should route_to('coffees#new', format: 'json')
    end

    it 'routes to #show' do
      get('/coffees/1').should route_to('coffees#show', id: '1')
      get('/coffees/1.json').should route_to('coffees#show', id: '1', format: 'json')
    end

    it 'routes to #create' do
      post('/coffees').should route_to('coffees#create')
      post('/coffees.json').should route_to('coffees#create', format: 'json')
    end

    it 'routes to #edit' do
      get('/coffees/1/edit').should route_to('coffees#edit', id: '1')
      get('/coffees/1/edit.json').should route_to('coffees#edit', format: 'json', id: '1')
    end

    it 'routes to #update' do
      put('/coffees/1').should route_to('coffees#update', id: '1')
      put('/coffees/1.json').should route_to('coffees#update', id: '1', format: 'json')
    end

    it 'routes to #destroy' do
      delete('/coffees/1').should route_to('coffees#destroy', id: '1')
      delete('/coffees/1.json').should route_to('coffees#destroy', id: '1', format: 'json')
    end
  end
end
