require 'spec_helper'

describe ReviewsController do
  describe 'routing' do

    it 'does not route to #index' do
      get('/reviews').should_not be_routable
      get('/reviews.json').should_not be_routable
    end

    it 'does not route to #new' do
      get('/reviews/new').should_not be_routable
      get('/reviews/new.json').should_not be_routable
    end

    it 'does not route to #show' do
      get('/reviews/1').should_not be_routable
      get('/reviews/1.json').should_not be_routable
    end

    it 'does not route to #edit' do
      get('/reviews/1/edit').should_not be_routable
      get('/reviews/1/edit.json').should_not be_routable
    end

    it 'routes to #create' do
      post('/beers/1/reviews').should route_to('reviews#create', beer_id: '1')
      post('/beers/1/reviews.json').should route_to('reviews#create', format: 'json', beer_id: '1')
      post('/coffees/1/reviews').should route_to('reviews#create', coffee_id: '1')
    end

    it 'does not route to #update' do
      put('/reviews/1').should_not be_routable
      put('/reviews/1.json').should_not be_routable
    end

    it 'routes to #destroy' do
      delete('/beers/1/reviews/1').should route_to('reviews#destroy', id: '1', beer_id: '1')
      delete('/beers/1/reviews/1.json').should route_to('reviews#destroy', id: '1', format: 'json', beer_id: '1')
      delete('/coffees/1/reviews/1').should route_to('reviews#destroy', id: '1', coffee_id: '1')
    end

  end
end
