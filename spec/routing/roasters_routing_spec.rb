require 'spec_helper'

describe RoastersController do
  describe 'routing' do

    it 'routes to #index' do
      get('/roasters').should route_to('roasters#index')
      get('/roasters.json').should route_to('roasters#index', format: 'json')
    end

    it 'routes to #new' do
      get('/roasters/new').should route_to('roasters#new')
      get('/roasters/new.json').should route_to('roasters#new', format: 'json')
    end

    it 'routes to #show' do
      get('/roasters/1').should route_to('roasters#show', id: '1')
      get('/roasters/1.json').should route_to('roasters#show', id: '1', format: 'json')
    end

    it 'routes to #create' do
      post('/roasters').should route_to('roasters#create')
      post('/roasters.json').should route_to('roasters#create', format: 'json')
    end

    it 'routes to #edit' do
      get('/roasters/1/edit').should route_to('roasters#edit', id: '1')
      get('/roasters/1/edit.json').should route_to('roasters#edit', format: 'json', id: '1')
    end

    it 'routes to #update' do
      put('/roasters/1').should route_to('roasters#update', id: '1')
      put('/roasters/1.json').should route_to('roasters#update', id: '1', format: 'json')
    end

    it 'routes to #destroy' do
      delete('/roasters/1').should route_to('roasters#destroy', id: '1')
      delete('/roasters/1.json').should route_to('roasters#destroy', id: '1', format: 'json')
    end
  end
end
