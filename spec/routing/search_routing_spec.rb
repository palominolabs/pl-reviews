require 'spec_helper'

describe SearchController do
  describe 'routing' do
    it 'routes to #index' do
      get('/search').should route_to('search#index')
    end

    it 'routes to #autocomplete' do
      get('/search/autocomplete.json').should route_to('search#autocomplete', format: 'json')
    end

    it 'routes to #beers' do
      get('/search/beers.json').should route_to('search#beers', format: 'json')
    end
  end
end
