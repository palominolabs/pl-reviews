require 'spec_helper'

describe BagOpenedActivitySerializer do
  it 'creates a json response for the BagOpenedActivity' do
    bag_opened_activity = FactoryGirl.build(:bag_opened_activity, coffee_id: 1, bag_id: 1)
    serializer = BagOpenedActivitySerializer.new bag_opened_activity
    serializer.stub(:serialize) do |association|
      case association.name
        when 'user'
          'user_data'
      end
    end

    expected_response = {
        bag_opened_activity: {
            id: nil,
            type: 'BagOpenedActivity',
            created_at: nil,
            bag_id: 1,
            coffee_id: 1,
            user: 'user_data'
        }
    }.with_indifferent_access
    expect(JSON.parse(serializer.to_json)).to eql expected_response
  end
end