require 'spec_helper'

describe BeerSerializer do
  it 'creates a json response for the Beer' do
    beer = FactoryGirl.build :beer, {name: 'beer name', inventory: 5}
    beer.stub(:average_rating) {3}
    serializer = BeerSerializer.new beer
    serializer.stub(:serialize) do |association|
      case association.name
        when 'brewery'
          expect(association.serializer_from_options).to be BreweryBaseSerializer
          'brewery_data'
        when 'reviews'
          expect(association.serializer_from_options).to be ReviewBaseSerializer
          'review_data'
      end
    end

    expected_response = {
        beer: {
            id: nil,
            name: 'beer name',
            brewery_id: 1,
            inventory: 5,
            abv: '1.1',
            ibu: 100,
            brewery: 'brewery_data',
            reviews: 'review_data',
            icon_url: 'some_icon.jpg',
            image_url: 'some_image.jpg',
            description: 'beer description',
            style: 'beer style',
            in_fridge: false,
            activities: nil
        }
    }.with_indifferent_access
    expect(JSON.parse(serializer.to_json)).to eql expected_response
  end
end