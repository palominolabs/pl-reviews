require 'spec_helper'

describe BrewerySerializer do
  it 'creates a json response for the brewery' do
    brewery = FactoryGirl.build :brewery
    serializer = BrewerySerializer.new brewery

    expected_response = {
        brewery: {
            id: nil,
            name: brewery.name,
            image_url: 'large image',
            icon_url: 'icon image',
            beers_count: nil
        }
    }.with_indifferent_access
    expect(JSON.parse(serializer.to_json)).to eql expected_response
  end
end