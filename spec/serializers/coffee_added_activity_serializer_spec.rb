require 'spec_helper'

describe CoffeeAddedActivitySerializer do
  it 'creates a json response for the CoffeeAddedActivity' do
    coffee_added_activity = FactoryGirl.build(:coffee_added_activity, coffee_id: 1, bag_id: 1)
    serializer = CoffeeAddedActivitySerializer.new coffee_added_activity
    serializer.stub(:serialize) do |association|
      case association.name
        when 'user'
          'user_data'
      end
    end

    expected_response = {
        coffee_added_activity: {
            id: nil,
            type: 'CoffeeAddedActivity',
            created_at: nil,
            coffee_id: 1,
            user: 'user_data'
        }
    }.with_indifferent_access
    expect(JSON.parse(serializer.to_json)).to eql expected_response
  end
end