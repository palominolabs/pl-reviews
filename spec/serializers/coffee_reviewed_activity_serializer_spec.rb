require 'spec_helper'

describe CoffeeReviewedActivitySerializer do
  it 'creates a json response for the CoffeeReviewedActivity' do
    coffee_reviewed_activity = FactoryGirl.build(:coffee_reviewed_activity, coffee_id: 1, bag_id: 1)
    serializer = CoffeeReviewedActivitySerializer.new coffee_reviewed_activity
    serializer.stub(:serialize) do |association|
      case association.name
        when 'review'
          'review_data'
      end
    end

    expected_response = {
        coffee_reviewed_activity: {
            id: nil,
            type: 'CoffeeReviewedActivity',
            created_at: nil,
            coffee_id: 1,
            review: 'review_data'
        }
    }.with_indifferent_access
    expect(JSON.parse(serializer.to_json)).to eql expected_response
  end
end