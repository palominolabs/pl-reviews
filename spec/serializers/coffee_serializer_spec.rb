require 'spec_helper'

describe CoffeeSerializer do
  it 'creates a json response for the Coffee' do
    coffee = FactoryGirl.build :coffee, {name: 'coffee name'}
    coffee.should_receive(:bags_count).and_return(5)
    serializer = CoffeeSerializer.new coffee
    serializer.stub(:serialize) do |association|
      case association.name
        when 'roaster'
          'roaster_data'
        when 'activities'
          'activities_data'
      end
    end

    expected_response = {
        coffee: {
            id: nil,
            name: 'coffee name',
            roaster_id: 1,
            origin: 'El Salvador',
            variety: 'Bourbon and Kenyan',
            drip_or_espresso: 'drip',
            bags_count: 5,
            roaster: 'roaster_data',
            activities: 'activities_data'
        }
    }.with_indifferent_access
    expect(JSON.parse(serializer.to_json)).to eql expected_response
  end
end