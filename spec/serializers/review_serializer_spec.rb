require 'spec_helper'

describe ReviewSerializer do
  it 'creates a json response for the Review' do
    beer = FactoryGirl.build(:beer, id: 1)
    review = FactoryGirl.build :review, {rating: 2, comment: 'some comment', reviewable: beer}
    serializer = ReviewSerializer.new review
    serializer.stub(:serialize) do |association|
      case association.name
        when 'user'
            'user_data'
      end
    end

    expected_response = {
        review: {
            id: nil,
            rating: 2,
            comment: 'some comment',
            reviewable_id: 1,
            reviewable_type: 'Beer',
            user: 'user_data'
        }
    }.with_indifferent_access
    expect(JSON.parse(serializer.to_json)).to eql expected_response
  end
end