require 'spec_helper'

describe RoasterSerializer do
  it 'creates a json response for the Roaster' do
    roaster = FactoryGirl.build :roaster, {name: 'roaster name'}
    serializer = RoasterSerializer.new roaster

    expected_response = {
        roaster: {
            id: nil,
            name: 'roaster name',
            location: 'Portland, OR',
            image_url: 'stumptown_image'
        }
    }.with_indifferent_access
    expect(JSON.parse(serializer.to_json)).to eql expected_response
  end
end