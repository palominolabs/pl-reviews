require 'spec_helper'

describe BrewerydbApiService do
  context 'brewery beers requests' do
    before do
      @expected_url = 'http://api.brewerydb.com/v2/brewery/test/beers'
      @expected_params = {
          params: {
              key: 'YOUR_KEY_HERE'
          },
          accept: :json
      }
    end
    describe '#get_brewery_beer_autocomplete' do
      before do
        api_response = {
            'data' => [
                {
                    'id' => 1,
                    'name' => 'Ruination'
                },
                {
                    'id' => 2,
                    'name' => 'IPA'
                },
                {
                    'id' => 3,
                    'name' => 'Arrogant Bastard'
                }
            ]
        }

        RestClient.should_receive(:get).with(@expected_url, @expected_params).and_return(api_response)
      end

      it 'returns a list of beer names' do
        expected_results = [{
                                label: 'Ruination',
                                value: 1
                            },
                            {
                                label: 'IPA',
                                value: 2
                            },
                            {
                                label: 'Arrogant Bastard',
                                value: 3
                            }]

        BrewerydbApiService.get_brewery_beer_autocomplete('test').should eql expected_results
      end
    end

    describe '#get_beer_batch' do
      it 'returns the requested beers' do
        api_response = {
            'data' => {
                'id' => '1',
                'abv' => '3.5',
                'ibu' => '100',
                'name' => 'Ruination',
                'description' => 'beer description',
                'style' => {
                    'name' => 'name of style'
                },
                'some_other_param' => 'some_other_value',
                'labels' => {
                    'large' => 'large image',
                    'icon' => 'icon image'
                }
            }
        }
        expected_url = 'http://api.brewerydb.com/v2/beer/1'
        expected_params = {
            params: {
                key: 'YOUR_KEY_HERE'
            },
            accept: :json
        }

        expected_response = {
            brewerydb_id: '1',
            name: 'Ruination',
            description: 'beer description',
            style: 'name of style',
            image_url: 'large image',
            icon_url: 'icon image',
            abv: '3.5',
            ibu: '100'
        }
        RestClient.should_receive(:get).with(expected_url, expected_params).and_return(api_response)
        BrewerydbApiService.get_beer(1).should eql expected_response
      end

      it 'should raise BeerNotFound error with no data returned' do
        RestClient.should_receive(:get).and_return({})
        expect{ BrewerydbApiService.get_beer(1) }.to raise_error(BrewerydbApiService::BeerNotFound)
      end
    end

    describe '#get_beer' do
      it 'returns the requested beer' do
        api_response = {
            'data' => {
                'id' => '1',
                'abv' => '3.5',
                'ibu' => '100',
                'name' => 'Ruination',
                'description' => 'beer description',
                'style' => {
                    'name' => 'name of style'
                },
                'some_other_param' => 'some_other_value',
                'labels' => {
                    'large' => 'large image',
                    'icon' => 'icon image'
                }
            }
        }
        expected_url = 'http://api.brewerydb.com/v2/beer/1'
        expected_params = {
            params: {
                key: 'YOUR_KEY_HERE'
            },
            accept: :json
        }

        expected_response = {
            brewerydb_id: '1',
            name: 'Ruination',
            description: 'beer description',
            style: 'name of style',
            image_url: 'large image',
            icon_url: 'icon image',
            abv: '3.5',
            ibu: '100'
        }
        RestClient.should_receive(:get).with(expected_url, expected_params).and_return(api_response)
        BrewerydbApiService.get_beer(1).should eql expected_response
      end

      it 'should raise BeerNotFound error with no data returned' do
        RestClient.should_receive(:get).and_return({})
        expect{ BrewerydbApiService.get_beer(1) }.to raise_error(BrewerydbApiService::BeerNotFound)
      end
    end

    describe '#get_brewery' do
      it 'returns the requested brewery' do
        api_response = {
            'data' => {
                'id' => 1,
                'name' => 'Stone Brewery',
                'images' => {
                    'large' => 'large image',
                    'icon' => 'icon image'
                }
            }
        }
        expected_url = 'http://api.brewerydb.com/v2/brewery/1'
        expected_params = {
            params: {
                key: 'YOUR_KEY_HERE'
            },
            accept: :json
        }

        expected_response = {
            brewerydb_id: 1,
            name: 'Stone Brewery',
            image_url: 'large image',
            icon_url: 'icon image'
        }
        RestClient.should_receive(:get).with(expected_url, expected_params).and_return(api_response)
        BrewerydbApiService.get_brewery(1).should eql expected_response
      end

      it 'should raise BreweryNotFound error with no data returned' do
        RestClient.should_receive(:get).and_return({})
        expect{ BrewerydbApiService.get_brewery(1) }.to raise_error(BrewerydbApiService::BreweryNotFound)
      end
    end

    describe '#create_brewery' do
      before do
        @brewery_form = FactoryGirl.build(:api_brewery_form, {name: 'The Rare Barrel'})
        @api_response = {
            'status' => 'success',
            'data' => {
                'id' => 'db_id'
            }
        }
      end
      it 'should make a post request to breweryDB' do
        expected_url = 'http://api.brewerydb.com/v2/breweries?key=YOUR_KEY_HERE'
        expected_params = {name: 'The Rare Barrel'}
        RestClient.should_receive(:post).with(expected_url, expected_params).and_return(@api_response)
        BrewerydbApiService.create_brewery(@brewery_form)
      end

      it 'should return the brewery_id on success' do
        RestClient.should_receive(:post).and_return(@api_response)
        BrewerydbApiService.create_brewery(@brewery_form).should eql 'db_id'
      end

      it 'should raise BreweryCreationFailed error with failed status' do
        @api_response['status'] = 'failed'
        RestClient.should_receive(:post).and_return(@api_response)
        expect{ BrewerydbApiService.create_brewery(@brewery_form) }.to raise_error(BrewerydbApiService::BreweryCreationFailed)
      end
    end

    describe '#create_beer' do
      before do
        @beer_form = FactoryGirl.build(:api_beer_form, {name: 'Egregious', style_brewerydb_id: 5, brewery_brewerydb_id: 'bdid', abv: '15.5' })
        @api_response = {
            'status' => 'success',
            'data' => {
                'id' => 'db_id'
            }
        }
      end

      it 'should make a post request to breweryDB' do
        expected_url = 'http://api.brewerydb.com/v2/beers?key=YOUR_KEY_HERE'
        expected_params = { name: 'Egregious', styleId: 5, brewery: 'bdid', abv: '15.5' }
        RestClient.should_receive(:post).with(expected_url, expected_params).and_return(@api_response)
        BrewerydbApiService.create_beer(@beer_form)
      end

      it 'should return the beer_id on success' do
        RestClient.should_receive(:post).and_return(@api_response)
        BrewerydbApiService.create_brewery(@beer_form).should eql 'db_id'
      end

      it 'should raise BeerCreationFailed error with failed status' do
        @api_response['status'] = 'failed'
        RestClient.should_receive(:post).and_return(@api_response)
        expect{ BrewerydbApiService.create_beer(@beer_form) }.to raise_error(BrewerydbApiService::BeerCreationFailed)
      end

      it 'does not include abv if value is empty' do
        @beer_form.abv = ''
        expected_url = 'http://api.brewerydb.com/v2/beers?key=YOUR_KEY_HERE'
        expected_params = { name: 'Egregious', styleId: 5, brewery: 'bdid' }
        RestClient.should_receive(:post).with(expected_url, expected_params).and_return(@api_response)
        BrewerydbApiService.create_beer(@beer_form)
      end
    end
  end
end