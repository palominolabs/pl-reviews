require 'spec_helper'

describe HipchatApiService do
  describe '#send_beer_opened_message' do
    before do
      File.stub(:read).and_return('file_contents')
      @beer_opened_file = double(evaluate: 'test_message')
      Erubis::Eruby.stub(:new).and_return(@beer_opened_file)
      HipchatApiService::HipchatContext.stub(:new).and_return('test_context')

      @client = double
      @client.stub(:send)
      HipChat::Client.stub(:new).and_return({'Hayden Playtime Room' => @client})
    end

    it 'should read the beer_opened file' do
      expected_path = Rails.root.join('app', 'views', 'admin', 'hipchat', 'beer_opened.text.erb')
      File.should_receive(:read).with(expected_path).and_return('file_contents')
      HipchatApiService.send_beer_opened_message('test_beer', 'test_user')
    end

    it 'should make a new HipChat context' do
      HipchatApiService::HipchatContext.should_receive(:new).with(beer: 'test_beer', user: 'test_user').and_return('test_context')
      HipchatApiService.send_beer_opened_message('test_beer', 'test_user')
    end

    it 'should evaluate the read file' do
      Erubis::Eruby.should_receive(:new).with('file_contents').and_return(@beer_opened_file)
      HipchatApiService.send_beer_opened_message('test_beer', 'test_user')
    end

    it 'should make a new HipChat context' do
      HipchatApiService::HipchatContext.should_receive(:new).with(beer: 'test_beer', user: 'test_user').and_return('test_context')
      HipchatApiService.send_beer_opened_message('test_beer', 'test_user')
    end

    it 'should make a new HipChat client' do
      HipChat::Client.should_receive(:new).with('qQXxBsyJQpm5Guy5nRL6902a5K7qly99BA5ycfkN', api_version: 'v2').and_return({'Hayden Playtime Room' => @client})
      HipchatApiService.send_beer_opened_message('test_beer', 'test_user')
    end

    it 'should send a message to the test_room' do
      @client.should_receive(:send).with('PL Reviews', 'test_message', notify: false, color: 'purple', message_format: 'text')
      HipchatApiService.send_beer_opened_message('test_beer', 'test_user')
    end
  end
end