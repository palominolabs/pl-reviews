require 'spec_helper'

shared_examples_for 'beer_related' do
  describe 'beer_related' do
    it { should validate_presence_of :type }
    it { should validate_presence_of :beer }
    it { should ensure_inclusion_of(:type).in_array(['BeerOpenedActivity', 'BeerAddedActivity', 'BeerReviewedActivity']) }
  end
end