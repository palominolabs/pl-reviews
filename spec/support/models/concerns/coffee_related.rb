require 'spec_helper'

shared_examples_for 'coffee_related' do
  describe 'coffee_related' do
    it { should validate_presence_of :type }
    it { should validate_presence_of :coffee }
    it { should ensure_inclusion_of(:type).in_array(['BagOpenedActivity', 'CoffeeAddedActivity', 'CoffeeReviewedActivity']) }
  end
end