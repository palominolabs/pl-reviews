require 'spec_helper'

describe HipchatBeerOpenedMessage do
  describe '#perform' do
    before do
      @beer = FactoryGirl.build(:beer)
      @user = FactoryGirl.build(:user)
      Beer.stub(:find).and_return(@beer)
      User.stub(:find).and_return(@user)
      HipchatApiService.stub(:send_beer_opened_message)
    end

    it 'should find the specified beer' do
      Beer.should_receive(:find).with(1)
      HipchatBeerOpenedMessage.perform(1,2)
    end

    it 'should find the specified user' do
      User.should_receive(:find).with(2)
      HipchatBeerOpenedMessage.perform(1,2)
    end

    it 'should call send_beer_opened_message with expected parameters' do
      HipchatApiService.should_receive(:send_beer_opened_message).with(@beer,@user)
      HipchatBeerOpenedMessage.perform(1,2)
    end
  end
end